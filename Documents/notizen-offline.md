# Hati's Offline Notizen

Zeit:


+ DialogNodes mit Factory erstellen
	+ Injecten von Systemen / Commands etc möglich (siehe ShipStates im Beispiel 1)

+ DialogNodeDecorators für:
	+ Grafiken (ermöglicht eleganten Austausch von Assets)
	+ Items erhalten

+ DialogOptionen per Factory erstellen
	+ Siehe oben generell bei Nodes

+ Routes Überblick (einzelne Routen) per Factory erstellen
	+ Routen in der Übersicht brauchen mehr Infos als Gedacht
		+ Für Fortschrittsanzeige (Anzahl der Wegpunkte)
			 => Wenn x/x Erreicht = Fertig
			 => RoutenName muss aus Tabelle gelesen werden
			 => Eigener Localizer (Localizer Context per Route Info)
	+ Ort muss ausgelesen sein / Als Information angegeben werden
		=> Ort by GPS Service online??

+ Sagenbuch-Inhalt brauch mehrere Templates
	=> Siehe Mockups
+ Anzahl an Gesamtseiten aus Route auslesen
+ Seitenanzahl wichtig bei Sagenbuch?


+ Taskbar Ähnliches Menü Unten mit Variablen Buttons
