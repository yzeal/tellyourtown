# Schulprojekt Unity Projekt

## Hinweise

## Benötigte Programme

## inhouse Plugins

### FolkloreLocation

FolkloreLocation ist ein Native Plugin für **Android und iOS**. Es dient primär zur Verbesserung der
Location API von Unity. (double Präzision, bessere Kontrolle über Abfragen, gezieltere Daten)

## externe Plugins

|Name|Homepage|Dokumentation|Pfad|
|---|---|---|---|
|[Couchbase Lite](#couchbase)|[Homepage]()|[API Reference](http://developer.couchbase.com/documentation/mobile/1.2/develop/references/index.html)|**`Assets/Plugins/Couchbase`**|
|[UniRx](#unirx)|[GitHub: neuecc/UniRx](https://github.com/neuecc/UniRx)|[GitHub](https://github.com/neuecc/UniRx)|**`Assets/Plugins/UniRx`**|
|[Zenject](#zenject)|[GitHub: modesttree/Zenject](https://github.com/modesttree/Zenject)|[GitHub](https://github.com/modesttree/Zenject)|**`Assets/Zenject`**|


### Couchbase Lite

Couchbase Lite ist eine embedded Database für mobile Systeme.
Diese Datenbank kann mit anderen Modulen des Datenbank System (Couchbase) nachsynchronisiert werden.
Sodass die Daten weiterhin offline verfügbar sind.

Die Binaries befinden sich unter **`Assets/Plugins/Couchbase`**.

Couchbase Lite ist verfügbar für **iOS, Android und Desktop**.

### UniRx

UniRx ist eine [ReactiveExtensions](https://gist.github.com/staltz/868e7e9bc2a7b8c1f754)(Rx) Implementierung für Unity.
Dabei bietet es neben den Standard Rx Implementierungen kleinere Unity spezifische Änderungen
um besser mit dem System zu interagieren.

### Zenject

Zenject ist ein [Dependency Injection](https://github.com/modesttree/Zenject#theory)(DI) Framework.
Der größte Anwendungsfall ist in der Implementierung von Templateszenen (Dialoge).

