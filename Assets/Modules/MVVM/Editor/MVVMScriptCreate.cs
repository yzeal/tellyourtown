﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using ModestTree;

public static class MVVMScriptCreate
{
	#region Utilities
	public static string ConvertFullAbsolutePathToAssetPath(string fullPath)
	{
		fullPath = Path.GetFullPath(fullPath);

		var assetFolderFullPath = Path.GetFullPath(Application.dataPath);

		if (fullPath.Length == assetFolderFullPath.Length)
		{
			Assert.IsEqual(fullPath, assetFolderFullPath);
			return "Assets";
		}

		var assetPath = fullPath.Remove(0, assetFolderFullPath.Length + 1).Replace("\\", "/");
		return "Assets/" + assetPath;
	}

	public static string GetCurrentDirectoryAssetPathFromSelection()
	{
		return ConvertFullAbsolutePathToAssetPath(
				GetCurrentDirectoryAbsolutePathFromSelection());
	}

	public static string GetCurrentDirectoryAbsolutePathFromSelection()
	{
		var folderPath = TryGetSelectedFolderPathInProjectsTab();

		if (folderPath != null)
		{
			return folderPath;
		}

		var filePath = TryGetSelectedFilePathInProjectsTab();

		if (filePath != null)
		{
			return Path.GetDirectoryName(filePath);
		}

		return Application.dataPath;
	}

	public static string TryGetSelectedFilePathInProjectsTab()
	{
		return GetSelectedFilePathsInProjectsTab().OnlyOrDefault();
	}

	public static List<string> GetSelectedFilePathsInProjectsTab()
	{
		return GetSelectedPathsInProjectsTab()
				.Where(x => File.Exists(x)).ToList();
	}

	public static List<string> GetSelectedPathsInProjectsTab()
	{
		var paths = new List<string>();

		UnityEngine.Object[] selectedAssets = Selection.GetFiltered(
				typeof(UnityEngine.Object), SelectionMode.Assets);

		foreach (var item in selectedAssets)
		{
			var relativePath = AssetDatabase.GetAssetPath(item);

			if (!string.IsNullOrEmpty(relativePath))
			{
				var fullPath = Path.GetFullPath(Path.Combine(
						Application.dataPath, Path.Combine("..", relativePath)));

				paths.Add(fullPath);
			}
		}

		return paths;
	}

	// Note that the path is relative to the Assets folder
	public static List<string> GetSelectedFolderPathsInProjectsTab()
	{
		return GetSelectedPathsInProjectsTab()
				.Where(x => Directory.Exists(x)).ToList();
	}

	// Returns the best guess directory in projects pane
	// Useful when adding to Assets -> Create context menu
	// Returns null if it can't find one
	// Note that the path is relative to the Assets folder for use in AssetDatabase.GenerateUniqueAssetPath etc.
	public static string TryGetSelectedFolderPathInProjectsTab()
	{
		return GetSelectedFolderPathsInProjectsTab().OnlyOrDefault();
	}

	static void AddCSharpClassTemplate(
						string friendlyName, string defaultFileName, bool editorOnly, string templateStr)
	{
		var folderPath = GetCurrentDirectoryAssetPathFromSelection();

		if (editorOnly && !folderPath.Contains("/Editor"))
		{
			EditorUtility.DisplayDialog("Error",
					"Editor window classes must have a parent folder above them named 'Editor'.  Please create or find an Editor folder and try again", "Ok");
			return;
		}

		var absolutePath = EditorUtility.SaveFilePanel(
				"Choose name for " + friendlyName,
				folderPath,
				defaultFileName + ".cs",
				"cs");

		if (absolutePath == "")
		{
			// Dialog was cancelled
			return;
		}

		if (!absolutePath.ToLower().EndsWith(".cs"))
		{
			absolutePath += ".cs";
		}

		var className = Path.GetFileNameWithoutExtension(absolutePath);
		File.WriteAllText(absolutePath, templateStr.Replace("CLASS_NAME", className));

		AssetDatabase.Refresh();

		var assetPath = ConvertFullAbsolutePathToAssetPath(absolutePath);

		EditorUtility.FocusProjectWindow();
		Selection.activeObject = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(assetPath);
	}
	#endregion

	[MenuItem("Assets/Create/MVVM/View", false, 1)]
	public static void CreateMVVMView()
	{
		AddCSharpClassTemplate("View", "UntitledView", false,
									"using Zenject;"
								+ "\nusing UniRx;"
								+ "\nusing System;"
								+ "\nusing System.Linq;"
								+ "\nusing UnityEngine;"
								+ "\nusing UnityEngine.Events;"
								+ "\nusing UnityEngine.UI;"
								+ "\n"
								+ "\npublic class CLASS_NAME : MonoBehaviour"
								+ "\n{"
								+ "\n	[Inject]"
								+ "\n	public void Construct("
								+ "\n		"
								+ "\n		)"
								+ "\n	{"
								+ "\n		// TODO"
								+ "\n	}"
								+ "\n}");
	}

	[MenuItem("Assets/Create/MVVM/View Model", false, 1)]
	public static void CreateMVVMViewModel()
	{
		AddCSharpClassTemplate("View Model", "UntitledView", false,
									"using Zenject;"
								+ "\nusing UniRx;"
								+ "\nusing System;"
								+ "\nusing System.Linq;"
								+ "\nusing UnityEngine;"
								+ "\nusing UnityEngine.UI;"
								+ "\n"
								+ "\npublic class CLASS_NAME"
								+ "\n{"
								+ "\n	[Inject]"
								+ "\n	public CLASS_NAME("
								+ "\n		"
								+ "\n		)"
								+ "\n	{"
								+ "\n		//"
								+ "\n	}"
								+ "\n}");
	}
}
