using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class FNameDynamicTextView : MonoBehaviour
{
	public FNameReactiveProperty p_Localization;
	[HideInInspector]
	public FName Localization { get { return p_Localization.Value; } set { p_Localization.Value = value; } }

	[Inject]
	public void Construct(
		Localizer l,
		LanguageHandler lh
		)
	{

		Observable.CombineLatest(
			lh.p_CurrentLang,
			p_Localization,
			(lang, r) => r
			)
			.Select(name => l.tr(name))
			.SubscribeToText(GetComponent<Text>())
			.AddTo(gameObject)
			;
	}

	private void OnValidate()
	{
		if (this.Localization)
		{
			GetComponent<Text>().text = this.Localization.Handle;
		}
		else
		{
			GetComponent<Text>().text = "";
		}
	}



}