using UnityEngine;
using Zenject;
using UniRx;

public class LocaleInstaller : Installer<LocaleInstaller>
{
	readonly LocalizationBook AppTranslations = new LocalizationBook()
	{
		{ "TR_START", new LocalizationEntry()
		{
			{ "de", "Spiel starten" },
			{ "en", "Start Game" }
		} },
		{ "TR_PROGRESS", new LocalizationEntry()
		{
			{ "de", "Fortschritt" },
			{ "en", "Progress" }
		} },
		{ "TR_LOCATION", new LocalizationEntry()
		{
			{ "de", "Ort" },
			{ "en", "Location" },
			{ "lb", "Uert" },
			{ "fr", "Place" }
		} },
		{ "TR_LENGTH", new LocalizationEntry()
		{
			{ "de", "Strecke" },
			{ "en", "Length" }
		} },
		{ "TR_HELP", new LocalizationEntry()
		{
			{ "de", "Hilfe" },
			{ "en", "Help" }
		} },
		{ "TR_CREDITS", new LocalizationEntry()
		{
			{ "de", "Credits" },
			{ "en", "Credits" }
		} },
		{ "TR_CREDITSAPP", new LocalizationEntry()
		{
			{ "de", "Applikation" },
			{ "en", "Application" }
		} },
		{ "TR_CREDITSCONTENT", new LocalizationEntry()
		{
			{ "de", "Inhalte" },
			{ "en", "Content" }
		} },
		{ "TR_CREDITSSUPPORT", new LocalizationEntry()
		{
			{ "de", "Unterstützer" },
			{ "en", "Supporters" }
		} },
		{ "TR_SKIP_WAYPOINT", new LocalizationEntry()
		{
			{ "de", "Wegstück überspringen" },
			{ "en", "Skip distance" },
			{ "lb", "Streck iwwersprangen" },
			{ "fr", "Sauter route" },
		} },
		{ "TR_CHOOSE_LANG", new LocalizationEntry()
		{
			{ "de", "Wähle eine Sprache:" },
			{ "en", "Choose a language:" }
		} },
		{ "TR_CHOOSE_ROUTE", new LocalizationEntry()
		{
			{ "de", "Wähle eine Route:" },
			{ "en", "Choose a route:" }
		} },
		{ "TR_GPS_STATUS", new LocalizationEntry()
		{
			{ "de", "GPS-Status:" },
			{ "en", "GPS Status:" },
			{ "lb", "Statut de GPS:" },
			{ "fr", "GPS Status:" },
		} },
		{ "TR_GPS_STATE_OFF", new LocalizationEntry()
		{
			{ "de", "[Keine Verbindung]" },
			{ "en", "[No Connection]" },
			{ "lb", "[keng Verbindung]" },
			{ "fr", "[pas de connection]" }
		} },
		{ "TR_GPS_STATE_ON", new LocalizationEntry()
		{
			{ "de", "[Verbindung erfolgreich]" },
			{ "en", "[Connection established]" },
			{ "lb", "[Connection gouf etabléiert]" },
			{ "fr", "[Connection établie]" }
		} },
		{ "TR_RESETROUTE", new LocalizationEntry()
		{
			{ "de", "Route neustarten" },
			{ "en", "Restart route" }
		} },
		{ "TR_CONTINUEROUTE", new LocalizationEntry()
		{
			{ "de", "Route fortsetzen" },
			{ "en", "Continue route" }
		} },
		{ "TR_STARTROUTE", new LocalizationEntry()
		{
			{ "de", "Route starten" },
			{ "en", "Start route" }
		} },
	};

	public override void InstallBindings()
	{
		Container.Bind<LocalizationBook>().WithId(EContext.Application).To<LocalizationBook>().FromInstance(AppTranslations);
		Container.Bind<Localizer>().AsSingle();
	}
}
