using UnityEngine;
using System.Collections;
using Zenject;

public class LocalizedTextInstaller : MonoInstaller<LocalizedTextInstaller>
{
	[InjectOptional]
	[HideInInspector]
	public string LocalizationTag = null;

	public override void InstallBindings()
	{
		Container.Bind<string>().FromInstance(LocalizationTag).AsSingle().WhenInjectedInto<LocalizedTextController>();
		Container.Bind<LocalizedTextController>().AsSingle().NonLazy();
	}
}
