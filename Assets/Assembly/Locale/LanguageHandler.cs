using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using System.Linq;

public class LanguageHandler
{
	public readonly Dictionary<FELanguages, FCultureInfo> AppLanguages = new Dictionary<FELanguages, FCultureInfo>() {
		{ FELanguages.German, new FCultureInfo("de") }
		, { FELanguages.English, new FCultureInfo("en", "English (United Kingdom)") }
		, { FELanguages.French, new FCultureInfo("fr") }
		, { FELanguages.Luxembourgish, new FCultureInfo("lb", "Lëtzebuergesch") }
	};

	public ReactiveProperty<FELanguages> p_CurrentLang = new ReactiveProperty<FELanguages>(FELanguages.German);
	public FELanguages CurrentLang { get { return p_CurrentLang.Value; } set { p_CurrentLang.Value = value; } }

	public FCultureInfo CurrentCulture { get { return AppLanguages[CurrentLang]; } }

	public FELanguages GetPrevAppLanguage(FELanguages l)
	{
		FELanguages? prev = null;
		foreach (var lang in AppLanguages)
		{
			if (lang.Key == CurrentLang)
			{
				if (prev.HasValue)
					return prev.Value;
			}
			prev = lang.Key;
		}
		return AppLanguages.Last().Key;
	}

	public FELanguages GetNextAppLanguage(FELanguages l)
	{
		bool hasCurrent = false;
		foreach (var lang in AppLanguages)
		{
			if (hasCurrent)
			{
				return lang.Key;
			}
			if (l == lang.Key)
			{
				hasCurrent = true;
			}
		}
		return AppLanguages.First().Key;
	}

	public IObservable<FELanguages> SetPrevAppLanguage()
	{
		return Observable.Create<FELanguages>(observer =>
		{
			var prev = GetPrevAppLanguage(CurrentLang);
			// Debug.LogFormat("Changing language from {0} to {1}.", CurrentLang, prev);
			CurrentLang = prev;

			observer.OnNext(CurrentLang);
			observer.OnCompleted();

			return Disposable.Create(() => { });
		});
	}

	public IObservable<FELanguages> SetNextAppLanguage()
	{
		return Observable.Create<FELanguages>(observer =>
		{
			var next = GetNextAppLanguage(CurrentLang);
			// Debug.LogFormat("Changing language from {0} to {1}.", CurrentLang, next);
			CurrentLang = next;

			observer.OnNext(CurrentLang);

			observer.OnCompleted();

			return Disposable.Create(() => { });
		});
	}

}
