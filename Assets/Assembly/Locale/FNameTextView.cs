using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class FNameTextView : MonoBehaviour
{
	public FName Localization;

	[Inject]
	public void Construct(
		Localizer l,
		LanguageHandler lh
		)
	{
		lh.p_CurrentLang
			.Select(_ => l.tr(Localization))
			.Select(localizedString => localizedString.Replace("###", "\n"))
			.Subscribe(newText =>
			{
				GetComponent<Text>().text = newText;
			})
			.AddTo(this.gameObject)
			;
	}

	private void OnValidate()
	{
		if (this.Localization)
		{
			GetComponent<Text>().text = this.Localization.Handle;
		}
		else
		{
			GetComponent<Text>().text = "";
		}
	}



}