using UnityEngine;
using System.Collections;
using System.Globalization;

public class FCultureInfo
{
	public string Name { get; private set; }
	public string NativeName { get; private set; }

	public CultureInfo _CultureInfo = null;
	public bool HasCultureInfo { get { return _CultureInfo != null; } }


	/**
	 * Name is the tag ie. 'en', 'de', 'fr'
	 */ 
	public FCultureInfo(string Name)
	{
		_CultureInfo = CultureInfo.CreateSpecificCulture(Name);
		this.Name = Name;
		this.NativeName = _CultureInfo.NativeName;
	}

	public FCultureInfo(string Name, string NativeName)
	{
		this.Name = Name;
		this.NativeName = NativeName;
	}
}
