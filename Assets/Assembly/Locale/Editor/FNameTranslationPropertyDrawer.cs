﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(FName.FNameElements))]
public class FNameTranslationPropertyDrawer : PropertyDrawer {

	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		return EditorGUIUtility.singleLineHeight * 3;
	}

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		var keyRect = position;
		keyRect.width /= 4;
		keyRect.width -= 4;

		keyRect.height = EditorGUIUtility.singleLineHeight;

		EditorGUI.PropertyField(keyRect, property.FindPropertyRelative("locale"), GUIContent.none, false);

		var valueRect = position;
		valueRect.x = keyRect.width + 4;
		valueRect.width = position.width - keyRect.width;
		valueRect.height = EditorGUIUtility.singleLineHeight * 3;

		EditorGUI.PropertyField(valueRect, property.FindPropertyRelative("translation"), GUIContent.none, false);
	}
}
