using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(LocalizedText))]
public class LocalizedTextEditor : Editor
{

	private static readonly string[] _dontIncludeMe = new string[] { "m_Text", "m_OnCullStateChanged" };

	public override void OnInspectorGUI()
	{
		serializedObject.Update();

		DrawPropertiesExcluding(serializedObject, _dontIncludeMe);

		if(GUI.changed)
		{
			serializedObject.FindProperty("m_Text").stringValue = serializedObject.FindProperty("LocalizationTag").stringValue;
			serializedObject.ApplyModifiedProperties();
		}
	}
}
