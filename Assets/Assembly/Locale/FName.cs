﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FName", menuName = "Folklore/FName")]
public class FName : ScriptableObject
{
	[ReadOnly]
	public string Handle;

	[System.Serializable]
	public class FNameElements
	{
		public string locale;
		[Multiline]
		public string translation;
	}

	[SerializeField]
	private List<FNameElements> __EdTranslations;

	public Dictionary<string, string> Translations;

	public void InitFromDictionary()
	{
		OnEnable();
	}

	private void OnEnable()
	{
		if (__EdTranslations == null) __EdTranslations = new List<FNameElements>();
		if (Translations == null) Translations = new Dictionary<string, string>();
		Translations.Clear();
		foreach (var tr in __EdTranslations)
		{
			Translations[tr.locale] = tr.translation;
		}
	}
}
