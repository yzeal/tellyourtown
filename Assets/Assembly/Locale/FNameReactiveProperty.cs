﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

[System.Serializable]
public class FNameReactiveProperty : ReactiveProperty<FName>
{
}
