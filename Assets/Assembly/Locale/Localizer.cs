using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zenject;
using UniRx;
using UnityEngine;

public class Localizer
{
	private Dictionary<string, LocalizationEntry> translations;

	private LanguageHandler language;

	[Inject]
	public Localizer(
		LanguageHandler lh,
		[Inject(Id = EContext.Application)] LocalizationBook appTranslation,
		[InjectOptional(Id = EContext.Route)] LocalizationBook routeTranslation
		)
	{
		this.language = lh;
		translations = new Dictionary<string, LocalizationEntry>();
		translations = translations.Concat(appTranslation).ToDictionary(s => s.Key, s => s.Value);
		if (routeTranslation != null)
		{
			translations = translations.Keys.Union(routeTranslation.Keys).ToDictionary(k => k, k => routeTranslation.ContainsKey(k) ? routeTranslation[k] : translations[k]);
		}
	}

	public string tr(string tag)
	{
		return tr(tag, language.CurrentCulture.Name);
	}

	public string tr(FName tag)
	{
		var CurrentLocale = language.CurrentCulture.Name;
		string localizedText;
		if (!tag || !tag.Translations.TryGetValue(CurrentLocale, out localizedText))
		{
			localizedText = string.Format("No locale for {0} in {1}", tag!=null?tag.Handle:"No asset", CurrentLocale);
		}
		return localizedText;
	}

	// @todo to be tested
	public IObservable<string> trAsObservable(FName tag)
	{
		return Observable.Create<string>((observer) =>
		{
			var currentLangSub = this.language.p_CurrentLang.Subscribe(_ =>
			{
				observer.OnNext(this.tr(tag));
			});


			return currentLangSub;
		});
	}

	public string tr(string tag, string CurrentLocale)
	{
		string translatedString = tag;

		if (translations.ContainsKey(tag) || translations.ContainsKey(tag.ToUpper()) || translations.ContainsKey(tag.ToLower()))
		{
			LocalizationEntry trMap;
			if (translations.TryGetValue(tag, out trMap) || translations.TryGetValue(tag.ToUpper(), out trMap) || translations.TryGetValue(tag.ToLower(), out trMap))
			{
				string text;
				if (trMap.TryGetValue(CurrentLocale, out text))
				{
					translatedString = text;
				}
			}
		}
		return translatedString;
	}
}
