using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Zenject;
using UniRx;

public class LocalizedTextController
{
	[Inject]
	public LocalizedTextController(
		[Inject(Source = InjectSources.Parent)] Localizer l,
		[Inject(Source = InjectSources.Local)] LocalizedText view,
		[InjectOptional(Source = InjectSources.Local)] string injectedTag,
		LanguageHandler lh
		)
	{
		string tag = injectedTag != "" ? injectedTag : view.LocalizationTag;
		view.text = l.tr(tag);
		Debug.LogFormat("Localizing {0} to {1}", tag, view.text);
		lh.p_CurrentLang
			.Subscribe(newlang =>
			{
				view.text = l.tr(tag);
				Debug.LogFormat("Localizing {0} to {1}", tag, view.text);
			})
			.AddTo(view);
	}
}
