using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Zenject;

public class LocalizerDebugHotkeys : ITickable
{
	private readonly CycleAppLanguagesCommand cycleLang;

	[Inject]
	public LocalizerDebugHotkeys
		(
		[InjectOptional] CycleAppLanguagesCommand cycleLang
		)
	{
		this.cycleLang = cycleLang;
	}

	public void Tick()
	{
		if (Input.GetKeyUp(KeyCode.L))
		{
			if (cycleLang != null)
			{
				cycleLang.Fire();
			}
			else
			{
				Debug.Log("cycleLang is null");
			}
		}
	}
}
