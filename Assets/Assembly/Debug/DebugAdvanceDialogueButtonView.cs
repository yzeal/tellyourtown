﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

[RequireComponent(typeof(Button))]
public class DebugAdvanceDialogueButtonView : MonoBehaviour
{

	[SerializeField]
	[ReadOnly]
	protected BoolReactiveProperty p_Enabled = new BoolReactiveProperty(false);
	public bool Enabled { get { return p_Enabled.Value; } set { p_Enabled.Value = value; } }

	[Inject]
	void Construct(
		DialogInstaller.Settings dsettings,
		AdvanceDialogCommand _advanceDialog,
		AudioSource uiaudio
		)
	{
		GetComponent<Button>()
			.OnClickAsObservable()
			.Where(_ => Enabled)
			.SelectMany(_ => dsettings.DialogWeiterCue.Play(uiaudio))
			.Subscribe(_ => _advanceDialog.Fire())
			.AddTo(gameObject)
			;
	}
}
