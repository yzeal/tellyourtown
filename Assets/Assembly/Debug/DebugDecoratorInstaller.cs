using UnityEngine;
using Zenject;

public class DebugDecoratorInstaller : MonoInstaller<DebugDecoratorInstaller>
{
	public override void InstallBindings()
	{
		Container.Bind<ITickable>().To<DialogDebugHotkeys>().AsSingle();
		Container.Bind<ITickable>().To<LocalizerDebugHotkeys>().AsSingle();
	}
}
