using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

public class DebugNextWaypointOnReach : IDisposable
{
	private WaypointReachedSignal _signal;
	private readonly RouteState currentState;
	private readonly ZenjectSceneLoader sceneLoader;
	private readonly ALocationHandler locationH;
	private PlayerProgress progress;
	private FDatabase _fdb;

	[Inject]
	public DebugNextWaypointOnReach(
		ALocationHandler alh,
		WaypointReachedSignal signal,
		RouteState state,
		ZenjectSceneLoader zsl,
		PlayerProgress pprogress,
		FDatabase fdb
		)
	{
		locationH = alh;
		currentState = state;
		_signal = signal;

		_signal += OnReached;
		sceneLoader = zsl;
		progress = pprogress;
		_fdb = fdb;
	}

	void OnReached()
	{
		progress.Points += 1;

		if(currentState.nextWaypoints.Count() == 0) /* End of route reached */
		{
			sceneLoader.LoadSceneAsync("MainMenu", LoadSceneMode.Single);
		}
		else
		{
			sceneLoader.LoadSceneAsync("RoutingTemplate", LoadSceneMode.Single, (container) =>
			{
				var target = currentState.nextWaypoints.First();

				var newRouteState = new RouteState()
				{
					name = currentState.name,
					targetWaypoint = target,
					nextWaypoints = currentState.nextWaypoints.Skip(1),
					CurrentDistance = locationH.Location.GetDistanceXY(target.Location)
				};
				container.Bind<RouteState>().FromInstance(newRouteState).WhenInjectedInto<LocationInstaller>();
				container.Bind<FLocation?>().FromInstance(locationH.Location).WhenInjectedInto<ALocationHandler>();
			});
		}
	}

	public void Dispose()
	{
		_signal -= OnReached;
	}
}
