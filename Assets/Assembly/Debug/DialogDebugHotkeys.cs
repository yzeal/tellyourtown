using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

public class DialogDebugHotkeys : ITickable 
{
	readonly AdvanceDialogCommand advanceDialog;
	readonly ZenjectSceneLoader sceneLoader;

	[Inject]
	public DialogDebugHotkeys(
		[InjectOptional] AdvanceDialogCommand _advanceDialog
		, ZenjectSceneLoader zSceneLoader
		)
	{
		this.advanceDialog = _advanceDialog;
		this.sceneLoader = zSceneLoader;

	} 

	public void Tick()
	{
		if(Input.GetKeyDown(KeyCode.Alpha1))
		{
			if (advanceDialog != null)
				advanceDialog.Fire();
		}
		else if(Input.GetKeyUp(KeyCode.Alpha5))
		{
			sceneLoader.LoadScene("DebugDecorator", LoadSceneMode.Single);
			sceneLoader.LoadScene("DialogTemplate", LoadSceneMode.Additive, (container) =>
			{
				container.Bind<string>().FromInstance("hello world").WhenInjectedInto<DialogInstaller>();
				container.Bind<Texture2D>().WithId("background-image").FromInstance(Resources.Load("bg-porta") as Texture2D).WhenInjectedInto<DialogInstaller>();
				container.Bind<Texture2D>().FromInstance(Resources.Load("eisverkaeufer") as Texture2D).WhenInjectedInto<DialogInstaller>();
			});
		}
	}
}
