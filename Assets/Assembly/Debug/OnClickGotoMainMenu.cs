using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Button))]
public class OnClickGotoMainMenu : MonoBehaviour
{
	private ZenjectSceneLoader sceneLoader;

	public void Start()
	{
		GetComponent<Button>()
			.OnClickAsObservable()
			.First()
			.Subscribe(_ => sceneLoader.LoadSceneAsync("MainMenu", LoadSceneMode.Single))
			.AddTo(gameObject)
			;
	}

	[Inject]
	public void Construct(
		ZenjectSceneLoader sceneLoader
		)
	{
		this.sceneLoader = sceneLoader;
	}
}