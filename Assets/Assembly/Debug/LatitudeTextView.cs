﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UniRx;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class LatitudeTextView : MonoBehaviour
{
	[Inject]
	void Construct(
		ALocationHandler locHandler
		)
	{
		Text text = GetComponent<Text>();
		locHandler.p_Location
			.Select(l => TextUtil.FixedDoubleString(l.latitude))
			.Subscribe(l =>
			{
				text.text = l;
			})
			.AddTo(gameObject);
	}
}
