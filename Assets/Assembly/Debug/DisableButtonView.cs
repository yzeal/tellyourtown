using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DisableButtonView : MonoBehaviour
{
	[Inject]
	public void Construct(
		
		)
	{
		GetComponent<Button>().interactable = false;
	}
}