﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

[RequireComponent(typeof(Button))]
public class DebugWinGameButtonView : MonoBehaviour {
	
	[Inject]
	void Construct(
		FContentSolvedSignal signal
		)
	{
		GetComponent<Button>().OnClickAsObservable()
			.First()
			.Subscribe(_ =>
			{
				signal.Fire();
			})
			.AddTo(gameObject)
			;
	}
}
