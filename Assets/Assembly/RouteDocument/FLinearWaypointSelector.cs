﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[CreateAssetMenu(fileName ="LinearWaypointSelector", menuName = "Folklore/Linear WaypointSelector")]
public class FLinearWaypointSelector : WaypointSelector {

	public override List<FWaypointInfo> MutateWaypoints(FRouteWaypoints Waypoints)
	{
		return Waypoints.Waypoints;
	}

}
