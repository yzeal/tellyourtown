using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;


[CreateAssetMenu(fileName = "TrierFRD", menuName = "Folklore/FixtureRoutes/Trier")]
public class TrierFRD : FRouteDocument
{

	public override string handle { get { return "trier"; } }

	readonly private LocalizationBook _localization = new LocalizationBook()
	{
		{ "TR_LOCATION", new LocalizationEntry() {
			{ "en", "Treves" },
			{ "de", "Trier" },
			{ "lb", "Tréier" },
			{ "fr", "Trèves" },
		} },
		{ "TR_ROUTENAME", new LocalizationEntry() {
			{ "en", "Karl Marx against Nero!" },
			{ "de", "Karl Marx gegen Nero!" },
			{ "fr", "Karl Marx à l'encontre de Nero!" },
			{ "lb", "Karl Marx widder Nero!" },
		} },
		{ "TR_ROUTEDESC", new LocalizationEntry() {
			{ "en", "" },
			{ "de", "" },
			{ "lb", "" },
			{ "fr", "" },
		} },
	};

	public override LocalizationBook localization { get { return _localization; } }

	public TrierFRD(
		)
	{
	}
}
