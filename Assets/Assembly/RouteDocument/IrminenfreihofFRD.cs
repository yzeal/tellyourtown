using System.Collections.Generic;
using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "IrminenfreihofFRD", menuName = "Folklore/FixtureRoutes/Irminenfreihof")]
public class IrminenfreihofFRD : FRouteDocument
{
	public override string handle { get { return "irminenfreihof"; } }
	public override LocalizationBook localization { get { return _book; } }

	private LocalizationBook _book = new LocalizationBook()
	{
		{ "TR_LOCATION", new LocalizationEntry() {
			{ "en", "Esch" },
			{ "de", "Esch" },
			{ "lb", "Esch" },
			{ "fr", "Esch" },
		} },
		{ "TR_ROUTENAME", new LocalizationEntry() {
			{ "en", "The miners of Esch!" },
			{ "de", "Die Escher Minenarbeiter!" },
			{ "lb", "d'Biergaarbechter vun Esch" },
			{ "fr", "Les mineurs d'Esch!" },
		} },
		{ "TR_ROUTEDESC", new LocalizationEntry() {
			{ "en", "" },
			{ "de", "" },
			{ "fr", "" },
			{ "lb", "" },
		} }
	};

	public IrminenfreihofFRD(
		)
	{
	}
}
