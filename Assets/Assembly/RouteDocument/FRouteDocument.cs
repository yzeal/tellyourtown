using Zenject;
using UniRx;
using System.Collections.Generic;
using UnityEngine;

public abstract class FRouteDocument : ScriptableObject
{
	public abstract string handle { get; }
	public abstract LocalizationBook localization { get; }

	public FRouteWaypoints Waypoints { get { return m_Waypoints; } }

	[SerializeField]
	protected FRouteWaypoints m_Waypoints;
	public WaypointSelector m_WaypointSelector;

	public FName RouteName;
	public FName RouteLocation;
	public FName RouteDescription;
	public PlayerProgress m_DefaultProgress;

	public FRouteDocument(
		)
	{
	}

	public PlayerProgress CopyPlayerProgressDefault()
	{
		return new PlayerProgress()
		{
			Points = this.m_DefaultProgress.Points,
			ResourceCapacity = this.m_DefaultProgress.ResourceCapacity,
			ResourceAvailable = this.m_DefaultProgress.ResourceAvailable,
			ResourceName = this.m_DefaultProgress.ResourceName,
			Items = new List<InventoryItem>(this.m_DefaultProgress.Items),
			ResourceImage = this.m_DefaultProgress.ResourceImage,
			ResourceChangeCue = this.m_DefaultProgress.ResourceChangeCue
		};
	}
}
