﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

[CreateAssetMenu(fileName = "RandomStartWaypointSelector", menuName = "Folklore/Random Start WaypointSelector")]
public class FShuffledWaypointSelector : WaypointSelector {

	[Tooltip("Index to start splitting at (defining the prefix)")]
	public int StartSplittingPoint = 0;
	[Tooltip("Indexes to start reenter the route defining the start index of splitting and running the route + this-prefix is the skipped part")]
	public List<int> ValidEntryPoints;
	[Tooltip("Index to start joining together the ShuffledPart and the tail of the route")]
	public int SynchronizationPoint;

	/**
	 * @param list - List of waypoints to be shuffled
	 * @param StartSplitIdx - Index inside the list to start Splitting at (0-based)
	 * @param EntryPointIdx - Index inside the list to reenter at (0-based)
	 * @param SyncIdx - Index inside the list to join shuffle together (0-based)
	 * */
	public static List<FWaypointInfo> Shuffle(IEnumerable<FWaypointInfo> list, int StartSplitIdx, int EntryPointIdx, int SyncIdx)
	{
		Assert.IsTrue(StartSplitIdx <= EntryPointIdx);
		Assert.IsTrue(EntryPointIdx <= SyncIdx);

		// Take everything in Prefix
		IEnumerable<FWaypointInfo> Prefix = list.Take(StartSplitIdx);

		// Skip till we reach EntryPointIdx & Take remaining length to Sync
		IEnumerable<FWaypointInfo> ShuffledPart = list.Skip(EntryPointIdx).Take(SyncIdx - EntryPointIdx);

		// Did we skip anything between Prefix and ShuffledPart? If so take everything up to "Reentry"
		IEnumerable<FWaypointInfo> SkippedPart = StartSplitIdx == EntryPointIdx ? new List<FWaypointInfo>() : list.Skip(StartSplitIdx).Take(EntryPointIdx - StartSplitIdx);
		IEnumerable<FWaypointInfo> Suffix = list.Skip(SyncIdx);

		List<FWaypointInfo> stitched = new List<FWaypointInfo>();
		stitched.AddRange(Prefix);
		stitched.AddRange(ShuffledPart);
		stitched.AddRange(SkippedPart);
		stitched.AddRange(Suffix);

		return stitched;
	}

	public override List<FWaypointInfo> MutateWaypoints(FRouteWaypoints Waypoints)
	{
		var seed = System.DateTime.Now.Millisecond;
		// Which route do we choose?
		UnityEngine.Random.InitState(seed);
		var EntryIdx = UnityEngine.Random.Range(0, ValidEntryPoints.Count);
		Debug.LogFormat("{0}/{1} Chosen as RoutePath {2}", EntryIdx, ValidEntryPoints.Count, seed);
		var WaypointList = FShuffledWaypointSelector.Shuffle(Waypoints.Waypoints, StartSplittingPoint, ValidEntryPoints[EntryIdx], SynchronizationPoint);

		return WaypointList;
	}
}
