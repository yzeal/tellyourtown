﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "RouteWaypoints", menuName = "Folklore/Data/Route Waypoints")]
public class FRouteWaypoints : ScriptableObject, IEnumerable
{
	public List<FWaypointInfo> Waypoints { get { return m_Waypoints; } }

	public int Count { get { return m_Waypoints.Count; } }

	[SerializeField]
	protected List<FWaypointInfo> m_Waypoints = new List<FWaypointInfo>();

	public IEnumerator GetEnumerator()
	{
		return m_Waypoints.GetEnumerator();
	}

	public void Validate()
	{
		foreach(FWaypointInfo waypoint in Waypoints) {
			waypoint.Validate();
		}
		Debug.LogFormat("Validation of {0} completed.", this);
	}
}
