﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomEditor(typeof(FRouteWaypoints), true)]
public class FRouteWaypointEditor : Editor {
	public float Y_EL_SPACING { get { return 2; } }

	private ReorderableList list;

	private FWaypointEditor elEditor;

	private void OnEnable()
	{

		elEditor = new FWaypointEditor(
			new string[] {
				"Name",
				"Location",
				"WaypointType",
				"RoutingBackground",
				"RoutingInfo",
				"Radius",
				"Game",
				"Dialog"
			}, Y_EL_SPACING);

		list = new ReorderableList(serializedObject,
						serializedObject.FindProperty("m_Waypoints"),
						true, true, true, true);

		list.drawHeaderCallback = (Rect rect) =>
		{
			EditorGUI.LabelField(rect, "Waypoints");
		};

		list.drawElementCallback =
			(Rect rect, int index, bool isActive, bool isFocused) =>
			{
				var element = list.serializedProperty.GetArrayElementAtIndex(index);
				rect.y += Y_EL_SPACING;

				elEditor.DrawDialogWaypointEditor(element, rect, index);
			}
			;

		list.elementHeightCallback =
			(int index) =>
			{
				var element = list.serializedProperty.GetArrayElementAtIndex(index);
				float totalHeight = Y_EL_SPACING;

				totalHeight += elEditor.GetElementHeight(element);

				return totalHeight;
			}
			;

	}

	public override void OnInspectorGUI()
	{
		if(GUILayout.Button(new GUIContent("Validate")))
		{
			var waypoints = (FRouteWaypoints)(target);
			waypoints.Validate();
		}
		serializedObject.Update();
		list.DoLayoutList();
		serializedObject.ApplyModifiedProperties();
	}


}
