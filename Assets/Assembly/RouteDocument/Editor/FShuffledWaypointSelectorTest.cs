using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

public class FShuffledWaypointSelectorTest
{


	[Test]
	public void TestShuffle()
	{
		List<FWaypointInfo> list = new List<FWaypointInfo>()
		{
			new FWaypointInfo() { Name = "X" }, // 0
			new FWaypointInfo() { Name = "A" }, // 1
			new FWaypointInfo() { Name = "H" }, // 2
			new FWaypointInfo() { Name = "B" }, // 3
			new FWaypointInfo() { Name = "C" }, // 4
			new FWaypointInfo() { Name = "Y" }, // 4
		};

		var Shuffle1 = FShuffledWaypointSelector.Shuffle(list, 1, 1, 5).Aggregate("", (acc, curr) => acc+curr.Name);
		var Shuffle2 = FShuffledWaypointSelector.Shuffle(list, 1, 2, 5).Aggregate("", (acc, curr) => acc + curr.Name);
		var Shuffle3 = FShuffledWaypointSelector.Shuffle(list, 1, 3, 5).Aggregate("", (acc, curr) => acc + curr.Name);
		var Shuffle4 = FShuffledWaypointSelector.Shuffle(list, 1, 4, 5).Aggregate("", (acc, curr) => acc + curr.Name);

		var NoShuffle = FShuffledWaypointSelector.Shuffle(list, 0, 0, 5).Aggregate("", (acc, curr) => acc + curr.Name);

		Assert.That(Shuffle1, Is.EqualTo("XAHBCY"));
		Assert.That(Shuffle2, Is.EqualTo("XHBCAY"));
		Assert.That(Shuffle3, Is.EqualTo("XBCAHY"));
		Assert.That(Shuffle4, Is.EqualTo("XCAHBY"));
		Assert.That(NoShuffle, Is.EqualTo("XAHBCY"));
	}
}