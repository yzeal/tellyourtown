﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class WaypointSelector : ScriptableObject {
	public abstract List<FWaypointInfo> MutateWaypoints(FRouteWaypoints Waypoints);
}
