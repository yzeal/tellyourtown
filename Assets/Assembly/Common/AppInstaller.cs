using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

[CreateAssetMenu(fileName = "AppInstaller", menuName = "Installers/AppInstaller")]
public class AppInstaller : ScriptableObjectInstaller<AppInstaller>
{
	public Settings _Settings;

	public override void InstallBindings()
	{
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		Application.runInBackground = false;

		foreach (var route in _Settings.Routes)
		{
			Container.Bind<FRouteDocument>().FromInstance(route);
		}

		Container.Bind<FDatabase>().ToSelf().AsSingle().NonLazy();

		Container.Bind<ProjectVersionHandler>().ToSelf().AsSingle();
		Container.Bind<LanguageHandler>().To<LanguageHandler>().AsSingle();

		Container.DeclareSignal<CycleAppLanguagesCommand>();
		Container.BindSignal<CycleAppLanguagesCommand>().To<LanguageHandler>(lh => new System.Action(() => lh.SetNextAppLanguage().Subscribe())).AsSingle();

		GPSInstaller.Install(Container);

		Container.DeclareSignal<RouteStateChangedSignal>();

		Container.Bind<ReplaySubject<RouteState>>().ToSelf().AsSingle();
	}


	[System.Serializable]
	public class Settings
	{
		public List<FRouteDocument> Routes;
	}
}
