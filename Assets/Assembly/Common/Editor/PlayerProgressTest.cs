﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;

public class PlayerProgressTest {
	[Test]
	public void TestSerialization() {
		// Use the Assert class to test conditions.

		InventoryItem item1 = new InventoryItem() { Count = 99, Name = ScriptableObject.CreateInstance<FName>() };
		item1.Name.Translations.Add("en", "Bucket");

		PlayerProgress pp = new PlayerProgress()
		{
			Points = 5,
			Waypoints = new List<FWaypointInfo>()
			{
				new FWaypointInfo() { Location = new FLocation() { longitude = 49, latitude = 48} }
			}
		};

		string json = pp.toJson();
		Debug.Log(json);

		PlayerProgress json2 = PlayerProgress.CreateFromJson(json);
		Debug.Log(json2);

		Assert.IsTrue(json.Equals(json2.toJson()));
	}
}
