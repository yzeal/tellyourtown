﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class FWaypointEditor
{
	string[] props;
	private readonly float Y_EL_SPACING;

	public FWaypointEditor(
		string[] props,
		float Y_EL_SPACING
		)
	{
		this.props = props;
		this.Y_EL_SPACING = Y_EL_SPACING;

	}

	public void DrawDialogWaypointEditor(SerializedProperty element, Rect rect, int index)
	{
		foreach (var prop_str in props)
		{
			var prop = element.FindPropertyRelative(prop_str);
			var desiredHeight = EditorGUI.GetPropertyHeight(prop);

			EditorGUI.PrefixLabel(
				new Rect(rect.x, rect.y, EditorGUIUtility.labelWidth, desiredHeight),
				new GUIContent(prop.displayName, ""+index));
			EditorGUI.PropertyField(
				new Rect(rect.x + EditorGUIUtility.labelWidth, rect.y, rect.width - EditorGUIUtility.labelWidth, desiredHeight),
				prop, GUIContent.none, true);

			rect.y += desiredHeight + Y_EL_SPACING;
		}
	}

	public float GetElementHeight(SerializedProperty element)
	{
		float elementHeight = 0;
		foreach (var prop_str in props)
		{
			var prop = element.FindPropertyRelative(prop_str);
			elementHeight += EditorGUI.GetPropertyHeight(prop, GUIContent.none, true) + Y_EL_SPACING;
		}
		return elementHeight;
	}
}
