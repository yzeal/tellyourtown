using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


public class OrthographicAspectRatio : MonoBehaviour
{
	[SerializeField]
	private float DesiredAspectRatio = (9.0f / 16.0f);

	private Camera bgCamera;
	[SerializeField]
	private Color pillarBoxColor = Color.black;

	public void Start()
	{

		
		// determine the game window's current aspect ratio
		float windowaspect = (float)Screen.width / (float)Screen.height;

		// current viewport height should be scaled by this amount
		// obtain camera component so we can modify its viewport
		Camera camera = GetComponent<Camera>();

		bgCamera = new GameObject("BackgroundCam", typeof(Camera)).GetComponent<Camera>();
		bgCamera.CopyFrom(camera);
		bgCamera.depth = bgCamera.depth - 1;
		bgCamera.cullingMask = 0;
		bgCamera.backgroundColor = pillarBoxColor;
		bgCamera.clearFlags = CameraClearFlags.SolidColor;

		float scalewidth = DesiredAspectRatio / windowaspect;

		Rect rect = camera.rect;

		rect.width = scalewidth;
		rect.height = 1.0f;
		rect.x = (1.0f - scalewidth) / 2.0f;
		rect.y = 0;

		camera.rect = rect;

		
	}
}