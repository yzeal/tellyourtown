using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class InventoryItem
{
	[SerializeField]
	private string m_ID;

	public string ID { get { return m_ID; } }
	public FName Name = null;

	public IntReactiveProperty p_Count = new IntReactiveProperty();
	public int Count { get { return p_Count.Value; } set { p_Count.Value = value; } }

	public Sprite Icon = null;
}