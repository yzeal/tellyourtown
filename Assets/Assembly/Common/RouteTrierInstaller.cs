using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Zenject;

public class RouteTrierInstaller : MonoInstaller<RouteTrierInstaller>
{
	readonly LocalizationBook RouteTrierTranslations = new LocalizationBook()
	{
		{ "TR_DIALOG00", new LocalizationEntry()
		{
			{ "de", "Hallo, mein Name ist Huibu!" },
			{ "en", "Hello, my name is Huibu!" }
		} }
		, { "TR_DIALOG01", new LocalizationEntry()
		{
			{ "de", "Aufwiedersehen und nicht erschrecken!" },
			{ "en", "Bye and don't be frightened!" }
		} }
	};


	public override void InstallBindings()
	{
		Container.Bind<LocalizationBook>().WithId(EContext.Route).To<LocalizationBook>().FromInstance(RouteTrierTranslations);
	}
}


