using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

public class PopupInstaller : Installer<PopupInstaller>
{
	[InjectOptional]
	PopupState popupState;

	public override void InstallBindings()
	{
		Container.BindInstance(popupState ?? new PopupState());
		Container.DeclareSignal<PopupCloseCommand>();
		Container.BindSignal<Ref<Scene>, PopupCloseCommand>().To<PopupHandler>(x => x.ClosePopup).AsSingle();
	}
}
