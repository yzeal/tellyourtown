using UnityEngine;
using System.Collections;
using UniRx;

[System.Serializable]
public class Texture2DReactiveProperty : ReactiveProperty<Texture2D>
{
}
