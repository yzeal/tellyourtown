using UnityEngine;
using Zenject;

public class LanguageSelectionInstaller : MonoInstaller<LanguageSelectionInstaller>
{
	public override void InstallBindings()
	{
		Container.Bind<MainMenuLanguageController>().ToSelf().AsSingle().NonLazy();
	}
}
