using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public static class TextUtil
{
	public static string MetricTextOfDistance(double distance)
	{
		string metricText = null;
		if (distance >= 1000.0)
		{
			metricText = Math.Round((distance / 1000.0), 2) + " km";
		}
		else
		{
			metricText = Math.Round(distance, 2) + " m";
		}
		return (metricText);
	}

	/** Returns a fixed length double string (total of 22 characters (including .) **/
	public static string FixedDoubleString(double value)
	{
		return string.Format("{0:000.000000000000000000}", value);
	}
}
