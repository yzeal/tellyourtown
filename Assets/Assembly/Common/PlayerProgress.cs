using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

[System.Serializable]
public class PlayerProgress
{
	public IntReactiveProperty p_Points = new IntReactiveProperty();
	public int Points { get { return p_Points.Value; } set { p_Points.Value = value; } }
	public List<FWaypointInfo> Waypoints;
	public List<InventoryItem> Items; 

	public FName ResourceName;
	public Sprite ResourceImage;
	public IntReactiveProperty p_ResourceCapacity = new IntReactiveProperty();
	public int ResourceCapacity { get { return p_ResourceCapacity.Value; } set { p_ResourceCapacity.Value = value; } }
	public IntReactiveProperty p_ResourceAvailable = new IntReactiveProperty();
	public int ResourceAvailable { get { return p_ResourceAvailable.Value; } set { p_ResourceAvailable.Value = value; } }
	public Color ResourceColor = Color.white;
	public AAudioCue ResourceChangeCue;
	public int version = 1;

	public void LoadFromJson(string jsonString)
	{
		PlayerProgressJson json = PlayerProgressJson.FromJson(jsonString);

		Points = json.p_Points.Value;
		Waypoints = new List<FWaypointInfo> (json.Waypoints);
		Items = new List<InventoryItem>(json.Items);
		ResourceName = json.ResourceName;
		ResourceImage = json.ResourceImage;
		ResourceCapacity = json.p_ResourceCapacity.Value;
		ResourceAvailable = json.p_ResourceAvailable.Value;
		ResourceColor = json.ResourceColor;
		version = json.version;
		ResourceChangeCue = json.ResourceChangeCue;
	}

	public static PlayerProgress CreateFromJson(string jsonString)
	{
		PlayerProgress pp = new PlayerProgress();
		pp.LoadFromJson(jsonString);
		return pp;
	}

	public string toJson()
	{
		return JsonUtility.ToJson(new PlayerProgressJson(this), false);
	}
}