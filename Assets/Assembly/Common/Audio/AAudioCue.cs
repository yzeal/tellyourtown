﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

// [CreateAssetMenu(fileName = "FName", menuName = "Folklore")]
public abstract class AAudioCue : ScriptableObject
{
	public abstract IObservable<Unit> Play(AudioSource Source);
}
