﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngineInternal;

[CreateAssetMenu(fileName ="A_Random_Cue", menuName = "Audio/RandomCue")]
public class RandomAudioCue : AAudioCue
{
	[System.Serializable]
	public class WeightedAudioClip
	{
		public AudioClip Clip;
		[Tooltip("Weight of that clip, equal values mean 1/n chance")]
		public float Weight = 1;
	}

	[Expand]
	public WeightedAudioClip[] WeightedClips;

	public RangedFloat Volume = new RangedFloat { minValue = 1, maxValue = 1 };

	[MinMaxRange(0F, 2F)]
	public RangedFloat Pitch = new RangedFloat { minValue = 1, maxValue = 1 };

	public override IObservable<Unit> Play(AudioSource Source)
	{
		float SummedWeight = 0f;
		foreach(var wc in WeightedClips)
		{
			SummedWeight += wc.Weight;
		}

		float R = Random.Range(0f, SummedWeight);
		foreach(var wc in WeightedClips)
		{
			R -= wc.Weight;
			if(R <= 0f)
			{
				Source.clip = wc.Clip;
				Source.volume = Volume.RandomValue();
				Source.pitch = Pitch.RandomValue();
				Source.Play();
				return Observable.Timer(System.TimeSpan.FromSeconds(wc.Clip.length)).AsUnitObservable();
			}
		}

		return Observable.ReturnUnit();
	}
}
