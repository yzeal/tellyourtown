﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PlayAudioCue : MonoBehaviour {

	public AAudioCue Cue;

	// Use this for initialization
	void Start () {
		var source = GetComponent<AudioSource>();
		Cue.Play(source);	
	}
}
