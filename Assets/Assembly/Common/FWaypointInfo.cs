using System;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;
using System.IO;

[System.Serializable]
public class FWaypointInfo
{
	public enum EWaypointType
	{
		Dialog,
		Game
	}

	public string Name;
	public FLocation Location;
	public EWaypointType WaypointType;
	public double Radius;
	public Sprite RoutingBackground;
	public FName RoutingInfo;

	[System.Serializable]
	public class GameSettings
	{
		public string GameScene;
	}

	[System.Serializable]
	public class DialogSettings
	{
		public ADialogNode RootNode;
	}

	public GameSettings Game;
	public DialogSettings Dialog;

	public FWaypointInfo(
		)
	{
	}

	public void Validate()
	{
#if UNITY_EDITOR
		if (RoutingInfo == null)
		{
			Debug.LogWarningFormat("{0} has no RoutingInfo.", Name);
		}
		if (WaypointType == EWaypointType.Dialog && Dialog.RootNode == null)
		{
			Debug.LogErrorFormat("{0} is set to Dialog, but has no Dialog Entry point!", Name);
		}
		var BuildSettingLevels = EditorBuildSettings.scenes
			.Where(scene => scene.enabled)
			.Select(scene => Path.GetFileNameWithoutExtension(scene.path))
			;
		if(WaypointType == EWaypointType.Game && (Game.GameScene == "" || !BuildSettingLevels.Contains(Game.GameScene))) {
			Debug.LogErrorFormat("{0} is set to Game, but GameScene is either empty or is no valid streamed level {1}!", Name, Game.GameScene);
		}
		if(RoutingBackground == null) {
			Debug.LogErrorFormat("{0} has no background set!", Name);
		}
#endif
	}
}
