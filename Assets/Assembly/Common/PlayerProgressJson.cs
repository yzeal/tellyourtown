using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class PlayerProgressJson
{
	public IntReactiveProperty p_Points;
	public List<FWaypointInfo> Waypoints; // is static asset (no need for fname serialization?)
	public FName ResourceName; // is static asset (no need for fname serialization?)
	public Sprite ResourceImage; // is static asset (no need for fname serialization?)
	public IntReactiveProperty p_ResourceCapacity;
	public IntReactiveProperty p_ResourceAvailable;
	public Color ResourceColor = Color.white;

	public List<InventoryItem> Items;
	public int version;
	public AAudioCue ResourceChangeCue;

	public PlayerProgressJson(
		PlayerProgress pp
		)
	{
		this.p_Points = pp.p_Points;
		this.Waypoints = pp.Waypoints;
		this.Items = new List<InventoryItem>(pp.Items);
		this.p_ResourceCapacity = pp.p_ResourceCapacity;
		this.p_ResourceAvailable = pp.p_ResourceAvailable;
		this.ResourceColor = pp.ResourceColor;
		this.ResourceName = pp.ResourceName;
		this.ResourceImage = pp.ResourceImage;
		this.version = pp.version;
		this.ResourceChangeCue = pp.ResourceChangeCue;
	}

	public static PlayerProgressJson FromJson(string json)
	{
		return JsonUtility.FromJson<PlayerProgressJson>(json);
	}
}