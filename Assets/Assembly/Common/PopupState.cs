using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PopupState 
{
	// Observer f�r Popup Zustand (Auf/Zu)
	public IObserver<Unit> PopupObserver;
	

	public PopupState()
	{
		PopupObserver = Observer.Create<Unit>(_ => { });
	}

}
