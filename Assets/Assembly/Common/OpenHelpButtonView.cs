using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UniRx;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Button))]
public class OpenHelpButtonView : MonoBehaviour
{
	[Inject]
	void Construct(
		ZenjectSceneLoader zsl
		)
	{
		var btn = GetComponent<Button>();
		btn.BindToOnClick(_ => Observable.Create<Unit>(
			(observer) => 
			{

				var load = zsl.LoadSceneAsync("HelpPopup", LoadSceneMode.Additive, (container) =>
				{
					// container.Bind<RouteContext.State>().FromInstance(routeContext).AsSingle().WhenInjectedInto<RouteInfoScreenInstaller>();
					container.Bind<PopupState>().FromInstance(new PopupState()
					{
						PopupObserver = observer
					}).AsSingle().WhenInjectedInto<PopupInstaller>();
				}, LoadSceneRelationship.Child);

				return Disposable.Create(() => { });
			})
			.AsUnitObservable()
		)
		.AddTo(gameObject)
		;



	}
}
