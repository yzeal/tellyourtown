using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UniRx;
using UnityEngine.SceneManagement;

public class PopupCloseButtonView : MonoBehaviour
{

	[Inject]
	void Construct(
		PopupCloseCommand cmd
		)
	{
		GetComponent<Button>().OnClickAsObservable()
			.Subscribe(x =>
			{
				cmd.Fire(gameObject.scene);
			})
			.AddTo(gameObject)
			;
	}
}
