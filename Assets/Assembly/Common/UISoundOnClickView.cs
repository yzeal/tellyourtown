using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UISoundOnClickView : MonoBehaviour
{
	public AAudioCue OnClickSound;

	[Inject]
	public void Construct(
		AudioSource uiaudio
		)
	{
		GetComponent<Button>().OnClickAsObservable()
			.Subscribe(_ =>
			{
				if(enabled)
				{
					var audio = GetComponent<AudioSource>();
					OnClickSound.Play(audio ? audio : uiaudio);
				}
			})
			.AddTo(this.gameObject)
			;
	}
}