using Zenject;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class FDatabase 
{
	private Dictionary<string, FRouteDocument> routes = new Dictionary<string, FRouteDocument>();

	[Inject]
	public FDatabase(
		List<FRouteDocument> rds
		)
	{
		foreach(var rd in rds)
		{
			routes[rd.handle] = rd;
			bool CreateNew = true;
			// Test for progress, but also check if versions match inside, otherwise we create a new copy
			if(PlayerPrefs.HasKey(rd.handle))
			{
				var loadedProgress = routeProgress[rd.handle] = PlayerProgress.CreateFromJson(PlayerPrefs.GetString(rd.handle));
				if(loadedProgress.version == rd.m_DefaultProgress.version)
				{
					CreateNew = false;
				} 
			}
			if(CreateNew)
			{
				routeProgress[rd.handle] = rd.CopyPlayerProgressDefault();
			}
		}
	}

	public LocalizationBook getLocalization(string routename)
	{
		LocalizationBook book = null; 
		if(routes.ContainsKey(routename))
		{
			book = routes[routename].localization;
		}
		return (book);
	}

	public FRouteWaypoints getWaypoints(string routename)
	{
		FRouteWaypoints waypoints = null;
		if (routes.ContainsKey(routename))
		{
			waypoints = routes[routename].Waypoints;
		}
		return (waypoints);
	}

	public FRouteDocument getRoute(string routename)
	{
		FRouteDocument document = null;
		if(routes.ContainsKey(routename))
		{
			document = routes[routename];
		}
		return (document);
	}


	Dictionary<string, PlayerProgress> routeProgress = new Dictionary<string, PlayerProgress>();

	public PlayerProgress getPlayerProgress(string routename)
	{
		PlayerProgress progress = null;
		if (routeProgress.ContainsKey(routename))
		{
			progress = routeProgress[routename];
		}
		return (progress);
	}

	public bool storePlayerProgress(string routename)
	{
		PlayerProgress progress = null;
		if (routeProgress.ContainsKey(routename))
		{
			progress = routeProgress[routename];
		}
		else
		{
			return false;
		}
		PlayerPrefs.SetString(routename, progress.toJson());
		PlayerPrefs.Save();
		return true;
	}

	public void OverwritePlayerProgress(string routename, PlayerProgress pp)
	{
		routeProgress[routename] = pp;
	}
}
