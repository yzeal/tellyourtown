using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class DialogOptionButtonView : MonoBehaviour
{
	[Inject]
	public void Construct(
		DialogOptionNode AssociatedNode,
		DialogHandler dialogHandler,
		AudioSource uiaudio,
		DialogInstaller.Settings dsettings
		)
	{
		AssociatedNode.OnEnter();
		GetComponent<Button>().BindToOnClick(_ =>
		{
			return dsettings.OptionSelectCue.Play(uiaudio)
				.SelectMany(_1 =>
				{
					if (AssociatedNode.NextNode != null)
					{
						dialogHandler.CurrentActiveNode = AssociatedNode.NextNode;
						for (int i = 0; i < this.transform.parent.childCount; ++i)
						{
							Destroy(this.transform.parent.GetChild(i).gameObject);
						}
					}
					else
					{
						dialogHandler.EndOfDialogueReached();
					}
					return Observable.ReturnUnit();
				});
				
			/*
			return Observable.Create<Unit>(observer =>
			{
				if(AssociatedNode.NextNode != null)
				{
					dsettings.OptionSelectCue.Play(uiaudio)
						.Subscribe(_1 =>
						{
							dialogHandler.CurrentActiveNode = AssociatedNode.NextNode;
							for(int i=0;i< this.transform.parent.childCount;++i)
							{
								Destroy(this.transform.parent.GetChild(i).gameObject);
							}
						}).AddTo(gameObject)
						;
				}
				else
				{
					dialogHandler.EndOfDialogueReached();
				}

				return Disposable.Create(() =>
				{
					AssociatedNode.Execute();
				});
			});
			*/
		})
		.AddTo(this.gameObject)
		;

	}

	public class Factory : Factory<DialogOptionNode, DialogOptionButtonView>
	{
	}
}