using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UniRx;
using System.Linq;
using System;

[RequireComponent(typeof(Text))]
public class DialogTextView : MonoBehaviour
{
	private AdvanceDialogCommand advanceSignal;
	private AllDialogPartsShownSignal allShownSignal;

	[Inject]
	public void Construct(
		DialogInstaller.State state
		, Localizer l
		, LanguageHandler lh
		, AdvanceDialogCommand advanceSignal
		, AllDialogPartsShownSignal allShownSignal
		)
	{
		this.allShownSignal = allShownSignal;
		this.advanceSignal = advanceSignal;
		var uiText = GetComponent<Text>();

		// @todo improve with combineLatest

		state.p_DialogTextAsset
			.Where(x => x!=null)
			// TODO: handle localization change here ?
			.Select(x => l.tr(x))
			.SelectMany(x => SplitTextAtMark(x))
			.SubscribeToText(uiText)
			.AddTo(gameObject)
			;

		//lh.p_CurrentLang
		//	.Select(lang => l.tr(state.DialogTextAsset))
		//	.SelectMany(x => SplitTextAtMark(x))
		//	.SubscribeToText(uiText)
		//	.AddTo(gameObject)
		//	;
	}

	IObservable<string> SplitTextAtMark(string text)
	{
		return Observable.Create<string>((observer) =>
		{
			var splittedText = text.Split(new string[] { "###" }, StringSplitOptions.None);
			observer.OnNext(splittedText[0]);

			IObservable<string> textStream = Observable.Return(splittedText.Skip(1)).SelectMany(x => x);

			return Observable.Zip(
					textStream,
					this.advanceSignal.AsObservable,
					(l, r) => l
				)
				.Do(x => print(x))
				.Subscribe(s => observer.OnNext(s), onCompleted: () =>
				{
					observer.OnCompleted();
				})
				;
		})
		.Finally(() => this.allShownSignal.Fire())
		;
	}

}
