using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

public class DialogHandler : IInitializable, IDisposable
{
	CompositeDisposable _disposables = new CompositeDisposable();

	ReactiveProperty<ADialogNode> p_CurrentActiveNode = new ReactiveProperty<ADialogNode>();
	public ADialogNode CurrentActiveNode { get { return p_CurrentActiveNode.Value; } set { p_CurrentActiveNode.Value = value; } }

	private ZenjectSceneLoader sceneLoad;
	private RouteState currentRoutingState;
	private ALocationHandler locationHandler;
	private FContentSolvedSignal signal;
	private readonly DialogOption.Factory DialogOptionFactory;
	private DialogInstaller.Settings dialogSettings;
	private AudioSource uiaudio;

	[Inject]
	public DialogHandler(
		ZenjectSceneLoader zsl,
		RouteState currentRoutingState,
		ALocationHandler alh,
		FContentSolvedSignal signal,
		DialogOption.Factory OptionFactory,
		DialogInstaller.Settings dsettings,
		AudioSource uiaudio,
		ADialogNode DialogueGraph = null
		)
	{
		this.uiaudio = uiaudio;
		this.dialogSettings = dsettings;
		this.DialogOptionFactory = OptionFactory;
		this.locationHandler = alh;
		this.sceneLoad = zsl;
		this.currentRoutingState = currentRoutingState;

		this.signal = signal;

		CurrentActiveNode = DialogueGraph;
	}

	public void AdvanceDialog()
	{
		CurrentActiveNode.Execute();
	}

	public void EndOfDialogueReached()
	{
		signal.Fire();
	}

	public void Initialize()
	{
		if (CurrentActiveNode)
		{
			CurrentActiveNode.OnEnter();
		}

		p_CurrentActiveNode
			.Subscribe(newNode =>
			{
				if(newNode)
				{
					newNode.OnEnter();
				}
				else
				{
					EndOfDialogueReached();
					dialogSettings.DialogAmEndeCue.Play(uiaudio);
				}
			})
			.AddTo(_disposables)
			;
	}

	public void AddOption(DialogOptionNode opt)
	{
		this.DialogOptionFactory.Create(opt);
	}

	public void Dispose()
	{
		_disposables.Clear();
	}
}
