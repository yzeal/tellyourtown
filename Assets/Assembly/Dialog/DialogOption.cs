using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

/**
 * Facade for DialogOption Buttons
 * */
public class DialogOption : MonoBehaviour
{
	[Inject]
	public void Construct(
		
		)
	{
	}

	public class Factory : Factory<DialogOptionNode, DialogOption> { }
}