using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class DialogOptionTextView : MonoBehaviour
{
	[Inject]
	public void Construct(
		Localizer l,
		DialogOptionNode AssociatedNode
		)
	{
		l.trAsObservable(AssociatedNode.OptionTextAsset)
			.Select(localizedString => localizedString.Replace("###", "\n"))
			.SubscribeToText(GetComponent<Text>())
			.AddTo(gameObject)
			;
	}
}