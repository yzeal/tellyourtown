using UnityEngine;
using Zenject;

public class DialogOptionInstaller : MonoInstaller<DialogOptionInstaller>
{
	[Inject]
	DialogOptionNode AssociatedNode = null;

	public override void InstallBindings()
	{
		Container.BindInstance(AssociatedNode);

	}
}