using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Zenject;
using System;
using UniRx;

public class DialogBackgroundView : IDisposable {
	readonly Image backgroundImage;
	CompositeDisposable disposeables = new CompositeDisposable();


	[Inject]
	public DialogBackgroundView(
		[Inject(Id = "background-image")] Image backgroundImage,
		Image personImage,
		DialogInstaller.State dialogState
		)
	{
		this.backgroundImage = backgroundImage;

		dialogState.p_DialogBackground
			.Where(newBg => newBg != null)
			.Subscribe((newBg) =>
			{
				backgroundImage.sprite = CreateSpriteFromTexture2D(newBg);
				backgroundImage.color = Color.white;
				backgroundImage.type = Image.Type.Simple;
				backgroundImage.preserveAspect = true;
			})
			.AddTo(disposeables)
			;
	}

	// TODO: Refactor into Utils Class
	public static Sprite CreateSpriteFromTexture2D(Texture2D tex)
	{
		return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(tex.width*0.5f, tex.height*0.5f), 100.0f, 0, SpriteMeshType.FullRect);
	}

	public void Dispose()
	{
		Sprite.Destroy(backgroundImage.sprite);
		disposeables.Dispose();
	}
}
