﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

[RequireComponent(typeof(Image))]
public class DialogPartnerImageView : MonoBehaviour
{
	[Inject]
	void Construct(
		DialogInstaller.State dialogState
		)
	{
		

		dialogState.p_DialogPartner
			.Where(newPartnerImage => newPartnerImage != null)
			.Subscribe((newPartnerImage) =>
			{
				var imageComp = GetComponent<Image>();
				imageComp.sprite = DialogBackgroundView.CreateSpriteFromTexture2D(newPartnerImage);
				imageComp.color = Color.white;
				imageComp.type = Image.Type.Simple;
				imageComp.preserveAspect = true;
			})
			.AddTo(gameObject)
			;
	}

	private void OnDestroy()
	{
		Sprite.Destroy(GetComponent<Image>().sprite);
	}
}
