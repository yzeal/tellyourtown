using UnityEngine;
using Zenject;
using UniRx;
using System.Collections.Generic;

public class DialogInstaller : MonoInstaller<DialogInstaller>
{
	[InjectOptional]
	public State DialogState;

	[InjectOptional]
	public ADialogNode DialogueRootNode;

	[SerializeField]
	private Settings _Settings;

	public override void InstallBindings()
	{
		RoutingInstaller.Install(Container);

		Container.DeclareSignal<AdvanceDialogCommand>();
		Container.DeclareSignal<AllDialogPartsShownSignal>();

		Container.BindSignal<AllDialogPartsShownSignal>().To<DialogHandler>(h => h.AdvanceDialog).AsSingle();

		Container.DeclareSignal<FContentSolvedSignal>();

		Container.Bind<ContentSolvedHandler_RouteToNextWaypoint>().AsSingle().NonLazy();
		Container.BindInterfacesAndSelfTo<ContentSolvedHandler_RouteToNextWaypoint>().AsSingle();

		Container.Bind<ADialogNode>().FromInstance(DialogueRootNode).AsSingle();

		Container.Bind<DialogHandler>().AsSingle().NonLazy();
		Container.Bind<IInitializable>().To<DialogHandler>().AsSingle();

		Container.Bind<DialogInstaller.State>().FromInstance(DialogState);

		Container.BindInstance(_Settings);

		Container.BindFactory<DialogOptionNode, DialogOption, DialogOption.Factory>().FromSubContainerResolve().ByNewPrefab<DialogOptionInstaller>(_Settings.DialogOptionViewPrefab).UnderTransform(_Settings.DialogOptionParent);

		DialogueRootNode.Initialize();

		Container.QueueForInject(DialogueRootNode);
	}

	// @todo rename to DialogViewModel
	[System.Serializable]
	public class State
	{
		public FNameReactiveProperty p_DialogTextAsset;
		public StringReactiveProperty p_PersonName;
		public Texture2DReactiveProperty p_DialogBackground;
		public Texture2DReactiveProperty p_DialogPartner;


		public FName DialogTextAsset { get { return p_DialogTextAsset.Value; } set { p_DialogTextAsset.Value = value; } }
		public string PersonName { get { return p_PersonName.Value; } set { p_PersonName.Value = value; } }
		public Texture2D DialogBackground { get { return p_DialogBackground.Value; } set { p_DialogBackground.Value = value; } }
		public Texture2D DialogPartner{ get { return p_DialogPartner.Value; } set { p_DialogPartner.Value = value; } }
	}

	[System.Serializable]
	public class Settings
	{
		public GameObject DialogOptionViewPrefab;
		public Transform DialogOptionParent;
		public AAudioCue OptionSelectCue;
		public AAudioCue DialogWeiterCue;
		public AAudioCue DialogAmEndeCue;
	}
}

