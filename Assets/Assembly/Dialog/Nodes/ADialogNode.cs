using System;
using UnityEngine;
using Zenject;

[System.Serializable]
public abstract class ADialogNode : ScriptableObject, IInitializable
{
	[System.NonSerialized]
	protected bool wasInjected = false;

	protected virtual void OnEnable()
	{
		wasInjected = false;
	}

	public virtual void OnEnter() { }
	public abstract void Execute();
	public virtual void OnExit() { }

	public virtual void Initialize()
	{
		OnEnable();
	}
}
