using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zenject;

[System.Serializable]
public class DialogOptionNode : ADialogNode
{
	public FName OptionTextAsset;
	public ADialogNode NextNode = null;

	[System.NonSerialized]
	private DialogInstaller.State m_state;

	protected override void OnEnable()
	{
		base.OnEnable();

		m_state = null;
	}

	[Inject]
	void Initialize(
		DiContainer container,
		DialogInstaller.State state
		)
	{
		if(!wasInjected)
		{
			wasInjected = true;
			if (NextNode)
			{
				container.Inject(NextNode);
			}
			m_state = state;
		}
	}

	public override void OnEnter()
	{
	}

	public override void Execute()
	{
		OnExit();
	}

	public override void OnExit()
	{
		// TODO: Destroy views here.

		// Enter next dialogue
	}

}

