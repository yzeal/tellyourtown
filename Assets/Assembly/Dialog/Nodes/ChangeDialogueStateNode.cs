﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "ChangeDialogStateNode", menuName = "Folklore/Dialog/Change Dialog State Node")]
public class ChangeDialogueStateNode : ADialogNode {

	public DialogInstaller.State ReplaceRouteState;
	public FName LocalizedPersonName;
	public ADialogNode NextNode;

	[System.NonSerialized]
	private DialogInstaller.State currentDialogState;
	[System.NonSerialized]
	private DialogHandler dialogHandler;
	private Localizer localizer;

	public override void OnEnter()
	{
		base.OnEnter();

		currentDialogState.DialogBackground = ReplaceRouteState.DialogBackground ?? currentDialogState.DialogBackground;
		currentDialogState.DialogPartner = ReplaceRouteState.DialogPartner ?? currentDialogState.DialogPartner;
		currentDialogState.PersonName = ReplaceRouteState.PersonName != "" ? ReplaceRouteState.PersonName : currentDialogState.PersonName;
		if(LocalizedPersonName != null)
		{
			currentDialogState.PersonName = localizer.tr(LocalizedPersonName);
			// TODO: Remove once we fully added LocalizedPersonName
		}
		currentDialogState.DialogTextAsset = ReplaceRouteState.DialogTextAsset ?? currentDialogState.DialogTextAsset;

		dialogHandler.CurrentActiveNode = NextNode;
		dialogHandler.CurrentActiveNode.OnEnter();
	}

	public override void Execute()
	{
	}

	[Inject]
	void Construct(
		DiContainer container,
		DialogInstaller.State currentDialogState,
		DialogHandler dialogHandler,
		Localizer l
		)
	{
		this.localizer = l;
		this.dialogHandler = dialogHandler;
		this.currentDialogState = currentDialogState;

		{
			wasInjected = true;
			if (NextNode)
			{
				container.QueueForInject(NextNode);
			}
		}
	}

}
