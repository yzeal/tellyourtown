using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zenject;

[CreateAssetMenu(fileName = "Dialog", menuName = "Folklore/Dialog/Text Option Node")]
public class DialogNode : ADialogNode
{
	public FName DialogTextAsset;
	public List<DialogOptionNode> Options;

	[System.NonSerialized]
	DialogInstaller.State m_State;
	[System.NonSerialized]
	private DialogHandler dialogHandler;
	[System.NonSerialized]
	private DebugAdvanceDialogueButtonView advanceView;

	protected override void OnEnable()
	{
		base.OnEnable();
		if (Options == null)
			Options = new List<DialogOptionNode>();

		this.dialogHandler = null;
		this.advanceView = null;
		this.m_State = null;
	}

	[Inject]
	public void Construct(
		DiContainer container,
		DialogInstaller.State state,
		DebugAdvanceDialogueButtonView view,
		DialogHandler dialogHandler
		)
	{
		this.dialogHandler = dialogHandler;
		this.advanceView = view;
		{
			wasInjected = true;

			m_State = state;

			foreach(var opt in Options)
			{
				container.QueueForInject(opt);
			}
		}
	}

	public override void OnEnter()
	{
		// m_State.DialogText = DialogTextTag;
		m_State.DialogTextAsset = DialogTextAsset;
		advanceView.Enabled = true;
	}

	public override void Execute()
	{
		// Get's called on user input
		// End of text reached.
		advanceView.Enabled = false;
		OnExit();
	}

	public override void OnExit()
	{
		// TODO: Handle Dialog Ende properly
		if (Options.Count == 0) // if there are no options - just go to next scene
		{
			dialogHandler.EndOfDialogueReached();
		}
		else
		{
			foreach(var opt in this.Options)
			{
				dialogHandler.AddOption(opt);
			}

			//dialogHandler.CurrentActiveNode = Options[0].NextNode;
			//dialogHandler.CurrentActiveNode.OnEnter();
		}
	}
}
