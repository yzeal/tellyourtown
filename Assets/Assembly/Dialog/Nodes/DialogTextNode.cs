using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Dialog", menuName = "Folklore/Dialog/Single Text Node")]
public class DialogTextNode : ADialogNode
{
	public FName DialogTextAsset;
	public ADialogNode NextNode;

	[System.NonSerialized]
	DialogInstaller.State m_State;
	[System.NonSerialized]
	private DialogHandler dialogHandler;
	[System.NonSerialized]
	private DebugAdvanceDialogueButtonView advanceView;

	protected override void OnEnable()
	{
		base.OnEnable();

		this.dialogHandler = null;
		this.advanceView = null;
		this.m_State = null;
	}

	[Inject]
	public void Construct(
		DiContainer container,
		DialogInstaller.State state,
		DebugAdvanceDialogueButtonView view,
		DialogHandler dialogHandler
		)
	{
		this.dialogHandler = dialogHandler;
		this.advanceView = view;
		{
			wasInjected = true;
			if (NextNode)
			{
				container.QueueForInject(NextNode);
			}
			m_State = state;

		}
	}

	public override void OnEnter()
	{
		// m_State.DialogText = DialogTextTag;
		m_State.DialogTextAsset = DialogTextAsset;
		advanceView.Enabled = true;
	}

	public override void Execute()
	{
		// Get's called on user input

		// End of text reached.
		OnExit();
	}

	public override void OnExit()
	{
		dialogHandler.CurrentActiveNode = NextNode;
	}
}