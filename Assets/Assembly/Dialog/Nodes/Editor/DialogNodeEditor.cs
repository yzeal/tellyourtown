﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomEditor(typeof(DialogNode), true)]
public class DialogNodeEditor : Editor
{
	public float Y_EL_SPACING { get { return 2; } }


	private ReorderableList list;

	private void OnEnable()
	{
		list = new ReorderableList(serializedObject,
						serializedObject.FindProperty("Options"),
						true, true, true, true);

		list.drawHeaderCallback = (Rect rect) =>
		{
			EditorGUI.LabelField(rect, "Dialog Options");
		};

		list.drawElementCallback =
			(Rect rect, int index, bool isActive, bool isFocused) =>
			{
				var element = list.serializedProperty.GetArrayElementAtIndex(index);
				rect.y += Y_EL_SPACING;

				SerializedObject propObj = new SerializedObject(element.objectReferenceValue);

				var optionText = propObj.FindProperty("OptionTextAsset");
				var next = propObj.FindProperty("NextNode");


				var desiredHeight = EditorGUI.GetPropertyHeight(optionText);

				EditorGUI.ObjectField(
					new Rect(rect.x, rect.y, rect.width, desiredHeight),
					optionText, typeof(FName));

				rect.y += desiredHeight + Y_EL_SPACING;

				desiredHeight = EditorGUI.GetPropertyHeight(next);

				EditorGUI.ObjectField(
					new Rect(rect.x, rect.y, rect.width, desiredHeight),
					next, typeof(ADialogNode));


				rect.y += desiredHeight + Y_EL_SPACING;
				propObj.ApplyModifiedProperties();
			}
			;

		list.elementHeightCallback =
			(int index) =>
			{
				var element = list.serializedProperty.GetArrayElementAtIndex(index);
				float totalHeight = Y_EL_SPACING;

				SerializedObject propObj = new SerializedObject(element.objectReferenceValue);
				var optionText = propObj.FindProperty("OptionTextAsset");
				var next = propObj.FindProperty("NextNode");

				totalHeight += EditorGUI.GetPropertyHeight(optionText, GUIContent.none, true) + Y_EL_SPACING;

				totalHeight += EditorGUI.GetPropertyHeight(next, GUIContent.none, true) + Y_EL_SPACING;

				return totalHeight;
			}
			;

		list.onAddCallback =
			(ReorderableList l) =>
			{
				var obj = (DialogNode)serializedObject.targetObject;
				var NewOption = CreateInstance<DialogOptionNode>();
				//NewOption.hideFlags = HideFlags.HideInHierarchy;
				AssetDatabase.AddObjectToAsset(NewOption, serializedObject.targetObject);
				NewOption.name = ""+NewOption.GetInstanceID();
				AssetDatabase.SaveAssets();


				obj.Options.Add(NewOption);
			}
			;

		list.onRemoveCallback =
			(ReorderableList l) =>
			{
				if (!Event.current.shift) // Shift is not held
				{
					if (EditorUtility.DisplayDialog("Warning! (Shift+Click to suppress)", "Are you sure you want to delete the option?", "Yes", "No"))
					{
						RemoveDialogOption(list.serializedProperty.GetArrayElementAtIndex(l.index));
					}
				}
				else
				{
					RemoveDialogOption(list.serializedProperty.GetArrayElementAtIndex(l.index));
				}
			}
			;
	}

	protected void RemoveDialogOption(SerializedProperty p)
	{
		DialogOptionNode n = (DialogOptionNode)p.objectReferenceValue;

		var obj = (DialogNode)serializedObject.targetObject;
		obj.Options.Remove(n);

		DestroyImmediate(p.objectReferenceValue, true);
		AssetDatabase.SaveAssets();
	}

	public override void OnInspectorGUI()
	{
		serializedObject.Update();
		Editor.DrawPropertiesExcluding(serializedObject, new string[] { "Options" });
		list.DoLayoutList();
		serializedObject.ApplyModifiedProperties();
	}
}
