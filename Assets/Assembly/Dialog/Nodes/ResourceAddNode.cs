using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "ResourceAdd", menuName = "Folklore/Dialog/Add Resource Node")]
public class ResourceAddNode : ADialogNode
{
	public int ResourcesAdded = 0;
	public int ResourceCapacityAdded = 0;
	public ADialogNode NextNode;

	CompositeDisposable disposables = new CompositeDisposable();

	private DialogHandler dialogHandler;
	private DialogInstaller.State currentDialogState;
	private AudioSource uiaudio;
	private PlayerProgress playerProgress;

	[Inject]
	public void Construct(
		DiContainer container,
		DialogInstaller.State state,
		DialogHandler dialogHandler,
		PlayerProgress pprogress,
		AudioSource uiaudio
		)
	{
		this.playerProgress = pprogress;
		this.dialogHandler = dialogHandler;
		this.currentDialogState = state;
		this.uiaudio = uiaudio;

		if(NextNode) container.QueueForInject(NextNode);
	}

	public override void OnEnter()
	{
		base.OnEnter();
		this.playerProgress.ResourceCapacity = Mathf.Max(this.playerProgress.ResourceCapacity + ResourceCapacityAdded, 0);
		this.playerProgress.ResourceAvailable = Mathf.Clamp(this.playerProgress.ResourceAvailable + ResourcesAdded, 0, this.playerProgress.ResourceCapacity);

		if(this.playerProgress.ResourceChangeCue)
		{
			this.playerProgress.ResourceChangeCue.Play(this.uiaudio)
				.Subscribe(_ =>
				{
					TriggerDialogAdvance();
				}).AddTo(disposables);
		}
		else
		{
			TriggerDialogAdvance();
		}

	}

	protected void TriggerDialogAdvance()
	{
		if (NextNode)
		{

			dialogHandler.CurrentActiveNode = NextNode;
		}
		else
		{
			dialogHandler.EndOfDialogueReached();
		}
	}

	public override void Execute()
	{
		throw new NotImplementedException();
	}

	public override void OnExit()
	{
		disposables.Clear();
	}
}