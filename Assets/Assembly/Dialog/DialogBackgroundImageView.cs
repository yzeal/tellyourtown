﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

[RequireComponent(typeof(Image))]
public class DialogBackgroundImageView : MonoBehaviour
{
	[Inject]
	void Construct(
		 DialogInstaller.State dialogState
		 )
	{
		dialogState.p_DialogBackground
			.Where(newBG => newBG != null)
			.Subscribe((newBg) =>
			{
				var imageComponent = GetComponent<Image>();
				imageComponent.sprite = DialogBackgroundView.CreateSpriteFromTexture2D(newBg);
				imageComponent.color = Color.white;
				imageComponent.type = Image.Type.Simple;
				imageComponent.preserveAspect = false;
			})
			.AddTo(gameObject)
			;
	}

	private void OnDestroy()
	{
		Sprite.Destroy(GetComponent<Image>().sprite);
	}
}
