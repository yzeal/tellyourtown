using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zenject;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

class DialogPersonNameView 
{
	[Inject]
	public DialogPersonNameView(
		[Inject(Id = "person-name")] LocalizedText personNameUi
		, DialogInstaller.State dialogState
		, Localizer l
		, LanguageHandler lh
		)
	{
		dialogState.p_PersonName
			.Select(t => l.tr(t))
			.SubscribeToText(personNameUi)
			.AddTo(personNameUi)
			;
		lh.p_CurrentLang
			.Select(lang => l.tr(dialogState.PersonName))
			.SubscribeToText(personNameUi)
			.AddTo(personNameUi)
			;
	}
}
