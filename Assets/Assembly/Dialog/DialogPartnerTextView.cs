﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

[RequireComponent(typeof(Text))]
public class DialogPartnerTextView : MonoBehaviour
{
	[Inject]
	void Construct(
		DialogInstaller.State dialogState
		, Localizer l
		)
	{
		var personName = GetComponent<Text>();

		dialogState.p_PersonName
			.Select(t => l.tr(t))
			.SubscribeToText(personName)
			.AddTo(gameObject)
			;
	}
}
