﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class Vogel : MonoBehaviour {

    public float speed = 3f;
    public float fleeSpeed = 6f;
    public float positionRangeUp = 4f;
    public float positionRangeDown = 3f;

    public Transform vogelDockingStation;
    public Transform vogelWaypoint; //hacky

	public ReactiveProperty<VogelState> p_CurrentState = new ReactiveProperty<VogelState>(VogelState.SLEEPING);
    public VogelState currentState { get { return p_CurrentState.Value; } set { p_CurrentState.Value = value; } }

    public float minTimeBetweenBirds = 1f; //Sekunden
    public float maxTimeBetweenBirds = 3f;

    public float birdLeavesAfterTime = 5f; //Sekunden
    private float currentBirdLeavesAfterTime = 0f;

    private Vector3 vogelStartPosition = Vector3.zero;

    private DragAndDropPinoepel_neu pinoepel;

    private Vector2 mousePosition = Vector2.zero;
    private bool beingDragged = false;
    private bool stoppedDrag = false;
    private bool beingDraggedSecondFinger = false;
    private bool stoppedDragSecondFinger = false;
    private bool overSprite = false;
    public SpriteRenderer spriteBox;

    public float minSwipeDistance = 3f;
    public float maxSwipeTime = 1f;

    private float currentSwipeTime = 0f;
    private Vector2 swipeStartPosition = Vector2.zero;
    private float swipeDistance = 0f;

    private bool waypointCleared = false;

    [HideInInspector]
    public Animator vogelAnimator;

	public AAudioCue FlatterndSound;
	public AAudioCue PickenSound;
	public AAudioCue VerjagtSound;

	private new AudioSource audio;

    // Use this for initialization
    void Start () {
		audio = GetComponent<AudioSource>();

        pinoepel = GameObject.FindWithTag("Pinoepel").GetComponent<DragAndDropPinoepel_neu>();
        spriteBox = GetComponentInChildren<SpriteRenderer>();

        vogelAnimator = GetComponentInChildren<Animator>();

        vogelStartPosition = transform.position;

        Invoke("RecallBird", Random.Range(minTimeBetweenBirds, maxTimeBetweenBirds));

		p_CurrentState
			.Subscribe(state =>
			{
				if(state == VogelState.SITTING)
				{
					audio.Stop();
					audio.loop = true;
					PickenSound.Play(audio);
					audio.Play();
					// Maybe delay here..
				}
				else if (state == VogelState.FLYING)
				{
					audio.Stop();
					audio.loop = true;
					FlatterndSound.Play(audio);
					audio.Play();
				}
				else if (state == VogelState.FLEEING || state == VogelState.FLEEING_EARLY)
				{
					audio.Stop();
					audio.loop = true;
					VerjagtSound.Play(audio);
					audio.Play();
				}
				else
				{
					audio.Stop();
				}
			})
			.AddTo(this.gameObject)
			;
    }
	
	// Update is called once per frame
	void Update () {

        if (!vogelWaypoint) waypointCleared = true;

        if (currentState == VogelState.FLYING && pinoepel.allowedToSlip)
        {


            transform.position = Vector3.MoveTowards(transform.position, vogelDockingStation.position, speed * Time.deltaTime);

            WaitForSwipe(currentState);

            //TODO: Animation!
        }

        if (currentState == VogelState.SITTING)
        {

			//TODO: Animation!

			WaitForSwipe(currentState);

            currentBirdLeavesAfterTime += Time.deltaTime;
            if (currentBirdLeavesAfterTime >= birdLeavesAfterTime)
            {
                FlyAway(VogelState.SITTING);
            }

            if (Random.Range(0, 100) < 50)
            {
                if(vogelAnimator) vogelAnimator.SetBool("Turn", true);
            }
            else
            {
                if (vogelAnimator) vogelAnimator.SetBool("Turn", false);
            }

        }

        if (currentState == VogelState.FLEEING)
        {

			if (!waypointCleared)
            {
                transform.position = Vector3.MoveTowards(transform.position, Vector3.Lerp(vogelStartPosition, vogelWaypoint.position, Vector3.Distance(transform.position, vogelWaypoint.position)), fleeSpeed * Time.deltaTime);
                if (Vector3.Distance(transform.position, vogelWaypoint.position) <= 1f)
                {
                    waypointCleared = true;
                }
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, vogelStartPosition, fleeSpeed * Time.deltaTime);
            }


            if (Vector3.Distance(transform.position, vogelStartPosition) <= 0.01f)
            {
                transform.position = vogelStartPosition;
                
                Invoke("RecallBird", Random.Range(minTimeBetweenBirds, maxTimeBetweenBirds));
                currentState = VogelState.SLEEPING;

                transform.position += Vector3.up * Random.Range(-positionRangeDown, positionRangeUp);

                waypointCleared = false;

                //TODO: Animation
                if (vogelAnimator)
                {
                    vogelAnimator.SetBool("Turn", false);
                    vogelAnimator.SetBool("Flee", false);
                    vogelAnimator.SetBool("Land", false);
                }
            }
        }

        if (currentState == VogelState.FLEEING_EARLY)
        {
						transform.position = Vector3.MoveTowards(transform.position, vogelStartPosition, fleeSpeed * Time.deltaTime);


            if (Vector3.Distance(transform.position, vogelStartPosition) <= 0.01f)
            {
                transform.position = vogelStartPosition;

                Invoke("RecallBird", Random.Range(minTimeBetweenBirds, maxTimeBetweenBirds));
                currentState = VogelState.SLEEPING;

                transform.position += Vector3.up * Random.Range(-positionRangeDown, positionRangeUp);


                //TODO: Animation
                if (vogelAnimator)
                {
                    vogelAnimator.SetBool("Turn", false);
                    vogelAnimator.SetBool("Flee", false);
                    vogelAnimator.SetBool("Land", false);
                }
            }
        }
    }

    private void RecallBird()
    {
        currentState = VogelState.FLYING;
        if (vogelAnimator)
        {
            vogelAnimator.SetBool("Turn", false);
            vogelAnimator.SetBool("Flee", false);
            vogelAnimator.SetBool("Land", false);
        }
	}

    private void WaitForSwipe(VogelState fromState)
    {

        // TOOD: Refactor... please

#if UNITY_EDITOR

        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        overSprite = spriteBox.bounds.Contains(mousePosition);
        //beingDragged = beingDragged && Input.GetButton("Fire1");

        if (overSprite)
        {
            if (Input.GetButton("Fire1"))
            {
                beingDragged = true;
                //Debug.Log("Vogel berührt!");

                currentSwipeTime = 0f;
                swipeStartPosition = mousePosition;
            }
        }

        if (!overSprite && !beingDragged && Input.touchCount > 1)
        {
            if (spriteBox.bounds.Contains(Input.GetTouch(1).position))
            {
                beingDraggedSecondFinger = true;
            }
        }
        else
        {
            stoppedDragSecondFinger = true;
        }

        if (beingDragged)
        {
            currentSwipeTime += Time.deltaTime;
        }

        if ((Input.GetButtonUp("Fire1") && beingDragged) || (stoppedDragSecondFinger && beingDraggedSecondFinger))
        {
            beingDragged = false;
            stoppedDragSecondFinger = false;
            beingDraggedSecondFinger = false;

            swipeDistance = Vector2.Distance(mousePosition, swipeStartPosition);

            //Debug.Log("Swipe beendet " + currentSwipeTime + "   " + swipeDistance);

            if ((swipeDistance >= minSwipeDistance && currentSwipeTime <= maxSwipeTime))
            {
                FlyAway(fromState);

                //Debug.Log("Vogel verscheucht!");
            }
        }

#else

        if(Input.touchCount > 1)
        {
            mousePosition = Camera.main.ScreenToWorldPoint(Input.GetTouch(1).position);
            overSprite = spriteBox.bounds.Contains(mousePosition);

            if (overSprite)
            {
                beingDraggedSecondFinger = true;

                currentSwipeTime = 0f;
                swipeStartPosition = mousePosition;
            }
            

            if (beingDraggedSecondFinger)
            {
                currentSwipeTime += Time.deltaTime;
            }

            
        }
        else if (Input.touchCount == 1)
        {
            if (beingDraggedSecondFinger && !stoppedDragSecondFinger)
            {
                stoppedDragSecondFinger = true;
            }

            if (stoppedDragSecondFinger && beingDraggedSecondFinger)
            {
                stoppedDragSecondFinger = false;
                beingDraggedSecondFinger = false;

                swipeDistance = Vector2.Distance(mousePosition, swipeStartPosition);                

                if (swipeDistance >= minSwipeDistance && currentSwipeTime <= maxSwipeTime)
                {
                    currentState = VogelState.FLEEING;
                    pinoepel.blocked = false;

                    transform.SetParent(null);

                    if(vogelAnimator) vogelAnimator.SetBool("Flee", true);
                    
                }
            }

            mousePosition = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            overSprite = spriteBox.bounds.Contains(mousePosition);

            if (overSprite)
            {
                
                beingDragged = true;

                currentSwipeTime = 0f;
                swipeStartPosition = mousePosition;
            }

            if (beingDragged)
            {
                currentSwipeTime += Time.deltaTime;
            }            
        }else if (Input.touchCount == 0)
        {
            if (beingDragged)
            {
                stoppedDrag = true;
            }

            if (beingDragged && stoppedDrag)
            {
                beingDragged = false;
                stoppedDrag = false;

                swipeDistance = Vector2.Distance(mousePosition, swipeStartPosition);
                

                if (swipeDistance >= minSwipeDistance && currentSwipeTime <= maxSwipeTime)
                {
                    currentState = VogelState.FLEEING;
                    pinoepel.blocked = false;

                    transform.SetParent(null);
                    if(vogelAnimator) vogelAnimator.SetBool("Flee", true);
                    
                }
            }
        }

        


#endif
	}

    public void FlyAway(VogelState fromState)
    {
        currentBirdLeavesAfterTime = 0f;

        if (fromState == VogelState.SITTING)
        {
            currentState = VogelState.FLEEING;
            if (vogelAnimator) vogelAnimator.SetBool("Flee", true);
        }
        else
        {
            currentState = VogelState.FLEEING_EARLY;
            if (vogelAnimator) vogelAnimator.SetBool("Flee", true);
        }

        pinoepel.blocked = false;

        transform.SetParent(null);
    }
}


public enum VogelState
{
    FLYING, FLEEING, SITTING, SLEEPING, FLEEING_EARLY
}
