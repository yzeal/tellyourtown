﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DragAndDropPinoepel : MonoBehaviour
{

    private bool beingDragged = false;
    private bool overSprite = false;
    public SpriteRenderer spriteBox;
    private Vector2 mousePosition = Vector2.zero;

    public Transform center;
    public Transform wheel;

    private float turnAmount;
    private float distanceToCenter = 0f;
    private float angle = 0f;
    private Vector3 correctionToCircle = Vector3.zero;

    private float lastAngle = 0f;
    [HideInInspector] public float turnAmountAgg = 0f;

    private float turnAmountMax = 10f;

    [HideInInspector] public float minTurnAmount = 0f;
    public float numUmdrehungenBisUnten = 5f;
    [HideInInspector] public float maxTurnAmount = 1800f; //5x kurbeln

    private Vector3 nextPosition = Vector3.zero;

    public GameObject radBG;
    public GameObject radBGMoving;
    public GameObject stange;
    public GameObject stangeMoving;

    public GameObject tutorialPfeil;
    private bool tutorialOver = false;


    private void Start()
    {
        maxTurnAmount = numUmdrehungenBisUnten * 360f;

        if (center)
        {
            distanceToCenter = Vector2.Distance(center.position, transform.position);
            angle = Modulo(Mathf.Atan2(transform.position.y - center.position.y, transform.position.x - center.position.x) * Mathf.Rad2Deg, 360);
        }

        if(stangeMoving) stangeMoving.SetActive(false);
        radBGMoving.SetActive(false);
    }
    
    void Update()
    {

        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        overSprite = spriteBox.bounds.Contains(mousePosition);
        beingDragged = beingDragged && Input.GetButton("Fire1");

        if (overSprite)
        {
            if (Input.GetButton("Fire1"))
            {
                beingDragged = true;
            }
        }
        if (beingDragged)
        {

            nextPosition = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0.0f);

            if (center)
            {
                lastAngle = angle;
                angle = Modulo(Mathf.Atan2(nextPosition.y - center.position.y, nextPosition.x - center.position.x) * Mathf.Rad2Deg, 360);
                correctionToCircle = Vector3.Normalize(nextPosition - center.position) * distanceToCenter;
                nextPosition = center.position + correctionToCircle;

                if (!((lastAngle < 10f && angle > 350f) || (lastAngle > 350f && angle < 10f)))
                {
                    turnAmount = lastAngle - angle;
                    if (Mathf.Abs(turnAmount) > turnAmountMax)
                    {
                        if (turnAmount > 0f)
                        {
                            turnAmount = turnAmountMax;
                        }
                        else
                        {
                            turnAmount = -turnAmountMax;
                        }
                    }

                    
                    turnAmountAgg += turnAmount;

                    if (turnAmountAgg < minTurnAmount) turnAmountAgg = minTurnAmount;
                    if (turnAmountAgg > maxTurnAmount) turnAmountAgg = maxTurnAmount;
                   

                    if (!((turnAmountAgg == minTurnAmount && turnAmount <= 0f) || (turnAmountAgg == maxTurnAmount && turnAmount >= 0f)))
                    {
                        transform.position = nextPosition;
                        wheel.rotation = Quaternion.Euler(0f, 0f, Mathf.Atan2(nextPosition.y - center.position.y, nextPosition.x - center.position.x) * Mathf.Rad2Deg);
                    }
                    else
                    {
                        nextPosition = transform.position;
                        beingDragged = false;
                        turnAmount = 0f;
                    }

                    if (turnAmount != 0f)
                    {
                        if (stange) stange.SetActive(false);
                        if (stangeMoving) stangeMoving.SetActive(true);
                        radBG.SetActive(false);
                        radBGMoving.SetActive(true);

                        if (!tutorialOver && tutorialPfeil)
                        {
                            tutorialPfeil.SetActive(false);
                        }
                    }
                    else
                    {
                        if(stange) stange.SetActive(true);
                        if (stangeMoving) stangeMoving.SetActive(false);
                        radBG.SetActive(true);
                        radBGMoving.SetActive(false);
                    }
                    
                }

                
                //Debug.Log("angle: " + angle + "  amount: " + turnAmountAgg);
            }
        }

    }

    private float Modulo(float dividend, int divisor)
    {
        float rest = dividend % divisor;
        return rest < 0 ? rest + divisor : rest;
    }
}