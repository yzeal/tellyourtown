﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EimerfuellIcon : MonoBehaviour {

    public GameObject eimerLeerSprite;
    public GameObject eimerVollSprite;

    // Use this for initialization
    void Awake () {
        eimerVollSprite.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Fuellen()
	{
		eimerVollSprite.SetActive(true);
		eimerLeerSprite.SetActive(false);
	}

	public void SetSortOrder(int order)
	{
		eimerLeerSprite.GetComponent<SpriteRenderer>().sortingOrder = order;
		eimerVollSprite.GetComponent<SpriteRenderer>().sortingOrder = order;
	}
}
