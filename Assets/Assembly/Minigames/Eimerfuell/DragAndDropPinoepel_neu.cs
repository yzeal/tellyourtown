﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DragAndDropPinoepel_neu : MonoBehaviour
{

    private bool beingDragged = false;
    private bool overSprite = false;
    //[HideInInspector]
    public bool blocked = false;

    public float slippingSpeed = 10f;
    [HideInInspector]
    public bool slipping = false;
    [HideInInspector]
    public bool allowedToSlip = false;

    public SpriteRenderer spriteBox;
    private Vector2 mousePosition = Vector2.zero;

    public Transform center;
    public Transform wheel;

    private float turnAmount;
    private float distanceToCenter = 0f;
    private float angle = 0f;
    private Vector3 correctionToCircle = Vector3.zero;

    private float lastAngle = 0f;
    [HideInInspector] public float turnAmountAgg = 0f;

    private float turnAmountMax = 10f;

    [HideInInspector] public float minTurnAmount = 0f;
    public float numUmdrehungenBisUnten = 5f;
    [HideInInspector] public float maxTurnAmount = 1800f; //5x kurbeln

    private Vector3 nextPosition = Vector3.zero;

    public GameObject radBG;
    public GameObject radBGMoving;
    public GameObject stange;
    public GameObject stangeMoving;

    public GameObject tutorialPfeil;
    private bool tutorialOver = false;

	public AAudioCue KurbelratternSound;
	public AAudioCue SlippingSound;
	private new AudioSource audio;

	private bool onEnterSlipping = true;
	private bool onEnterDrag = true;

    private void Start()
    {
		audio = GetComponent<AudioSource>();
    maxTurnAmount = numUmdrehungenBisUnten * 360f;

    if (center)
    {
        distanceToCenter = Vector2.Distance(center.position, transform.position);
        angle = Modulo(Mathf.Atan2(transform.position.y - center.position.y, transform.position.x - center.position.x) * Mathf.Rad2Deg, 360);
    }

    if (stangeMoving) stangeMoving.SetActive(false);
    radBGMoving.SetActive(false);
		audio.loop = true;
		KurbelratternSound.Play(audio);
		audio.Pause();
    }

    void Update()
    {

#if UNITY_EDITOR

        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        overSprite = spriteBox.bounds.Contains(mousePosition);
        beingDragged = beingDragged && Input.GetButton("Fire1");

        if (overSprite)
        {
            if (Input.GetButton("Fire1"))
            {
                beingDragged = true;
                slipping = false;
            }
            else if (allowedToSlip)
            {
                slipping = true;
            }
        }

        if (!overSprite && !Input.GetButton("Fire1") && allowedToSlip)
        {
            slipping = true;
				}

#else
        if (Input.touchCount > 0)
        {
            mousePosition = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            overSprite = spriteBox.bounds.Contains(mousePosition);
            if (overSprite)
            {
                beingDragged = true;
                slipping = false;
            }
            else if (allowedToSlip && !beingDragged)
            {
                slipping = true;
            }
        }
        else
        {
            beingDragged = false;
            if(allowedToSlip){
                slipping = true;
            }
        }


        if (!overSprite && !beingDragged && allowedToSlip)
        {
            slipping = true;
        }
#endif



        if (beingDragged && !slipping)
        {
					if(onEnterDrag)
			{
				onEnterSlipping = true;
				onEnterDrag = false;
				KurbelratternSound.Play(audio);
				audio.Pause();
			}

#if UNITY_EDITOR
            nextPosition = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0.0f);
#else
            nextPosition = new Vector3(Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position).x, Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position).y, 0.0f);
#endif
            if (center)
            {
                lastAngle = angle;
                angle = Modulo(Mathf.Atan2(nextPosition.y - center.position.y, nextPosition.x - center.position.x) * Mathf.Rad2Deg, 360);


                correctionToCircle = Vector3.Normalize(nextPosition - center.position) * distanceToCenter;
                nextPosition = center.position + correctionToCircle;

                if (!((lastAngle < 10f && angle > 350f) || (lastAngle > 350f && angle < 10f)))
                {
                    turnAmount = lastAngle - angle;

                    if (Mathf.Abs(turnAmount) > turnAmountMax)
                    {
                        if (turnAmount > 0f)
                        {
                            turnAmount = turnAmountMax;
                        }
                        else
                        {
                            turnAmount = -turnAmountMax;
                        }
                    }


                    turnAmountAgg += turnAmount;

                    if (turnAmountAgg < minTurnAmount) turnAmountAgg = minTurnAmount;
                    if (turnAmountAgg > maxTurnAmount) turnAmountAgg = maxTurnAmount;


                    //if (!((turnAmountAgg == minTurnAmount && turnAmount <= 0f) || (turnAmountAgg == maxTurnAmount && turnAmount >= 0f)))
                    if (!((turnAmountAgg == minTurnAmount && turnAmount <= 0f) || (turnAmountAgg == maxTurnAmount && turnAmount >= 0f) || (blocked && turnAmount <= 0f)))
                    {
                        transform.position = nextPosition;
                        wheel.rotation = Quaternion.Euler(0f, 0f, Mathf.Atan2(nextPosition.y - center.position.y, nextPosition.x - center.position.x) * Mathf.Rad2Deg);
                    }
                    else
                    {
                        nextPosition = transform.position;
                        beingDragged = false;
                        turnAmount = 0f;
                    }

                    AnimateWellMovement();
                }




                //Debug.Log("angle: " + angle + "  amount: " + turnAmountAgg);
            }
        }

        if (slipping && center)
        {
			beingDragged = false;
			if(onEnterSlipping)
			{
				onEnterSlipping = false;
				onEnterDrag = true;
				SlippingSound.Play(audio);
				print("Play slipping sound");
			}

			turnAmount = -slippingSpeed * Time.deltaTime;

            turnAmountAgg += Mathf.Abs(turnAmount);
            if (turnAmountAgg < minTurnAmount) turnAmountAgg = minTurnAmount;
            if (turnAmountAgg > maxTurnAmount) turnAmountAgg = maxTurnAmount;


            if (!(turnAmountAgg == maxTurnAmount))
            {
                //memo: eine Drehmatrix im quasi-R^2 um turnAmount Grad um den Punkt center herum: (x1 + cosA * (x - x1) - sinA * (y - y1), y1 + sinA * (x - x1) + cosA * (y - y1), 0) 
                nextPosition = new Vector3(center.position.x + Mathf.Cos(turnAmount * Mathf.Deg2Rad) * (transform.position.x - center.position.x) - Mathf.Sin(turnAmount * Mathf.Deg2Rad) * (transform.position.y - center.position.y),
                                           center.position.y + Mathf.Sin(turnAmount * Mathf.Deg2Rad) * (transform.position.x - center.position.x) + Mathf.Cos(turnAmount * Mathf.Deg2Rad) * (transform.position.y - center.position.y),
                                           0f);

                transform.position = nextPosition;
                wheel.rotation = Quaternion.Euler(0f, 0f, Mathf.Atan2(nextPosition.y - center.position.y, nextPosition.x - center.position.x) * Mathf.Rad2Deg);
            }
            else
            {
                nextPosition = transform.position;
                beingDragged = false;
                slipping = false;
                turnAmount = 0f;
            }



            AnimateWellMovement();
        }

    }

    public static float Modulo(float dividend, int divisor)
    {
        float rest = dividend % divisor;
        return rest < 0 ? rest + divisor : rest;
    }

    private void AnimateWellMovement()
    {
			if (turnAmount != 0f)
			{
				if (stange) stange.SetActive(false);
				if (stangeMoving) stangeMoving.SetActive(true);
				radBG.SetActive(false);
				radBGMoving.SetActive(true);

				if (!tutorialOver && tutorialPfeil)
				{
					tutorialPfeil.SetActive(false);
					allowedToSlip = true;
				}

				if (!audio.isPlaying)
				{
					audio.UnPause();
				}

			}
			else
			{
				if (stange) stange.SetActive(true);
				if (stangeMoving) stangeMoving.SetActive(false);
				radBG.SetActive(true);
				radBGMoving.SetActive(false);

				if (audio.isPlaying)
				{
					audio.Pause();
				}
			}
    }
}