﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UnityEngine.UI;
using System;

public class EimerBewegen_neu : MonoBehaviour
{

    public int minEimerNumberForTimer = 3;
    public float timerMaxValue = 60f; //in Sekunden

	public DragAndDropPinoepel_neu pinoepel;
	public Transform endPositionIndicator;

	public GameObject eimerLeerSprite;
	public GameObject eimerVollSprite;

	private Vector3 startPosition = Vector3.zero;
	private Vector3 endPosition = Vector3.zero;

	private bool eimerVoll = false;
	private int eimerNum = 5; //TODO: Bekommen!
	private int eimerVollNum = 0;

	public GameObject eimerfuellIcon;
	public float eimerfuellIconsDistance = 0.7f;
	public float eimerfuellIconsRowSpacing = .35f;
	public int eimerPerRow = 4;

    public Text timerText;

	private List<EimerfuellIcon> eimerIcons = new List<EimerfuellIcon>(); //TODO: private und in Start befüllen!

	private bool done = false;
	private FContentSolvedSignal OnContentSolved;
	private PlayerProgress PlayerProgress;

	private float ropeDistance = 1.033944f; //hackhack
	public Transform stange;
	private Vector3 stangePos = new Vector3(0f, 1.66f, 0f);
	private int numRopePieces = 0;
	private float distanceToTop = 0f;
	public GameObject ropePiece;
	private List<GameObject> ropePieces = new List<GameObject>();

	public Text eimerLeerText;
	public Text eimerVollText;

	public ParticleSystem wasserspritzer;

    [HideInInspector]
    public Vector3 oldPosition = Vector3.zero;
    [HideInInspector]
    public Vector3 newPosition = Vector3.zero;

    private float currentTimerValue = 0f;
    private bool startTimer = false;

	public AAudioCue EimerGefuelltSound;

	// Use this for initialization
	void Start()
	{

		startPosition = transform.position;
		if(stange) stangePos = stange.position;
		if (endPositionIndicator) endPosition = endPositionIndicator.position;
		eimerVollSprite.SetActive(false);

		if (eimerfuellIcon)
		{
			eimerfuellIcon.GetComponent<EimerfuellIcon>().SetSortOrder(0);
			eimerIcons.Add(eimerfuellIcon.GetComponent<EimerfuellIcon>());

			for (int i = 1; i < eimerNum; i++)
			{
				Vector3 spawnPosition = eimerfuellIcon.transform.position + Vector3.right * eimerfuellIconsDistance * (i % eimerPerRow);
				spawnPosition.x += ((i / eimerPerRow) % 2) * eimerfuellIconsDistance * .5f;
				spawnPosition += Vector3.down * eimerfuellIconsRowSpacing * (i / eimerPerRow);

				GameObject newIcon = GameObject.Instantiate(eimerfuellIcon, spawnPosition, Quaternion.identity);
				newIcon.GetComponent<EimerfuellIcon>().SetSortOrder(i + 20);

				if (i < eimerVollNum) { newIcon.GetComponent<EimerfuellIcon>().Fuellen(); }
				eimerIcons.Add(newIcon.GetComponent<EimerfuellIcon>());
			}
			if (eimerVollNum > 0)
			{
				eimerfuellIcon.GetComponent<EimerfuellIcon>().Fuellen();
			}
		}

		if (eimerNum == eimerVollNum)
		{
			FinishGame();
		}

		eimerVollText.text = eimerVollNum.ToString();
		if (eimerLeerText) eimerLeerText.text = (eimerNum - eimerVollNum).ToString();

        if (timerText) timerText.gameObject.SetActive(false);
        currentTimerValue = timerMaxValue;
	}

	private void FinishGame()
	{
		this.PlayerProgress.ResourceAvailable = PlayerProgress.ResourceCapacity;
		OnContentSolved.Fire();

	}

	[Inject]
	void Construct(
		FContentSolvedSignal ContentSolved,
		PlayerProgress playerProgress
		)
	{
		this.OnContentSolved = ContentSolved;

		this.PlayerProgress = playerProgress;

		eimerNum = this.PlayerProgress.ResourceCapacity;
		eimerVollNum = this.PlayerProgress.ResourceAvailable;
	}

	// Update is called once per frame
	void Update()
	{
        oldPosition = transform.position;
        newPosition = Vector3.Lerp(startPosition, endPosition, pinoepel.turnAmountAgg / (pinoepel.maxTurnAmount - pinoepel.minTurnAmount));

        //if (!(oldPosition.y < newPosition.y && pinoepel.blocked))
        //{
            transform.position = newPosition;

            distanceToTop = Vector3.Distance(startPosition, transform.position);

            if (ropePiece && stange)
            {
                if (Vector3.Distance(ropePiece.transform.position, stangePos) > ropeDistance * (ropePieces.Count + 1) / 3.6f)
                {
                    GameObject newRopePiece = Instantiate<GameObject>(ropePiece, ropePiece.transform.position, Quaternion.identity, transform);
                    ropePieces.Add(newRopePiece);
                    newRopePiece.transform.localPosition += Vector3.up * ropeDistance * ropePieces.Count;
                    newRopePiece.transform.localScale = Vector3.one * ropePiece.transform.localScale.x;
                    if (newRopePiece.transform.childCount > 0)
                    {
                        Destroy(newRopePiece.transform.GetChild(0).gameObject);
                    }

                }
                else if (Vector3.Distance(ropePiece.transform.position, stangePos) <= ropeDistance * ropePieces.Count / 3.6f)
                {
                    GameObject ropePieceToDestroy = ropePieces[ropePieces.Count - 1];
                    ropePieces.Remove(ropePieceToDestroy);
                    if (ropePieceToDestroy.transform.childCount != 0)
                    {
                        for (int i = 0; i < ropePieceToDestroy.transform.childCount; i++)
                        {
                            Transform bird = ropePieceToDestroy.transform.GetChild(i);
                        if (bird.GetComponent<Vogel>())
                            {
                                bird.SetParent(null);
                                bird.GetComponent<Vogel>().FlyAway(VogelState.SITTING);
                            }
                        }
                        
                    }
                    Destroy(ropePieceToDestroy);
                }
            }


            if (!eimerVoll && transform.position == endPosition && !done)
            {
                eimerVoll = true;
                //TODO: Sprite ändern! Evtl Sound? Animation/Partikeleffekt?
                eimerLeerSprite.SetActive(false);
                eimerVollSprite.SetActive(true);

                if (wasserspritzer) wasserspritzer.Play();
			EimerGefuelltSound.Play(GetComponent<AudioSource>());
			//Debug.Log("Eimer gefüllt!");
		}

            if (eimerVoll && transform.position == startPosition && !done)
            {
                eimerVoll = false;
                eimerVollNum++;

                //TODO: Sprite ändern! Evtl Sound? Eimeranzeige ändern (Anzahl der zu füllenden Eimer)
                eimerLeerSprite.SetActive(true);
                eimerVollSprite.SetActive(false);

                if (wasserspritzer) wasserspritzer.Play();
                if (eimerfuellIcon) eimerIcons[eimerVollNum - 1].Fuellen();

                eimerVollText.text = eimerVollNum.ToString();
                if (eimerLeerText) eimerLeerText.text = (eimerNum - eimerVollNum).ToString();

                //Debug.Log(eimerVollNum + ". Eimer aus dem Brunnen geholt!");

                if (eimerVollNum == eimerNum)
                {
                    Debug.Log("Done!");
                    done = true;
                    FinishGame();
                }
            }
        //}

        if (!startTimer && eimerVollNum >= minEimerNumberForTimer)
        {
            startTimer = true;
            if (timerText) timerText.gameObject.SetActive(true);
        }

        if (startTimer)
        {
            currentTimerValue -= Time.deltaTime;

            if (currentTimerValue <= 0f)
            {
                FinishGame();
            }
            else
            {
                int minutes = Mathf.FloorToInt(currentTimerValue / 60f);
                int seconds = Mathf.FloorToInt(currentTimerValue - minutes * 60f);
                if (timerText) timerText.text = minutes.ToString() + ":" + (seconds < 10 ? "0" + seconds.ToString() : seconds.ToString());
            }
        }

		if (Input.GetKeyUp(KeyCode.C))
		{
			FinishGame();
		}

        
	}
}
