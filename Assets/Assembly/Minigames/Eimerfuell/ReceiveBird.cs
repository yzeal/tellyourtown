﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReceiveBird : MonoBehaviour {

    private GameObject vogel;
    private DragAndDropPinoepel_neu pinoepel;

	// Use this for initialization
	void Start () {
        pinoepel = GameObject.FindWithTag("Pinoepel").GetComponent<DragAndDropPinoepel_neu>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("Vogel"))
        {
            //Debug.Log("Seiltrigger getriggert!");

            vogel = collider.gameObject;
            vogel.transform.SetParent(transform);
            vogel.GetComponent<Vogel>().currentState = VogelState.SITTING;
            if (vogel.GetComponent<Vogel>().vogelAnimator) vogel.GetComponent<Vogel>().vogelAnimator.SetBool("Land", true);

            pinoepel.blocked = true;
        }
    }


}
