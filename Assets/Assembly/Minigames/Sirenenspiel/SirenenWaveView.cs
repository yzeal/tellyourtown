using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SirenenWaveView : MonoBehaviour
{
	public SirenenPinoepel Pinoepel;

	[Tooltip("Mapt [0,1] auf Abspielgeschwindigkeit der Animation (als Multiplier). [0,1] berechnet sich aus Turn/Desired")]
	public AnimationCurve AnimationSpeedCurve;

	private Animator animator;
	private int Trigger_Animate;
	private int Trigger_Fadeout;

	private void Start()
	{
		this.animator = GetComponent<Animator>();

		this.Trigger_Animate = Animator.StringToHash("tStart");
		this.Trigger_Fadeout = Animator.StringToHash("tFadeout");


		Pinoepel.p_CurrentTurnAmountPerSecond
			.Subscribe(turn =>
			{
				float alpha = turn / Pinoepel.DesiredTurnAmountPerSecond;

				var color = GetComponent<SpriteRenderer>().color;
				color.a = 0f;
				GetComponent<SpriteRenderer>().color = color;

				animator.SetFloat("fWaveSpeed", AnimationSpeedCurve.Evaluate(alpha));

				if(turn > .1F)
				{
					animator.SetTrigger(Trigger_Animate);
				}
				else
				{
					animator.SetTrigger(Trigger_Fadeout);
				}

			})
			.AddTo(this.gameObject)
			.AddTo(Pinoepel.gameObject)
			;
	}

	[Inject]
	public void Construct(
		)
	{
		// TODO
	}
}