using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SirenenVolumeView : MonoBehaviour
{
	public SirenenPinoepel Pinoepel;
	public RectTransform ZeigerPivot;

	[Tooltip("Mapt [0,1] auf �Grad der Nadel. [0,1] berechnet sich aus Turn/Desired")]
	public AnimationCurve ZeigerRotationCurve;

	public void Start()
	{
		Pinoepel.p_CurrentTurnAmountPerSecond
			.Subscribe(turn =>
			{
				var degree = 0F;

				degree = ZeigerRotationCurve.Evaluate(turn / Pinoepel.DesiredTurnAmountPerSecond);

				ZeigerPivot.localRotation = Quaternion.Euler(0f, 0f, degree);
			})
			.AddTo(this.gameObject)
			.AddTo(Pinoepel.gameObject)
			;
	}
}