﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestruct : MonoBehaviour {

    public float delay = 1f;
    public bool onStart = true;

	// Use this for initialization
	void Start () {
        if (onStart) {
            Invoke("SelfDestruction", delay);
        }
	}
	
	private void SelfDestruction()
    {
        Destroy(gameObject);
    }

    public void InitiateDestruction()
    {
        Invoke("SelfDestruction", delay);
    }
}
