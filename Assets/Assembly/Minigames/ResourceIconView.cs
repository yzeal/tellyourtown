using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ResourceIconView : MonoBehaviour
{
	[Inject]
	public void Construct(
		PlayerProgress pprogress
		)
	{
		GetComponent<Image>().sprite = pprogress.ResourceImage;
	}
}