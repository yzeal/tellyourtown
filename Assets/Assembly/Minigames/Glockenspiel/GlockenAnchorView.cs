using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GlockenAnchorView : MonoBehaviour
{
	private GlockenspielViewModel Glockenspiel;

	[Inject]
	public void Construct(
		GlockenspielViewModel Glockenspiel
		)
	{
		this.Glockenspiel = Glockenspiel;
	}

	public void RegisterPlay()
	{
		this.Glockenspiel.Played(this);
	}
}