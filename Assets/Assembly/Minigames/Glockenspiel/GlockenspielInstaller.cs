using UnityEngine;
using Zenject;

public class GlockenspielInstaller : MonoInstaller<GlockenspielInstaller>
{
	[Expand]
	public Settings m_Settings;

	public override void InstallBindings()
	{
		Container.BindInstance(m_Settings);
		Container.Bind<GlockenspielViewModel>().ToSelf().AsSingle();
		Container.Bind<System.IDisposable>().To<GlockenspielViewModel>().AsSingle();
	}

	[System.Serializable]
	public class Settings
	{
		[Expand]
		public GlockenAnchorView[] Reihenfolge;

		public AudioSource solvedSource;
		public AAudioCue SolvedSoundCue;
	}
}