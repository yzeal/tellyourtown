	using UnityEngine;
	using System.Collections;
	using System.Linq;
	using System.Collections.Generic;
	using UniRx;
	using Zenject;

public class GlockenspielViewModel : System.IDisposable {

	CompositeDisposable disposables = new CompositeDisposable();
	public List<GlockenAnchorView> PlayedOrder;
	public GlockenspielInstaller.Settings settings;
	private readonly GlockenAnchorView[] Reihenfolge;
	private FContentSolvedSignal ContentSolved;

	public bool Solved = false;

	public GlockenspielViewModel(
		GlockenspielInstaller.Settings GlockenspielSettings,
		FContentSolvedSignal ContentSolvedSignal
		)
	{
		PlayedOrder = new List<GlockenAnchorView>();
		this.settings = GlockenspielSettings;
		this.Reihenfolge = GlockenspielSettings.Reihenfolge;
		this.ContentSolved = ContentSolvedSignal;
	}

	public void Played(GlockenAnchorView glocke)
	{
		PlayedOrder.Add(glocke);
		Debug.Log(TestOrder());
		if(!TestOrder()) // wenn falsch gespielt => 
		{
			PlayedOrder.Clear();
		}
		else
		{
			if ((PlayedOrder.Count == Reihenfolge.Length))
			{
				Solved = true;
				this.settings.SolvedSoundCue.Play(settings.solvedSource)
					.Subscribe(_ =>
					{
						ContentSolved.Fire();
						PlayedOrder.Clear();
					}).AddTo(disposables);
			}
		}
	}

	private bool TestOrder()
	{
		bool result = true;
		for(int i=0;i<PlayedOrder.Count;++i)
		{
				result &= (Reihenfolge[i] == PlayedOrder[i]);
		}
		return (result);
	}

	public void Dispose()
	{
		disposables.Clear();
	}
}
