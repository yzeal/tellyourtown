using UnityEngine;
using System.Collections;
using Zenject;

[RequireComponent(typeof(AudioSource))]
public class GlockeView : MonoBehaviour
{
	private AudioSource audioSource;
	private float audioLength;

	private bool IsSwinging = false;
	private float swingTime = 0f;
	private Quaternion originRotation;
	private GlockenspielViewModel Glockenspiel;
	private GlockenAnchorView AnchorView;

	public void Start()
	{
		audioSource = GetComponent<AudioSource>();
		audioLength = audioSource.clip.length;

		AnchorView = transform.parent.GetComponent<GlockenAnchorView>();
	}

	public void Update()
	{
		if (IsSwinging)
		{
			swingTime += Time.deltaTime;
			float freq = (2f * Mathf.PI) / audioLength;
			float a = Mathf.Sin(freq * swingTime);

			transform.localRotation = originRotation * Quaternion.AngleAxis(Mathf.Rad2Deg * a, new Vector3(-1f, 0f, 0f));

			if (swingTime > audioLength)
			{
				StopSwing();
			}
		}
	}

	[Inject]
	public void Construct(
		GlockenspielViewModel Glockenspiel
		)
	{
		this.Glockenspiel = Glockenspiel;
	}

	public void OnMouseDown()
	{
		if (!audioSource.isPlaying && !this.Glockenspiel.Solved)
		{
			audioSource.Play();
			StartSwing();
			this.AnchorView.RegisterPlay();
		}
	}

	private void StartSwing()
	{
		swingTime = 0f;
		IsSwinging = true;
		originRotation = transform.localRotation;
	}

	private void StopSwing()
	{
		IsSwinging = false;
	}
}
