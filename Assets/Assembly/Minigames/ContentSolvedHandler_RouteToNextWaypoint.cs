﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

public class ContentSolvedHandler_RouteToNextWaypoint : IDisposable
{
	private FContentSolvedSignal signal;
	private ZenjectSceneLoader sceneLoader;
	private RouteState currentRouteState;
	private ALocationHandler locationHandler;
	private FDatabase database;
	private PlayerProgress playerProgress;

	[Inject]
	public ContentSolvedHandler_RouteToNextWaypoint(
		FContentSolvedSignal signal,
		RouteState currentRouteState,
		ZenjectSceneLoader zsl,
		ALocationHandler alh,
		FDatabase database,
		PlayerProgress playerProgress
		)
	{
		this.playerProgress = playerProgress;
		this.database = database;
		this.locationHandler = alh;

		this.signal = signal;
		this.signal += OnContentSolved;

		this.currentRouteState = currentRouteState;
		this.sceneLoader = zsl;
	}

	void OnContentSolved()
	{
		this.playerProgress.Points += 1;

		if (database.storePlayerProgress(currentRouteState.name))
		{
			Debug.Log("Successfully saved progress");
		}

		if (currentRouteState.nextWaypoints.Count() == 0) // has no further content
		{
			sceneLoader.LoadSceneAsync("MainMenu", LoadSceneMode.Single);
		}
		else
		{
			sceneLoader.LoadSceneAsync("RoutingTemplate", LoadSceneMode.Single, (container) =>
			{

				var target = currentRouteState.nextWaypoints.First();
				var newRouteState = new RouteState()
				{
					name = currentRouteState.name,
					targetWaypoint = target,
					nextWaypoints = currentRouteState.nextWaypoints.Skip(1),
					CurrentDistance = locationHandler.Location.GetDistanceXY(target.Location)
				};

				container.Bind<RouteState>().FromInstance(newRouteState).WhenInjectedInto<RoutingInstaller>();
				//container.BindInterfacesTo<RouteStatePublisher>().AsSingle();
			})
			;

		}
	}

	public void Dispose()
	{
		this.signal -= OnContentSolved;
	}
}
