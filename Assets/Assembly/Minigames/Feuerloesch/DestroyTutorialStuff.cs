﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyTutorialStuff : MonoBehaviour {

    private void OnMouseDown()
    {
        GameObject tutStuff = GameObject.Find("TutorialStuff");
        if (tutStuff) Destroy(tutStuff);
    }
}
