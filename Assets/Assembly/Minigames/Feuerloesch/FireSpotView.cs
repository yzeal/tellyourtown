using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Collider2D))]
public class FireSpotView : MonoBehaviour
{
	public float Health = 1f;
  public GameObject spawnOnExtinguished;

	public AAudioCue AmbientSound;
	public AAudioCue ExtinguishedSound;

	private FireExtinguished OnFireExtinguished;

	public AudioSource AmbientAS;
	public AudioSource ExtinguishAS;

	[Inject]
	public void Construct(
		FireExtinguished OnFireExtinguished
		)
	{
		this.OnFireExtinguished = OnFireExtinguished;
		if (spawnOnExtinguished)
		{
			spawnOnExtinguished.SetActive(false);
		}
	}

	public void Start()
	{
		var audio = GetComponent<AudioSource>();
		if(AmbientSound)
		{
			audio.loop = true;
			AmbientSound.Play(AmbientAS);
		}
	}
	private IObservable<Unit> ExtinguishedSoundDone;
	public void Damage(float value)
	{
		Health -= value;
		if(Health <= Mathf.Epsilon)
		{
			ExtinguishedSoundDone = ExtinguishedSound.Play(ExtinguishAS);
			ExtinguishedSoundDone.DoOnCompleted(() => ExtinguishedSoundDone = null);
			GetComponent<Animator>().SetTrigger("tExtinguished");
			if (spawnOnExtinguished)
			{
				spawnOnExtinguished.SetActive(true);
			}
}
	}

	private void SmokeAnimationDone()
	{
		if (ExtinguishedSoundDone != null)
		{
			ExtinguishedSoundDone.Subscribe(_ =>
			{
				OnFireExtinguished.Fire();
				Destroy(this.gameObject);
			}).AddTo(this.gameObject);
		}
		else
		{
			OnFireExtinguished.Fire();
			Destroy(this.gameObject);
		}
	}

	private void OnDestroy()
	{

	}

	public class Factory : Factory<FireSpotView> { }
}

public class FireExtinguished : Signal<FireExtinguished>
{
}