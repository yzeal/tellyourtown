using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


[RequireComponent(typeof(TargetJoint2D))]
public class WassereimerView : MonoBehaviour
{
	private enum State
	{
		Thrown,
		Released,
		Hold,
	}

	[ReadOnly]
	[SerializeField]
	private State CurrentInputState = State.Thrown;
	[ReadOnly]
	private Vector3 ReleasePoint;

	private Vector3 PositionLastFrame;
	[ReadOnly]
	[SerializeField]
	private float CurrentSpeed;
	public float MinimumRequiredSpeed = 1f;

	public float SplashRadius = 1f;

	public float ThrowingStrength;
	public float ThrowingDegree = 45f;
	public AnimationCurve FlightHeightScaling;

	public AAudioCue SpawnSound;
	public AAudioCue ThrowSound;
	public AAudioCue MissedSound;

	private float FlightTime;
	private float MaxHeightReachedAt;

	private float TotalFlightTime;
	private FRequestResourceSignal eimerUsed;
	private Transform nextBucketStartTransform;

	private AudioSource audioSource;

	[Inject]
	public void Construct(
		FRequestResourceSignal requestResource,
		[Inject(Source = InjectSources.Local)] Transform BucketInitialPosition
		)
	{
		this.nextBucketStartTransform = BucketInitialPosition;
		this.eimerUsed = requestResource;

	}

	public void Start()
	{
		audioSource = GetComponent<AudioSource>();

		SpawnSound.Play(audioSource);
	}

	public Vector2 WorldPositionFromTouch(Vector2 touch)
	{
		Vector3 mp = touch;
		mp.z = -Camera.main.transform.position.z;

		return Camera.main.ScreenToWorldPoint(mp);
	}

	public bool IsTouch()
	{
		if(Application.isEditor)
		{
			return Input.GetMouseButton(0);
		}
		else
		{
			return Input.touchCount > 0;
		}
	}

	public Vector2 GetTouch()
	{
		if (Application.isEditor)
		{
			return Input.mousePosition;
		}
		else
		{
			return Input.touches[0].position;
		}
	}

	bool once = true;
	bool onceThrowSound = true;
	public void Update()
	{
		if (CurrentInputState == State.Thrown && IsTouch())
		{
			Vector3 TouchWorldPosition = WorldPositionFromTouch(GetTouch());
			var TouchRay = Camera.main.ScreenPointToRay(GetTouch());
			var Hit = Physics2D.Raycast(TouchRay.origin, TouchRay.direction);
			if (Hit && Hit.transform.GetComponent<WassereimerView>())
			{
				this.GetComponent<TargetJoint2D>().anchor = WorldPositionFromTouch(GetTouch()) - new Vector2(transform.position.x, transform.position.y);
				//this.GetComponent<Rigidbody2D>().MovePosition(WorldPositionFromTouch(GetTouch()));
				CurrentInputState = State.Hold;
				PositionLastFrame = transform.position;
			}
		}

		if (CurrentInputState == State.Released)
		{
			if(onceThrowSound)
			{
				onceThrowSound = false;
				ThrowSound.Play(audioSource);
			}
			this.GetComponent<Rigidbody2D>().freezeRotation = true;
			FlightTime = FlightTime + Time.deltaTime;
			var s = FlightHeightScaling.Evaluate(FlightTime / TotalFlightTime);
			this.transform.localScale = new Vector3(s, s, s);

			if (FlightTime > TotalFlightTime && once)
			{
				once = false;
				var allHits = Physics2D.CircleCastAll(this.transform.position, SplashRadius, Vector2.zero);
				allHits = allHits.Where(x => x.transform.GetComponent<FireSpotView>() != null).ToArray();

				if(allHits.Length > 0)
				{
					foreach (var hit in allHits)
					{
						var fireSpot = hit.transform.GetComponent<FireSpotView>();
						if (fireSpot)
						{
							fireSpot.Damage(fireSpot.Health);
						}
					}

					eimerUsed.Fire();

					Destroy(this.gameObject);
				}
				else
				{
					GetComponent<SpriteRenderer>().enabled = false;
					MissedSound.Play(audioSource)
						.Subscribe(_ =>
						{
							eimerUsed.Fire();
							Destroy(this.gameObject);
						}).AddTo(this.gameObject);
				}
			}
		}
	}

	public void FixedUpdate()
	{
		if(CurrentInputState == State.Hold)
		{
			if (Input.GetMouseButton(0))
			{
				Vector3 mp = WorldPositionFromTouch(GetTouch());
				GetComponent<TargetJoint2D>().target = mp;

				CurrentSpeed = Vector2.Distance(transform.position, PositionLastFrame) / Time.fixedDeltaTime;
				PositionLastFrame = transform.position;
			}
			else // Touch released
			{
				this.nextBucketStartTransform.position = this.transform.position;
				if (CurrentSpeed < MinimumRequiredSpeed)
				{
					CurrentInputState = State.Thrown;
				}
				else
				{
					GetComponent<TargetJoint2D>().enabled = false;
					Vector3 delta = GetComponent<TargetJoint2D>().target - (GetComponent<TargetJoint2D>().anchor+ new Vector2(transform.position.x, transform.position.y));

					ReleasePoint = transform.position;

					Vector2 v0 = delta.normalized * (CurrentSpeed - delta.magnitude) * ThrowingStrength;

					GetComponent<Rigidbody2D>().velocity = v0;
					MaxHeightReachedAt = (v0.magnitude * Mathf.Sin(Mathf.Deg2Rad * ThrowingDegree) ) / (Physics2D.gravity.magnitude);

					TotalFlightTime = 2 * MaxHeightReachedAt;

					CurrentInputState = State.Released;
					GetComponent<Animator>().SetTrigger("tThrown");
				}
			}
		}

		
	}

	private void OnDrawGizmos()
	{
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere(this.transform.position, this.SplashRadius);
	}


	public class Factory : Factory<WassereimerView> {
	}
}