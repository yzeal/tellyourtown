using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class FeuerloeschViewModel : IDisposable, IInitializable
{
	CompositeDisposable _disposables = new CompositeDisposable();

	private PlayerProgress playerProgress;
	private WassereimerView.Factory wassereimerFactory;
	private ReactiveProperty<WassereimerView> currentWassereimer = new ReactiveProperty<WassereimerView>();
	private FRequestResourceSignal OnResourceRequested;
	private Transform WassereimerSpawn;

	[Inject]
	public FeuerloeschViewModel(
		PlayerProgress pprogress,
		WassereimerView.Factory wassereimerFactory,
		FRequestResourceSignal requestResource,
		FContentSolvedSignal contentSolved,
		FireExtinguished OnFireExtinguished,
		[Inject(Source = InjectSources.Local)] Transform BucketSpawnTransform
		)
	{
		this.WassereimerSpawn = BucketSpawnTransform;
		this.OnResourceRequested = requestResource;
		this.playerProgress = pprogress;
		this.wassereimerFactory = wassereimerFactory;

		this.OnResourceRequested
			.AsObservable
			.Subscribe(
			_ =>
			{
				if(IsContentSolved())
				{
					contentSolved.Fire();
				}
				else
				{
					RespawnWaterBucket();
				}
			}
			)
			.AddTo(this._disposables)
			;

		OnFireExtinguished
			.AsObservable
			.Subscribe(
			_ =>
			{
				FireCount--;

				if(FireCount <= 0)
				{
					contentSolved.Fire();
				}
			}
			)
			.AddTo(this._disposables)
			;
	}

	public int FireCount { get; internal set; }

	public void Dispose()
	{
		_disposables.Clear();
	}

	public void Initialize()
	{
		OnResourceRequested.Fire();
	}

	public bool IsContentSolved()
	{
		return 
			(this.playerProgress.ResourceAvailable == 0)
			;
	}

	public bool RespawnWaterBucket()
	{
		this.playerProgress.ResourceAvailable--;
		this.currentWassereimer.Value = wassereimerFactory.Create();

		this.currentWassereimer.Value.transform.position = this.WassereimerSpawn.position;

		// TODO: Place in throwing start area

		return true;
	}

}