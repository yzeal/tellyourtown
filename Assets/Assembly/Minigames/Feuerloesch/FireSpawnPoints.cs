using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class FireSpawnPoints : MonoBehaviour
{
	[ReadOnly]
	[SerializeField]
	List<Transform> Children;
	[Range(0F, 1F)]
	public float PercentageOfFires;

	[Inject]
	public void Construct(
		FireSpotView.Factory fireFactory,
		FeuerloeschViewModel feuerloesch
		)
	{
		this.Children = new List<Transform>(transform.childCount);
		foreach(Transform child in this.transform)
		{
			Children.Add(child);
		}

		// Randomize Children / Shuffle
		this.Children = this.Children.OrderBy(_ => Guid.NewGuid()).ToList();
		int MaxCount = (int)(PercentageOfFires * this.Children.Count);

		feuerloesch.FireCount = MaxCount;

		for(int i=0;i<MaxCount;++i)
		{
			var fireSpot = fireFactory.Create();
			fireSpot.transform.localPosition = this.Children[i].position;
			fireSpot.GetComponent<SpriteRenderer>().sortingOrder = i;
		}
	}
}