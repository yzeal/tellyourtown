using UnityEngine;
using Zenject;

public class FeuerloeschInstaller : MonoInstaller<FeuerloeschInstaller>
{
	public Settings _Settings;

	public override void InstallBindings()
	{
		Container.DeclareSignal<FireExtinguished>();

		Container.BindFactory<FireSpotView, FireSpotView.Factory>().FromComponentInNewPrefab(_Settings.FireSpotPrefab);
		Container.BindFactory<WassereimerView, WassereimerView.Factory>().FromComponentInNewPrefab(_Settings.WassereimerPrefab);

		Container.BindInterfacesAndSelfTo<FeuerloeschViewModel>().AsSingle();
	}

	[System.Serializable]
	public class Settings
	{
		public GameObject FireSpotPrefab;
		public GameObject WassereimerPrefab;
	}
}