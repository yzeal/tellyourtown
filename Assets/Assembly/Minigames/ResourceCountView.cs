using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ResourceCountView : MonoBehaviour
{
	public int ResourceCountAdjustment = 1;

	[Inject]
	public void Construct(
		PlayerProgress pprogress
		)
	{
		pprogress
			.p_ResourceAvailable
			.Select(x => x+ResourceCountAdjustment)
			.SubscribeToText(GetComponent<Text>())
			.AddTo(gameObject)
			;
	}
}