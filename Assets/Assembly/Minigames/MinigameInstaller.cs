using UnityEngine;
using Zenject;

public class MinigameInstaller : MonoInstaller<MinigameInstaller>
{
	public override void InstallBindings()
	{
		RoutingInstaller.Install(Container);

		Container.DeclareSignal<FRequestResourceSignal>();
		Container.DeclareSignal<FContentSolvedSignal>();

		//Container.Bind<ContentSolvedHandler_RouteToNextWaypoint>().AsSingle().NonLazy();
		Container.BindInterfacesTo<ContentSolvedHandler_RouteToNextWaypoint>().AsSingle().NonLazy();
	}
}