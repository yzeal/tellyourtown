using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class RouteInfoScreenInstaller : MonoInstaller<RouteInfoScreenInstaller>
{
	[SerializeField]
	[InjectOptional]
	RouteContext.State routeContext;


	public override void InstallBindings()
	{
		LocaleInstaller.Install(Container);
		PopupInstaller.Install(Container);

		Container.Bind<RouteContext.State>().FromInstance(routeContext).AsSingle();
	}
}
