using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UniRx;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Button))]
public class RouteInfoStartRouteButtonView : MonoBehaviour
{
	[Inject]
	void Construct(
		ZenjectSceneLoader sceneLoader,
		RouteContext.State routeContext,
		FRouteWaypoints waypoints,
		PlayerProgress pprogress,
		ALocationHandler locations,
		FRouteDocument routeDocument
		)
	{
		GetComponent<Button>()
			.BindToOnClick(
				pprogress.p_Points.Select(x => x < waypoints.Count).ToReactiveProperty(),
				_ => Observable.Create<Unit>((observer) =>
				{
					var loadingScene = sceneLoader.LoadSceneAsync("RoutingTemplate", LoadSceneMode.Single,
						(container) =>
						{
							var waypointList = waypoints.Waypoints;

							if (pprogress.Waypoints == null)
							{
								if (routeDocument.m_WaypointSelector != null)
								{
									waypointList = routeDocument.m_WaypointSelector.MutateWaypoints(waypoints);
									pprogress.Waypoints = waypointList;
								}
							}
							else
							{
								waypointList = pprogress.Waypoints;
							}


							var targetWaypoint = waypointList.Skip(pprogress.Points).First();
							var newRouteState = new RouteState()
							{
								name = routeContext.name,
								targetWaypoint = targetWaypoint,
								nextWaypoints = waypointList.Skip(pprogress.Points + 1),
								CurrentDistance = locations.Location.GetDistanceXY(targetWaypoint.Location)
							};
							container.Bind<RouteState>().FromInstance(newRouteState).WhenInjectedInto<RoutingInstaller>();
							container.BindInterfacesTo<RouteStatePublisher>().AsSingle();
						})
						;
					loadingScene.allowSceneActivation = false;

					Progress<float> p = new Progress<float>((f) =>
					{
						Debug.LogFormat("loading {0} %", f * 100);

						if (f >= 0.9f)
						{
							// TODO: May stagger loading here..
							loadingScene.allowSceneActivation = true;
						}
					})
					;

					var sub = loadingScene.AsAsyncOperationObservable(p)
						.Subscribe(asyncOp =>
						{
							observer.OnCompleted();
						})
						;

					return Disposable.Create(() => { sub.Dispose(); });
				})
				.AsUnitObservable()
			)
			.AddTo(gameObject)
			;
	}
}
