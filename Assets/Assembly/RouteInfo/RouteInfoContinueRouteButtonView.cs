using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UniRx;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Button))]
public class RouteInfoContinueRouteButtonView : MonoBehaviour
{
	[Inject]
	void Construct(
		ZenjectSceneLoader sceneLoader,
		RouteContext.State routeContext,
		FRouteWaypoints waypoints,
		PlayerProgress pprogress
		)
	{
		gameObject.SetActive(pprogress.Points > 0);

		GetComponent<Button>()
			.BindToOnClick(
				pprogress.p_Points.Select(x => x > 0 && x < waypoints.Count).ToReactiveProperty(),
				_ => Observable.Create<Unit>((observer) =>
				{
					var loadingScene = sceneLoader.LoadSceneAsync("RoutingTemplate", LoadSceneMode.Single,
						(container) =>
						{
							var newRouteState = new RouteState()
							{
								name = routeContext.name,
								targetWaypoint = waypoints.Waypoints.Skip(pprogress.Points).First(),
								nextWaypoints = waypoints.Waypoints.Skip(pprogress.Points + 1)
							};
							container.Bind<RouteState>().FromInstance(newRouteState).WhenInjectedInto<LocationInstaller>();
						})
						;
					Progress<float> p = new Progress<float>((f) =>
						{
							Debug.LogFormat("loading {0} %", f * 100);
						})
						;

					var sub = loadingScene.AsAsyncOperationObservable(p)
						.Subscribe(asyncOp =>
						{
							observer.OnCompleted();
						})
						;

					return Disposable.Create(() => { sub.Dispose(); });
				})
				.AsUnitObservable()
			)
			.AddTo(gameObject)
			;
	}
}
