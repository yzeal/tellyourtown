using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UniRx;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Button))]
public class RouteInfoResetRouteButtonView : MonoBehaviour
{
	[Inject]
	void Construct(
		ZenjectSceneLoader sceneLoader,
		RouteContext.State routeContext,
		FRouteWaypoints waypoints,
		PlayerProgress pprogress,
		ALocationHandler locationHandler,
		FRouteDocument routeDocument,
		FDatabase database
		)
	{
		GetComponent<Button>()
			.BindToOnClick(
				pprogress.p_Points.Select(x => /*x > 0 &&*/ x <= waypoints.Count).ToReactiveProperty(),
				_ => Observable.Create<Unit>((observer) =>
				{
					var loadingScene = sceneLoader.LoadSceneAsync("RoutingTemplate", LoadSceneMode.Single, (container) =>
					{
						var waypointList = waypoints.Waypoints;

						if (routeDocument.m_WaypointSelector != null)
						{
							waypointList = routeDocument.m_WaypointSelector.MutateWaypoints(waypoints);
						}

						pprogress = routeDocument.CopyPlayerProgressDefault();
						pprogress.Waypoints = waypointList;

						var targetWaypoint = waypointList.First();

						database.OverwritePlayerProgress(routeContext.name, pprogress);
						database.storePlayerProgress(routeContext.name);

						var newRouteState = new RouteState()
						{
							name = routeContext.name,
							targetWaypoint = targetWaypoint,
							nextWaypoints = waypointList.Skip(1),
							CurrentDistance = locationHandler.Location.GetDistanceXY(targetWaypoint.Location)
						};
						container.Bind<RouteState>().FromInstance(newRouteState).WhenInjectedInto<RoutingInstaller>();
						container.BindInterfacesTo<RouteStatePublisher>().AsSingle();
					});

					Progress<float> p = new Progress<float>((f) =>
					{
						Debug.LogFormat("loading {0} %", f * 100);
					});
					var sub = loadingScene.AsAsyncOperationObservable(p)
						.Subscribe(asyncOp =>
						{
							observer.OnCompleted();
						});

					return Disposable.Create(() => { sub.Dispose(); });
				})
				.AsUnitObservable()
			)
			.AddTo(gameObject)
			;
	}
}
