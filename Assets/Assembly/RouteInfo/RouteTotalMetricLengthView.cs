using UnityEngine;
using System.Collections;
using Zenject;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class RouteTotalMetricLengthView : MonoBehaviour {

	[Inject]
	void Construct(
		FRouteWaypoints waypoints
		)
	{
		double length = 0.0;
		var current = waypoints.Waypoints.FirstOrDefault();
		foreach(var next in waypoints.Waypoints.Skip(1))
		{
			length += current.Location.GetDistanceXY(next.Location);
			current = next;
		}

		GetComponent<Text>().text = TextUtil.MetricTextOfDistance(length);
	}
}
