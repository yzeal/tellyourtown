using UnityEngine;
using Zenject;

public class OptionsMenuInstaller : MonoInstaller<OptionsMenuInstaller>
{
	[InjectOptional]
	public RouteState CurrentState;

	public override void InstallBindings()
	{
		PopupInstaller.Install(Container);

		if(CurrentState == null)
		{
			CurrentState = new RouteState() { name = "irminenfreihof" };
		}
		Container.Bind<RouteState>().FromInstance(CurrentState).AsSingle();

		RoutingInstaller.Install(Container);

		Container.Bind<FRouteWaypoints>().ToSelf().FromResolveGetter<FDatabase>(x => x.getWaypoints(CurrentState.name)).AsSingle();

		//Container.Bind<PlayerProgress>().ToSelf().FromResolveGetter<FDatabase>(x => x.getPlayerProgress(CurrentState.name)).AsSingle();


	}
}
