using UnityEngine;
using System.Collections;
using Zenject;
using UnityEngine.UI;
using UniRx;
using System.Threading;
using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.Linq;

public class OptionsPopupButtonView : MonoBehaviour {
	private ZenjectSceneLoader sceneLoader;

	[Inject]
	void Construct(
		ZenjectSceneLoader zsl,
		RouteState routeState
		)
	{
		sceneLoader = zsl;
		var btnComp = GetComponent<Button>();
		print(routeState.name);
		btnComp.BindToOnClick(
			_ => Observable.Create<Unit>((observer) => { // Create Async Context until observer.OnCompleted called

				

				var loadingScene = sceneLoader.LoadSceneAsync("OptionMenu", LoadSceneMode.Additive, (container) =>
				{
					//container.Bind<RouteState>().FromInstance(routeState).AsSingle().WhenInjectedInto<RoutingInstaller>();
					container.Bind<RouteState>().FromInstance(routeState).AsSingle().WhenInjectedInto<OptionsMenuInstaller>();
					container.Bind<PopupState>().FromInstance(new PopupState()
					{
						PopupObserver = observer
					}).AsSingle().WhenInjectedInto<PopupInstaller>();
				}, LoadSceneRelationship.Child);

				Progress<float> p = new Progress<float>((f) =>
				{
					Debug.LogFormat("loading {0} %", f*100.0f);
				});
				var sub = loadingScene.AsAsyncOperationObservable(p)
					.Subscribe(asyncOp =>
					{
						// observer.OnCompleted();
					});

				return Disposable.Create(() => { sub.Dispose(); });
			})
			.AsUnitObservable()
		)
		.AddTo(gameObject)
		;
	}

}
