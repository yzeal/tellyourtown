using UnityEngine;
using Zenject;

public class RouteContext : MonoBehaviour
{
	[Inject]
	void Construct(
		)
	{
	}

	public class Factory : Factory<string, RouteContext> { }

	[System.Serializable]
	public class State
	{
		public string name;
	}
}
