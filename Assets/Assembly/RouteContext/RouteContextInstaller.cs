using UnityEngine;
using Zenject;
using System;
using System.Collections.Generic;

public class RouteContextInstaller : MonoInstaller<RouteContextInstaller>
{
	[SerializeField] /* Show in Inspector */
	[Expand]
	RouteContext.State RouteContext = null;

	[InjectOptional] /* can be set in inspector or via Factory.Create(..) */
	RouteContext.State RouteContextOverride = null;

	public override void InstallBindings()
	{
		var usedSettings = RouteContextOverride != null ? RouteContextOverride : RouteContext;

		Container.Bind<LocalizationBook>().WithId(EContext.Route).To<LocalizationBook>().FromResolveGetter<FDatabase>(x => x.getLocalization(usedSettings.name)).AsSingle();
		Container.Bind<PlayerProgress>().ToSelf().FromResolveGetter<FDatabase>(x => x.getPlayerProgress(usedSettings.name)).AsSingle();
		Container.Bind<FRouteDocument>().ToSelf().FromResolveGetter<FDatabase>(x => x.getRoute(usedSettings.name)).AsSingle();
		Container.Bind<FRouteWaypoints>().ToSelf().FromResolveGetter<FDatabase>(x => x.getWaypoints(usedSettings.name)).AsSingle();
		Container.Bind<RouteContext.State>().FromInstance(usedSettings).AsSingle();

		LocaleInstaller.Install(Container);
	}
}
