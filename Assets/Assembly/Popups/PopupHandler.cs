using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.SceneManagement;
using Zenject;

public class PopupHandler
{
	private readonly PopupState state;

	[Inject]
	public PopupHandler(
		PopupState popup
		)
	{
		this.state = popup;

	}

	public void ClosePopup(Ref<Scene> popup)
	{
		state.PopupObserver.OnCompleted();
		SceneManager.UnloadSceneAsync(popup);
	}

}
