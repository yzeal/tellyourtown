using UnityEngine;
using Zenject;

public class CreditsPopupInstaller : MonoInstaller<CreditsPopupInstaller>
{
	public override void InstallBindings()
	{
		LocaleInstaller.Install(Container);
		PopupInstaller.Install(Container);
	}
}
