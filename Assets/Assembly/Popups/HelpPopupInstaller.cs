using UnityEngine;
using Zenject;

public class HelpPopupInstaller : MonoInstaller<HelpPopupInstaller>
{
	public override void InstallBindings()
	{
		LocaleInstaller.Install(Container);
		PopupInstaller.Install(Container);
	}
}
