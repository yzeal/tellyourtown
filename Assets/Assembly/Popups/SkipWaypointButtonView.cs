using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;


[RequireComponent(typeof(Button))]
public class SkipWaypointButtonView : MonoBehaviour
{

	[Inject]
	void Construct(
		[InjectOptional] WaypointReachedSignal signal
	)
	{
		ReactiveProperty<bool> enabled = new ReactiveProperty<bool>(signal != null);
		GetComponent<Button>().BindToOnClick(enabled, _ => Observable.Create<Unit>((observer) =>
			{
				signal.Fire(); // Changes scene to next available
				return Disposable.Empty;
			})
		)
		.AddTo(gameObject);



	}
}
