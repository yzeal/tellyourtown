using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class RoutingProximityMaskView : MonoBehaviour
{
	[Tooltip("L, T, R, B")]
	public Vector4 FullAnchorTarget = new Vector4(-.1f, 1.3f, 1.2f, 0);
	private Vector4 AnchorOrigin = new Vector4();

	public bool UseCaps = false;
	public Vector4 AnchorTargetMaxCap = new Vector4(1, 1, 1, 1);
	public Vector4 AnchorTargetMinCap = new Vector4(0, 0, 0, 0);

	public AnimationCurve InterpolationToTargetBoundCurve;

	public double LerpDistanceFactor = 1.1;

	[ReadOnly]
	[SerializeField]
	private double LerpDistance;

	private RouteState routeState;

	public float BoundAdjustmentRate = 1f;

	[ReadOnly][SerializeField]
	private Vector4 TargetAnchor;

	new RectTransform transform;
	private Vector4 CurrentAnchor;

	[Inject]
	public void Construct(
		RouteState routeState,
		ALocationHandler locationHandler
		)
	{
		this.routeState = routeState;
		routeState
			.p_CurrentDistance
			.Subscribe(dist =>
			{
				LerpDistance = dist * LerpDistanceFactor;

				CalculateMask(locationHandler.Location);
			})
			.AddTo(gameObject)
			;

		locationHandler.p_Location
			.Subscribe(loc =>
			{
				CalculateMask(loc);
			})
			.AddTo(gameObject)
			;

		transform = GetComponent<RectTransform>();

		AnchorOrigin.x = transform.anchorMin.x; AnchorOrigin.w = transform.anchorMin.y;
		AnchorOrigin.z = transform.anchorMax.x; AnchorOrigin.y = transform.anchorMax.y;



		CurrentAnchor = TargetAnchor = AnchorOrigin;
	}

	void Update()
	{
		CurrentAnchor = (CurrentAnchor + (TargetAnchor - CurrentAnchor) * Time.deltaTime * BoundAdjustmentRate);
		if (UseCaps)
		{
			transform.anchorMin = new Vector2(Mathf.Clamp(CurrentAnchor.x, AnchorTargetMinCap.x, AnchorTargetMaxCap.x), Mathf.Clamp(CurrentAnchor.w, AnchorTargetMinCap.w, AnchorTargetMaxCap.w));
			transform.anchorMax = new Vector2(Mathf.Clamp(CurrentAnchor.z, AnchorTargetMinCap.z, AnchorTargetMaxCap.z), Mathf.Clamp(CurrentAnchor.y, AnchorTargetMinCap.y, AnchorTargetMaxCap.y));
		}
		else
		{
			transform.anchorMin = new Vector2(CurrentAnchor.x, CurrentAnchor.w);
			transform.anchorMax = new Vector2(CurrentAnchor.z, CurrentAnchor.y);
		}
	}

	private void CalculateMask(FLocation loc)
	{
		var currentDistance = loc.GetDistanceXY(routeState.targetWaypoint.Location);

		float CurveAlpha = ((float)(currentDistance / LerpDistance));
		if(!float.IsNaN(CurveAlpha))
		{
			var Alpha = InterpolationToTargetBoundCurve.Evaluate(Mathf.Clamp(CurveAlpha, 0f, 1f));

			TargetAnchor = FullAnchorTarget * Alpha + AnchorOrigin * ( 1f - Alpha );
		}
	}
}
 