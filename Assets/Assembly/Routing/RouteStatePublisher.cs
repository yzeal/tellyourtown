using System;
using UnityEngine;
using Zenject;

public class RouteStatePublisher : IInitializable
{
	private RouteState currentState;
	private RouteStateChangedSignal signal;

	[Inject]
	public RouteStatePublisher(
		RouteState currentState,
		RouteStateChangedSignal signal
		)
	{
		this.currentState = currentState;
		this.signal = signal;
	}

	public void Initialize()
	{
		this.signal.Fire(this.currentState);
	}
}