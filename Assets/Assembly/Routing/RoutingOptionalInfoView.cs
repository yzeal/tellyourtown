using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class RoutingOptionalInfoView : MonoBehaviour
{
	public FNameDynamicTextView DynamicText;

	[Inject]
	public void Construct(
		RouteState routeState
		)
	{
		DynamicText.Localization = routeState.targetWaypoint.RoutingInfo;

		if(routeState.targetWaypoint.RoutingInfo == null)
		{
			this.gameObject.SetActive(false);
		}
	}
}