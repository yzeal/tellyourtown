using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UniRx;

[System.Serializable]
public class RouteState
{
	public string name; // RouteId
	public FWaypointInfo targetWaypoint; // Current Target
	public IEnumerable<FWaypointInfo> nextWaypoints = new List<FWaypointInfo>(); // Remaining Waypoints

	public ReactiveProperty<double> p_CurrentDistance = new ReactiveProperty<double>();
	public double CurrentDistance { get { return p_CurrentDistance.Value; } set { p_CurrentDistance.Value = value; } }
}
