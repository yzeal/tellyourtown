using UnityEngine;
using Zenject;

public class RoutingInstaller : Installer<RoutingInstaller>
{
	[InjectOptional]
	public RouteState CurrentState;

	public override void InstallBindings()
	{
		// Install Locales
		LocaleInstaller.Install(Container);

		if(CurrentState != null)
		{
			// Bind Current RouteState if any?
			Container.Bind<RouteState>().FromInstance(CurrentState).AsSingle();

			// Bind the RouteDocument
			Container.Bind<FRouteDocument>().ToSelf().FromResolveGetter<FDatabase>(x => x.getRoute(CurrentState.name));

			// Bind Databases Playerprogress to CurrentRoute
			Container.Bind<PlayerProgress>().ToSelf().FromResolveGetter<FDatabase>(x => x.getPlayerProgress(CurrentState.name)).AsSingle();

			// Bind Localization Book to resolved Localizations from RouteDocument
			Container.Bind<LocalizationBook>().WithId(EContext.Route).To<LocalizationBook>().FromResolveGetter<FDatabase>(x => x.getLocalization(CurrentState.name)).AsSingle();
		}
		else
		{
			// Bind Current RouteState if any?
			Container.Bind<RouteState>().FromNew().AsSingle();

			// Bind the RouteDocument
			Container.Bind<FRouteDocument>().ToSelf().FromResolveGetter<FDatabase>(x => x.getRoute("irminenfreihof"));

			// Bind Databases Playerprogress to CurrentRoute
			Container.Bind<PlayerProgress>().ToSelf().FromResolveGetter<FDatabase>(x => x.getPlayerProgress("irminenfreihof")).AsSingle();

			// Bind Localization Book to resolved Localizations from RouteDocument
			Container.Bind<LocalizationBook>().WithId(EContext.Route).To<LocalizationBook>().FromResolveGetter<FDatabase>(x => x.getLocalization("irminenfreihof")).AsSingle();
		}
	}
}