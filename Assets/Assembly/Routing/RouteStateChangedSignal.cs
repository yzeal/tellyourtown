using UnityEngine;
using Zenject;

public class RouteStateChangedSignal : Signal<RouteState, RouteStateChangedSignal>
{
}