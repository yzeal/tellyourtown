using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class RoutingWaypointBackgroundView : MonoBehaviour
{
	[Inject]
	public void Construct(
		RouteState routeState
		)
	{
		GetComponent<Image>().sprite = routeState.targetWaypoint.RoutingBackground;
	}
}