using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Button))]
public class ToggleInventoryButtonView : MonoBehaviour
{
	[Inject]
	public void Construct(
		)
	{
		var btn = GetComponent<Button>();

		btn.OnClickAsObservable().Subscribe(_ => MessageBroker.Default.Publish(new ToggleInventoryMessage() { }));
		//btn.BindToOnClick(_ => Observable.Create<Unit>(
		//	(observer) =>
		//	{
		//		return Disposable.Create(() => { });
		//	})
		//	.AsUnitObservable()
		//)
		//.AddTo(gameObject)
		//;
	}
}