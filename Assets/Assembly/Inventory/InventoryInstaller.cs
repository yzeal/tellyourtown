using UnityEngine;
using Zenject;

public class InventoryInstaller : MonoInstaller<InventoryInstaller>
{
	public Settings m_Settings;

	public override void InstallBindings()
	{

		Container.Bind<ResourceInventoryViewModel>().ToSelf().AsSingle().NonLazy();

		Container.BindFactory<InventoryItem, InventoryItemView, InventoryItemView.Factory>().FromComponentInNewPrefab(m_Settings.InventoryItemPrefab).UnderTransform(m_Settings.InventoryItemsParentTransform);
	}

	[System.Serializable]
	public class Settings
	{
		[Tooltip("Prefab instantiated for every item in the inventory.")]
		public GameObject InventoryItemPrefab;
		public Transform InventoryItemsParentTransform;
	}
}