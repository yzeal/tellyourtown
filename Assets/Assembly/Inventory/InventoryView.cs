using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class InventoryView : MonoBehaviour
{

	private void Start()
	{
		// Hidden by default
		gameObject.SetActive(false);
	}

	[Inject]
	public void Construct(
		PlayerProgress playerProgress,
		InventoryItemView.Factory InventoryItemFactory
		)
	{
		foreach (var item in playerProgress.Items)
		{
			InventoryItemFactory.Create(item);
		}

		MessageBroker.Default.Receive<ToggleInventoryMessage>()
			.Subscribe(_ =>
			{
				this.gameObject.SetActive(!this.gameObject.activeSelf);
			})
			.AddTo(gameObject)
			;
	}
}