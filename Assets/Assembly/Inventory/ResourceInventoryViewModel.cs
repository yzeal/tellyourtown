using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ResourceInventoryViewModel : IDisposable
{
	CompositeDisposable _disposables = new CompositeDisposable();

	// ID of Empty Resource Item
	private string RESOURCE_EMPTY = "RESOURCE_EMPTY";
	// ID of Resource Available
	private string RESOURCE = "RESOURCE";

	[Inject]
	public ResourceInventoryViewModel(
		PlayerProgress playerProgress
		)
	{
		// Observe Resource difference to Capacity (if requested by Inventory)
		Observable.CombineLatest(
			playerProgress
				.Items
				.Where(x => x.ID == RESOURCE_EMPTY)
				.ToObservable(),
				playerProgress.p_ResourceAvailable, 
				playerProgress.p_ResourceCapacity,
				(emptyResourceItem, available, capacity) =>
				{
					return emptyResourceItem;
				}
			)
			.Subscribe(emptyResource =>
			{
				emptyResource.Count = Mathf.Max(playerProgress.ResourceCapacity - playerProgress.ResourceAvailable, 0);
			})
			.AddTo(_disposables)
			;

		// Observe Available Resource (if requested by inventory)
		Observable.CombineLatest(
			playerProgress
				.Items
				.Where(x => x.ID == RESOURCE)
				.ToObservable(),
				playerProgress.p_ResourceAvailable,
				playerProgress.p_ResourceCapacity,
				(emptyResourceItem, available, capacity) =>
				{
					return emptyResourceItem;
				}
			)
			.Subscribe(resource =>
			{
				resource.Count = Mathf.Min(playerProgress.ResourceAvailable, playerProgress.ResourceCapacity);
			}
			)
			.AddTo(_disposables)
			;
	}

	public void Dispose()
	{
		_disposables.Dispose();
	}
}