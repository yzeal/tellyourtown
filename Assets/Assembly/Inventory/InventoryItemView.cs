using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class InventoryItemView : MonoBehaviour
{
	[ReadOnly][Expand]
	public InventoryItem AssignedItem;

	public Image ItemImage;
	public Text ItemCountText;

	[Inject]
	public void Construct(
		InventoryItem _ViewModel
		)
	{
		// Count changes propagated to Text
		_ViewModel
			.p_Count
			.SubscribeToText(ItemCountText)
			.AddTo(gameObject)
			;

		ItemImage.sprite = _ViewModel.Icon;
	}

	public class Factory : Factory<InventoryItem, InventoryItemView> { }
}