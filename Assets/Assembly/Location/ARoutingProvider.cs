﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using Zenject;

public abstract class ARoutingProvider : IInitializable, IDisposable
{
	public IObservable<FLocation> s_Location;

	public abstract void Dispose();
	public abstract void Initialize();
}
