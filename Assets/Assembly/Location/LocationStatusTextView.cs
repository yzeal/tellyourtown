using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class LocationStatusTextView : MonoBehaviour
{
	public string TR_Off = "TR_GPS_STATE_OFF";
	public string TR_On = "TR_GPS_STATE_ON";

	public StringReactiveProperty p_CurrentTag = new StringReactiveProperty();
	public string CurrentTag { get { return p_CurrentTag.Value; } set { p_CurrentTag.Value = value; } }

	protected Text TextComp;

	[Inject]
	public void Construct(
		ILocationStatusProvider statusProvider,
		Localizer l,
		LanguageHandler lh
		)
	{
		TextComp = GetComponent<Text>();

		Observable.CombineLatest(lh.p_CurrentLang, statusProvider.p_Status, (lang, status) => status)
			.Select(status => SetCurrentTag(status))
			.Select(tag => l.tr(tag))
			.SubscribeToText(TextComp)
			.AddTo(TextComp)
			.AddTo(this.gameObject)
			.AddTo(this)
			;
		this.CurrentTag = TR_Off;
	}

	protected string SetCurrentTag(LocationServiceStatus status)
	{
		switch (status)
		{
			case LocationServiceStatus.Stopped:
			case LocationServiceStatus.Failed:
				this.CurrentTag = TR_Off;
				break;
			case LocationServiceStatus.Initializing:
			case LocationServiceStatus.Running:
				this.CurrentTag = TR_On;
				break;
		}
		return this.CurrentTag;
	}
}