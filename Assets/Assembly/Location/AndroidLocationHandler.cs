using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Zenject;

public class AndroidLocationHandler : ALocationHandler
{
	[Inject]
	public AndroidLocationHandler(
		)
	{
	}

	public override void Initialize()
	{
		Input.compass.enabled = true;
	}

	public override void FixedTick()
	{
		Location = CurrentLocation();
		TrueHeading = Input.compass.trueHeading;
	}

	private FLocation CurrentLocation()
	{
		using (AndroidJavaClass gpsActivityClass = new AndroidJavaClass("com.thoughtbread.schulprojekt.FolkloreLocation"))
		{
			using (AndroidJavaObject LocationObject = gpsActivityClass.GetStatic<AndroidJavaObject>("currentLocation"))
			{
				LocationObject.Call("removeBearing");
				return (new FLocation()
				{
					latitude = LocationObject.Call<double>("getLatitude"),
					longitude = LocationObject.Call<double>("getLongitude"),
					accuracy = LocationObject.Call<float>("getAccuracy"),
					/*              Location::getExtras():Bundle                     -> Bundle::getInt(                     name       , default) */
					numSatellites = LocationObject.Call<AndroidJavaObject>("getExtras").Call<int>("getInt", new object[] { "satellites", -1 })
				});
			}
		}
	}
}
