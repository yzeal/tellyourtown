using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Zenject;
using UniRx;

public class MockLocationHandler : ALocationHandler, IDisposable
{
	CompositeDisposable disposeables = new CompositeDisposable();

	[Inject]
	public MockLocationHandler(
		RouteStateChangedSignal routeStateChanged
	)
	{
		routeStateChanged
			.AsObservable
			.Subscribe(
			(newState) =>
			{
				var l = newState.targetWaypoint.Location;
				l.latitude -= 0.0005;
				this.Location = l;

				newState.CurrentDistance = this.Location.GetDistanceXY(newState.targetWaypoint.Location);
			})
			.AddTo(this.disposeables);

		Observable.EveryUpdate()
			.ObserveOnMainThread()
			.Subscribe(_ =>
			{
				if (Input.GetKeyUp(KeyCode.F1))
				{
					Status = (LocationServiceStatus)(((int)Status + 1) % System.Enum.GetValues(typeof(LocationServiceStatus)).Length);
				}
			})
			.AddTo(this.disposeables);
	}

	public void Dispose()
	{
		this.disposeables.Dispose();
	}

	public override void FixedTick()
	{
		var CurrentLoc = Location;

		CurrentLoc.latitude += Input.GetAxis("Vertical") * Time.deltaTime * 0.001f;
		CurrentLoc.longitude -= Input.GetAxis("Horizontal") * Time.deltaTime * 0.001f;

		Location = CurrentLoc;

		TrueHeading += Input.GetAxis("RotateQE") * Time.deltaTime * 15f;
	}
}
