﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public interface ILocationStatusProvider
{
	LocationServiceStatus Status { get; }
	ReactiveProperty<LocationServiceStatus> p_Status { get; }
}
