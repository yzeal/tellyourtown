using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class TrueHeadingView : MonoBehaviour
{
	public bool X_Axis = false;
	public bool Y_Axis = false;
	public bool Z_Axis = false;
	private ALocationHandler location;

	[Inject]
	public void Construct(
		ALocationHandler locationVM
		)
	{
		this.location = locationVM;

		this.location.p_TrueHeading
			.Subscribe(
			heading =>
			{
				//GetComponent<Image>().transform.localRotation = Quaternion.Euler(0f, 0f, heading);
				//var t = Input.compass.rawVector;
				//var t = new Vector3(-7.5f, -1.5f, 4.0f);
				//var angle = Vector3.Angle(Vector3.right, new Vector3(t.x, 0f, t.z).normalized);
				//GetComponent<Image>().transform.localRotation = Quaternion.AngleAxis(angle, Vector3.forward);
				GetComponent<Image>().transform.localRotation = Quaternion.Euler(0f, 0f, heading);

				//Vector3 d = Input.compass.rawVector;
				//if(X_Axis)
				//{
				//	GetComponent<Image>().transform.localRotation = Quaternion.Euler(0f, 0f, d.x * Mathf.Rad2Deg);
				//}
				//else if(Y_Axis)
				//{
				//	GetComponent<Image>().transform.localRotation = Quaternion.Euler(0f, 0f, d.y * Mathf.Rad2Deg);
				//}
				//else if(Z_Axis)
				//{
				//	GetComponent<Image>().transform.localRotation = Quaternion.Euler(0f, 0f, d.z * Mathf.Rad2Deg);
				//}



			})
			.AddTo(gameObject);
	}
}