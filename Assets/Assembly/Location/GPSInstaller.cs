using System;
using UnityEngine;
using Zenject;

public class GPSInstaller : Installer<GPSInstaller>
{
	bool bUseUnityForLocation = true;

	public override void InstallBindings()
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			if (bUseUnityForLocation)
			{
				Container.Bind<ALocationHandler>().To<UnityLocationHandler>().AsSingle().NonLazy();
				Container.Bind(typeof(IFixedTickable), typeof(IInitializable)).To<UnityLocationHandler>().AsSingle();
			}
			else
			{
				Container.Bind<ALocationHandler>().To<iPhoneLocationHandler>().AsSingle().NonLazy();
				Container.Bind(typeof(IFixedTickable)).To<iPhoneLocationHandler>().AsSingle();
			}
			Container.Bind(typeof(ILocationStatusProvider)).To<UnityLocationHandler>().AsSingle();
		}
		else if (Application.platform == RuntimePlatform.Android)
		{

			if (bUseUnityForLocation)
			{
				Container.Bind<ALocationHandler>().To<UnityLocationHandler>().AsSingle().NonLazy();
				Container.Bind(typeof(IFixedTickable), typeof(IInitializable)).To<UnityLocationHandler>().AsSingle();
			}
			else
			{
				AndroidJNI.AttachCurrentThread();
				Container.Bind<ALocationHandler>().To<AndroidLocationHandler>().AsSingle().NonLazy();
				Container.Bind(typeof(IFixedTickable)).To<AndroidLocationHandler>().AsSingle();
			}
			Container.Bind(typeof(ILocationStatusProvider)).To<UnityLocationHandler>().AsSingle();
		}
		else
		{
			Container.Bind<ALocationHandler>().To<MockLocationHandler>().AsSingle().NonLazy();
			Container.Bind(typeof(IFixedTickable), typeof(IInitializable)).To<MockLocationHandler>().AsSingle();
			Container.Bind(typeof(ILocationStatusProvider)).To<MockLocationHandler>().AsSingle();
		}

		Container.Bind<ARoutingProvider>().To<MeanRoutingProvider>().AsSingle();
		Container.BindInterfacesTo<MeanRoutingProvider>().AsSingle();
	}
}