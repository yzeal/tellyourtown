using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UniRx;
using UnityEngine;
using Zenject;

public abstract class ALocationHandler : IFixedTickable, IInitializable, ILocationStatusProvider
{
	public ReactiveProperty<FLocation> p_Location = new ReactiveProperty<FLocation>();
	public FLocation Location { get { return p_Location.Value; } protected set { p_Location.Value = value; } }

	public ReactiveProperty<LocationServiceStatus> p_Status { get; protected set; }
	public LocationServiceStatus Status { get { return p_Status.Value; } protected set { p_Status.Value = value; } }

	public ReactiveProperty<float> p_TrueHeading = new ReactiveProperty<float>();
	public float TrueHeading { get { return p_TrueHeading.Value; } set { p_TrueHeading.Value = value; } }

	public ALocationHandler(
		)
	{
		this.p_Status = new ReactiveProperty<LocationServiceStatus>();
	}

	public IObservable<Quaternion> rotationTowardsObservable(FLocation loc)
	{
		return (
			p_Location
				.CombineLatest(p_TrueHeading, (l, h) => new { l, h })
				.Select(p => rotationTowards(loc))
			);
	}

	public Quaternion rotationTowards(FLocation loc)
	{
		var rotationZ = Location.GetBearing(loc);
		return /*Quaternion.Euler(0f, 0f, TrueHeading) * */Quaternion.Euler(0f, 0f, (float)rotationZ);
	}

	public virtual void Initialize() { }
	public abstract void FixedTick();
}
