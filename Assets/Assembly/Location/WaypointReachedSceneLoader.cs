﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;
using UniRx;

public class WaypointReachedSceneLoader : IDisposable, IInitializable
{
	CompositeDisposable disposeable = new CompositeDisposable();

	private WaypointReachedSignal signal;
	private RouteState currentRouteState;
	private ZenjectSceneLoader zenjectSceneLoader;
	private FDatabase fDatabase;
	private ALocationHandler locationHandler;

	[Inject]
	public WaypointReachedSceneLoader(
		WaypointReachedSignal signal,
		RouteState routeState,
		ZenjectSceneLoader zsl,
		PlayerProgress pprogress,
		FDatabase fDatabase,
		ALocationHandler ahl
		)
	{
		this.locationHandler = ahl;
		this.signal = signal;
		this.currentRouteState = routeState;
		this.zenjectSceneLoader = zsl;

		this.fDatabase = fDatabase;

		this.signal.AsObservable
			.Subscribe(
			onNext: (_) =>
			{
				Debug.Log("Waypoint Reached, Loading next scene!");
				OnWaypointReached();
			}
			)
			.AddTo(this.disposeable);
	}

	public void Dispose()
	{
		this.disposeable.Dispose();
	}

	public void Initialize()
	{
		
	}

	private void OnWaypointReached()
	{
		// Test for Waypoint Content

		switch (currentRouteState.targetWaypoint.WaypointType)
		{
			case FWaypointInfo.EWaypointType.Dialog:
				// TODO: Add Test for Root Dialogue and pop-down if it is null
				this.zenjectSceneLoader.LoadSceneAsync("DialogTemplate", LoadSceneMode.Single, (container) =>
				{
					// TODO: Redundant after moving ALocationHandlers to GPSInstaller and to App-Level ?
					container.Bind<FLocation?>().FromInstance(locationHandler.Location).WhenInjectedInto<ALocationHandler>();
					container.Bind<RouteState>().FromInstance(currentRouteState).WhenInjectedInto<RoutingInstaller>();

					container.Bind<ADialogNode>().FromInstance(currentRouteState.targetWaypoint.Dialog.RootNode).WhenInjectedInto<DialogInstaller>();
				});
				break;
			case FWaypointInfo.EWaypointType.Game:
				// TODO: Add Test for scene + Error handling (pop-down) if none found.
				this.zenjectSceneLoader.LoadSceneAsync(currentRouteState.targetWaypoint.Game.GameScene, LoadSceneMode.Single, (container) =>
				{
					// TODO: Redundant after moving ALocationHandlers to GPSInstaller and to App-Level ?
					container.Bind<FLocation?>().FromInstance(locationHandler.Location).WhenInjectedInto<ALocationHandler>();
					container.Bind<RouteState>().FromInstance(currentRouteState).WhenInjectedInto<RoutingInstaller>();
				});
				break;
			default:
				// TODO: Add proper waypoint type handling
				throw new InvalidOperationException("This is unexpected land.");
		}
	}
}
