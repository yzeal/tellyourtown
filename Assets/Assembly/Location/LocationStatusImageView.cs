﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UniRx;

[RequireComponent(typeof(Image))]
public class LocationStatusImageView : MonoBehaviour
{
	public Sprite ImageOff;
	public Sprite ImageFailed;
	public Sprite ImageInitializing;
	public Sprite ImageRunning;

	[Inject]
	void Construct(
		ILocationStatusProvider statusProvider
		)
	{
		var sub = statusProvider.p_Status
			.Subscribe((status) =>
			{
				Debug.Log(status);
				var image = GetComponent<Image>();
				switch (status)
				{
					case LocationServiceStatus.Failed:
						image.sprite = ImageFailed; break;
					case LocationServiceStatus.Initializing:
						image.sprite = ImageInitializing; break;
					case LocationServiceStatus.Running:
						image.sprite = ImageRunning; break;
					default:
						image.sprite = ImageOff; break;
				}
			})
			.AddTo(this.gameObject)
			.AddTo(this);
	}

}
