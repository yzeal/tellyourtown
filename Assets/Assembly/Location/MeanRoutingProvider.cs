﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UniRx;

public class MeanRoutingProvider : ARoutingProvider
{
	private ALocationHandler locationHandler;
	private CompositeDisposable disposables = new CompositeDisposable();
	Subject<FLocation> lastValue = new Subject<FLocation>();

	BehaviorSubject<FLocation[]> latestLocations;
	FLocation[] latestLocationValues;
	int writePosition = 0;
	int usedSlots = 0;

	[Inject]
	public MeanRoutingProvider(
		ALocationHandler handler
	)
	{
		this.locationHandler = handler;

		latestLocationValues = new FLocation[12];
		latestLocations = new BehaviorSubject<FLocation[]>(latestLocationValues);

		this.locationHandler.p_Location.Subscribe(lastValue).AddTo(disposables);

		lastValue.Scan(latestLocationValues, (acc, val) =>
		{
			acc[writePosition] = val;
			writePosition = (writePosition + 1) % (latestLocationValues.Length);
			usedSlots = Math.Min(usedSlots + 1, latestLocationValues.Length);
			return acc;
		})
		.Subscribe(latestLocations)
		.AddTo(disposables)
		;

		s_Location = latestLocations.Select(locations =>
		{
			var avg = new FLocation();
			for (int i = 0; i < usedSlots; ++i)
			{
				var l = locations[i];
				avg.latitude += l.latitude;
				avg.longitude += l.longitude;
			}
			avg.longitude /= usedSlots;
			avg.latitude /= usedSlots;

			return avg;
		});
	}

	public override void Dispose()
	{
		disposables.Clear();
	}

	public override void Initialize()
	{
	}
}
