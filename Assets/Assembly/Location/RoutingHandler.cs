using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zenject;
using UniRx;
using UnityEngine;

public class RoutingHandler : IDisposable, IInitializable
{
	CompositeDisposable disposeable = new CompositeDisposable();
	private WaypointReachedSignal waypointReached;
	private ARoutingProvider routings;
	private RouteState routeState;
	private Localizer localizer;
	private AudioSource uiaudio;
	private LocationInstaller.Settings lsettings;

	[Inject]
	public RoutingHandler(
		ARoutingProvider routings,
		WaypointReachedSignal signal,
		RouteState routeState,
		Localizer localizer,
		AudioSource uiaudio,
		LocationInstaller.Settings lsettings
		)
	{
		this.lsettings = lsettings;
		this.uiaudio = uiaudio;
		this.waypointReached = signal;
		this.routings = routings;
		this.routeState = routeState;
		this.localizer = localizer;

		
	}

	public void Dispose()
	{
		disposeable.Dispose();
	}

	public void Initialize()
	{
		//locations.s_Location
		//	.Subscribe(x => Debug.LogFormat("{0}:{1}", x.latitude, x.longitude))
		//	.AddTo(this.disposeable);
		bool once = true;
		routings.s_Location
			.Where(loc => loc.GetDistanceXY(routeState.targetWaypoint.Location) <= routeState.targetWaypoint.Radius)
			.Where(_ => once)
			.Do(_ => once = false)
			.SelectMany(reached => lsettings.WaypointReachedCue.Play(uiaudio), (reached, _) => reached)
			.Subscribe(reached =>
			{
				Debug.LogFormat("Reached waypoint {0}!", localizer.tr(routeState.targetWaypoint.Name));
				waypointReached.Fire();
			})
			.AddTo(this.disposeable)
			;
	}
}
