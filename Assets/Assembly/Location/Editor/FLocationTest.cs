using Zenject;
using NUnit.Framework;

/**
 * https://www.sunearthtools.com/en/tools/distance.php
 * */
[TestFixture]
public class FLocationTest : ZenjectUnitTestFixture
{
	[Test]
	public void TestCalculatedLength()
	{
		FLocation loc0 = new FLocation() { latitude= 49.759091, longitude= 6.632191 };
		FLocation loc1 = new FLocation() { latitude= 49.759286, longitude= 6.632903 };

		// Halber Meter +/-
		Assert.That(loc0.GetDistanceXY(loc1), Is.EqualTo(55.6).Within(.5)); 
	}

	[Test]
	public void TestBearing()
	{
		FLocation loc0 = new FLocation() { latitude = 49.759091, longitude = 6.632191 };
		FLocation loc1 = new FLocation() { latitude = 49.759286, longitude = 6.632903 };

		// 0.05 Degree +/-
		Assert.That(loc0.GetBearing(loc1), Is.EqualTo(67.03).Within(.05));
		// Von B nach A sollten es 180.0 + 67.03� sein.
		Assert.That(loc1.GetBearing(loc0), Is.EqualTo(180.0 + 67.03).Within(.05));
	}

	[Test]
	public void TestBearing2()
	{
		FLocation loc0 = new FLocation() { latitude = 49.759383, longitude = 6.633609 };
		FLocation loc1 = new FLocation() { latitude = 49.759286, longitude = 6.632903 };

		Assert.That(loc1.GetBearing(loc0), Is.EqualTo(77.99).Within(.05));
		Assert.That(loc0.GetBearing(loc1), Is.EqualTo(257.99).Within(.05));
	}

}