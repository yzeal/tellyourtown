using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zenject;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
class RoutingDirectionView : MonoBehaviour
{
	[Inject]
	public void Construct(
		RouteState route,
		ALocationHandler locationHandler
		)
	{
		QuaternionReactiveProperty targetRotation = new QuaternionReactiveProperty(Quaternion.identity);
		var uiImage = GetComponent<Image>();
		locationHandler.rotationTowardsObservable(route.targetWaypoint.Location)
			.Subscribe(q =>
			{
				// targetRotation.Value = q;
			})
			.AddTo(gameObject)
			;

		var update = Observable.EveryUpdate()
			.Subscribe(_ =>
			{
				targetRotation.Value = Quaternion.Euler(0f, 0f, Input.compass.trueHeading) * locationHandler.rotationTowards(route.targetWaypoint.Location);
				uiImage.transform.localRotation = Quaternion.Slerp(uiImage.transform.localRotation, targetRotation.Value, 30f / 500f);
			})
			.AddTo(gameObject)
			;



	}
}
