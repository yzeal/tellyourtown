using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zenject;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
class RoutingDistanceTextView : MonoBehaviour
{
	[Inject]
	public void Construct(
			ALocationHandler locationHandler,
			RouteState route
		)
	{
		var uiText = GetComponent<Text>();

		uiText.text = TextUtil.MetricTextOfDistance(locationHandler.Location.GetDistanceXY(route.targetWaypoint.Location));

		locationHandler.p_Location
			.Subscribe(loc =>
			{
				uiText.text = TextUtil.MetricTextOfDistance(loc.GetDistanceXY(route.targetWaypoint.Location));
				
			})
			.AddTo(gameObject)
			;
	}

	
}
