﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UniRx;

public class UnityLocationHandler : ALocationHandler, IDisposable
{

	CompositeDisposable disposables = new CompositeDisposable();


	[Inject]
	public UnityLocationHandler(
		)
	{
	}

	public override void Initialize()
	{
		base.Initialize();

		Observable.FromCoroutine<Tuple<FLocation, LocationServiceStatus, float>>(StartGPS_Coroutine)
			.Subscribe((result) =>
			{
				Location = result.Item1;
				Status = result.Item2;
				TrueHeading = result.Item3;
			})
			.AddTo(this.disposables)
			;
	}

	IEnumerator StartGPS_Coroutine(IObserver<Tuple<FLocation, LocationServiceStatus, float>> observer, CancellationToken cancel)
	{
		while (!Input.location.isEnabledByUser)
		{
			observer.OnNext(Tuple.Create(new FLocation(), LocationServiceStatus.Stopped, 0f));
			yield return null;
		}

		Input.location.Stop();
		Input.location.Start(7F, 3F);

		FLocation lastLocation = new FLocation();

		while (!cancel.IsCancellationRequested)
		{
			// Restart GPS if failed to launch?
			if (Input.location.status == LocationServiceStatus.Failed || Input.location.status == LocationServiceStatus.Stopped)
			{
				Input.location.Start(7F, 3F);

				int maxWait = 60;
				while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
				{
					yield return new WaitForSeconds(1);
					maxWait--;
				}
				lastLocation = new FLocation()
				{
					latitude = Input.location.lastData.latitude,
					longitude = Input.location.lastData.longitude
				};

				observer.OnNext(Tuple.Create(lastLocation, Input.location.status, Input.compass.trueHeading));
			}

			if (Input.location.status == LocationServiceStatus.Running)
			{
				lastLocation = new FLocation()
				{
					latitude = Input.location.lastData.latitude,
					longitude = Input.location.lastData.longitude
				};
			}

			observer.OnNext(Tuple.Create(lastLocation, Input.location.status, Input.compass.trueHeading));

			yield return Observable.Timer(TimeSpan.FromSeconds(Time.fixedDeltaTime)).ToYieldInstruction();
		}

		observer.OnNext(Tuple.Create(lastLocation, LocationServiceStatus.Stopped, Input.compass.trueHeading));
		Input.location.Stop();
		observer.OnCompleted();

		if (Input.location.status == LocationServiceStatus.Failed)
		{
			yield break;
		}
	}

	public override void FixedTick()
	{
	}

	public void Dispose()
	{
		this.disposables.Clear();
	}
}
