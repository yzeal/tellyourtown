using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Runtime.InteropServices;
using Zenject;
using UniRx;

public class iPhoneLocationHandler : ALocationHandler
{

	//[DllImport("__Internal")]
	//private static extern double GetLatitude();

	// TODO: Implement if using iPhone Plugin


	[Inject]
	public iPhoneLocationHandler(
		)
	{
		Input.compass.enabled = true;
		MainThreadDispatcher.StartCoroutine(StartLocationService());
	}

	public override void FixedTick()
	{
		Location = new FLocation()
		{
			latitude = Input.location.lastData.latitude,
			longitude = Input.location.lastData.longitude,
			accuracy = (0.5f*(Input.location.lastData.horizontalAccuracy + Input.location.lastData.verticalAccuracy))
		};
		TrueHeading = Input.compass.trueHeading;
	}


	IEnumerator StartLocationService()
	{
		Input.location.Start();

		while(Input.location.status != LocationServiceStatus.Running)
		{
			yield return null;
		}
		// TODO: Implement? or Plugin?
	}


}
