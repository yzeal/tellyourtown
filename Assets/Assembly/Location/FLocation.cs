﻿using UnityEngine;
using System;
using System.Collections;

[System.Serializable]
public struct FLocation
{

	/**
	 * Latitude in ...
	 */
	public double latitude;

	/**
	 * Longitude in ...
	 */
	public double longitude;

	/**
	 * Provider which created the FLocation
	 * Est. values are: 'WIFI', 'GPS', null
	 */
	[HideInInspector]
	public string provider;

	/**
	 * Get the estimated accuracy of this location, in meters.
	 */
	[HideInInspector]
	public float accuracy;

	[HideInInspector]
	public int numSatellites;


	public double GetDistanceXY(FLocation other)
	{
		const double EARTH_RADIUS = 6371000;
		const double Deg2Rad = (double)Mathf.Deg2Rad;

		double dLat = (other.latitude - this.latitude) * (Deg2Rad);
		double dLon = (other.longitude - this.longitude) * (Deg2Rad);

		double latThis = this.latitude * Deg2Rad;
		double latOther = other.latitude * Deg2Rad;

		double sin_dLat_half = Math.Sin(dLat * 0.5);
		double sin_dLon_half = Math.Sin(dLon * 0.5);

		double Alpha = (sin_dLat_half * sin_dLat_half)
								 + (sin_dLon_half * sin_dLon_half * Math.Cos(latThis) * Math.Cos(latOther));

		double C = 2.0 * Math.Atan2(Math.Sqrt(Alpha), Math.Sqrt(1.0 - Alpha));
		return (EARTH_RADIUS * C);
	}

	public double GetBearing(FLocation other)
	{
		double LatA = Mathf.Deg2Rad * this.latitude;
		double LonA = Mathf.Deg2Rad * this.longitude;
		double LatB = Mathf.Deg2Rad * other.latitude;
		double LonB = Mathf.Deg2Rad * other.longitude;

		/**
		 * mod(
		 *  atan2(sin(lon2-lon1)*cos(lat2),
     *   cos(lat1)*sin(lat2)-sin(lat1)*cos(lat2)*cos(lon2-lon1)),
     * 2*pi)
		 * */

		double Lat = Math.Sin(LonB - LonA) * Math.Cos(LatB);
		double Long = Math.Cos(LatA) * Math.Sin(LatB) - Math.Sin(LatA) * Math.Cos(LatB) * Math.Cos(LonB - LonA);

		double PI2 = Math.PI * 2.0;
		double bearing = ( (Math.Atan2(Lat, Long) + PI2) % PI2);
		// bearing = map(bearing, -180.0, 180.0, 0.0, 360.0);
		bearing = Mathf.Rad2Deg * bearing;
		return (bearing);
	}

	public static double map(double val, double from1, double from2, double to1, double to2)
	{
		return to1 + (to2 - to1) * ((val - from1) / (from2 - from1));
	}
}
