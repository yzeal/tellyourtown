using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class TrueHeadingTextView : MonoBehaviour
{
	[Inject]
	public void Construct(
		ALocationHandler locations
		)
	{
		locations.p_TrueHeading
			.Subscribe(
			(heading) =>
			{
				GetComponent<Text>().text = ""+heading + "( "+ Input.compass.trueHeading +" )";
			}
			)
			.AddTo(gameObject)
			;
	}
}