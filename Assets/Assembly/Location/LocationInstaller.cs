using System;
using UnityEngine;
using Zenject;

public class LocationInstaller : MonoInstaller<LocationInstaller>
{
	public Settings settings;

	public override void InstallBindings()
	{
		RoutingInstaller.Install(Container);

		Container.BindInstance<Settings>(settings);
		Container.DeclareSignal<WaypointReachedSignal>();

		Container.BindInterfacesTo<RoutingHandler>().AsSingle();
		//Container.Bind<RoutingHandler>().AsSingle().NonLazy();

		Container.BindInterfacesTo<WaypointReachedSceneLoader>().AsSingle();

	}

	[System.Serializable]
	public class Settings
	{
		public AAudioCue WaypointReachedCue;
	}
}
