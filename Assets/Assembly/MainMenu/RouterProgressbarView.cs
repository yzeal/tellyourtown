using Zenject;
using UniRx;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;

public class RouterProgressbarView : MonoBehaviour
{
	ReactiveProperty<float> p_Progress = new ReactiveProperty<float>(0F);

	public Image ProgressBar;
	public Text ProgressText;

	private IEnumerator LerpCoroutine(float from, float to, IObserver<float> observer, CancellationToken cancellationToken)
	{
		float Alpha = 0F;

		float Value = Mathf.Lerp(from, to, Alpha);
		observer.OnNext(Value);

		while (Alpha < 1F && !cancellationToken.IsCancellationRequested)
		{
			if (ProgressBar == null || ProgressText == null) { yield break; }
			Value = Mathf.Lerp(from, to, Alpha);
			observer.OnNext(Value);
			Alpha += Time.deltaTime;
			yield return null;
		}

		if (!cancellationToken.IsCancellationRequested)
		{
			Value = Mathf.Lerp(from, to, 1F);
			observer.OnNext(Value);

			observer.OnCompleted();
		}
	}

	private void Start()
	{
		var LerpedProgressStream = p_Progress
			.SelectMany(newProgress => Observable.FromMicroCoroutine<float>( (observer, cancel) => LerpCoroutine(ProgressBar.fillAmount, newProgress, observer, cancel) ))
			;

		LerpedProgressStream
			.Subscribe(p => ProgressBar.fillAmount = p)
			.AddTo(ProgressBar)
			.AddTo(this.gameObject);
			;

		LerpedProgressStream
			.Select(progress => progress.ToString("p0"))
			.SubscribeToText(ProgressText)
			.AddTo(ProgressText)
			.AddTo(this.gameObject)
			;
	}

	[Inject]
	public void Construct(
		PlayerProgress pprogress,
		FRouteWaypoints waypoints
		)
	{
		p_Progress.Value = (float)pprogress.Points / (float)waypoints.Count;

		p_Progress
			.AddTo(this.gameObject);
	}
}