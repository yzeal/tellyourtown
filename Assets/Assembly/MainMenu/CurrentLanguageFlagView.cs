using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
using Zenject;
using UniRx;

public class CurrentLanguageFlagView : MonoBehaviour {

	[Inject]
	void Construct(
		LanguageHandler lh,
		LanguageFlagsInstaller.FlagSetting[] FlagSettings
		)
	{
		var imageComp = GetComponent<Image>();

		lh.p_CurrentLang
			.Subscribe(newLang =>
			{
				imageComp.sprite = FlagSettings.First(fs => fs.Language == newLang).AssociatedFlag;
			}).AddTo(this.gameObject);
	}
}
