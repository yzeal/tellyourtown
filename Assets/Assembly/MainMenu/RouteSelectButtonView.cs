using UnityEngine;
using System.Collections;
using Zenject;
using UnityEngine.UI;
using UniRx;
using System.Threading;
using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.Linq;

public class RouteSelectButtonView : MonoBehaviour {
	private ZenjectSceneLoader sceneLoader;

	[Inject]
	void Construct(
		ZenjectSceneLoader zsl,
		PlayerProgress pprogress,
		FRouteWaypoints waypoints,
		RouteContext.State routeContext,
		MainMenu.State mainMenuState
		)
	{
		sceneLoader = zsl;
		var btnComp = GetComponent<Button>();

		btnComp.BindToOnClick(mainMenuState.CanExecRouteSelectionButtons, 
			_ => Observable.Create<Unit>((observer) => { // Create Async Context until observer.OnCompleted called
				var loadingScene = sceneLoader.LoadSceneAsync("RouteInfoPopup", LoadSceneMode.Additive, (container) =>
				{
					container.Bind<RouteContext.State>().FromInstance(routeContext).AsSingle().WhenInjectedInto<RouteInfoScreenInstaller>();
					container.Bind<PopupState>().FromInstance(new PopupState()
					{
						PopupObserver = observer
					}).AsSingle().WhenInjectedInto<PopupInstaller>();
				}, LoadSceneRelationship.Child);

				Progress<float> p = new Progress<float>((f) =>
				{
					Debug.LogFormat("loading {0} %", f*100.0f);
				});
				var sub = loadingScene.AsAsyncOperationObservable(p)
					.Subscribe(asyncOp =>
					{
						// observer.OnCompleted();
					});

				return Disposable.Create(() => { sub.Dispose(); });
			})
			.AsUnitObservable()
		)
		.AddTo(gameObject)
		;
	}

}
