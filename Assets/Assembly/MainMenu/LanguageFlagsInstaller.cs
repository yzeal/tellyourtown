using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "LanguageFlags", menuName = "Installers/LanguageFlags")]
public class LanguageFlagsInstaller : ScriptableObjectInstaller<LanguageFlagsInstaller>
{
	[System.Serializable]
	public class FlagSetting
	{
		public FELanguages Language;
		public Sprite AssociatedFlag;
	}

	public FlagSetting[] LanguageFlags = new FlagSetting[System.Enum.GetNames(typeof(FELanguages)).Length];

	public override void InstallBindings()
	{
		Container.BindInstance(LanguageFlags);
	}
}
