using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Zenject;
using UniRx;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class OpenCreditsButtonView : MonoBehaviour
{

	[Inject]
	void Construct(
		ZenjectSceneLoader zsl
		)
	{
		GetComponent<Button>().BindToOnClick(_ => Observable.Create<Unit>((observer) =>
			{

				var load = zsl.LoadSceneAsync("CreditsPopup", LoadSceneMode.Additive, (container) =>
				{
					// container.Bind<RouteContext.State>().FromInstance(routeContext).AsSingle().WhenInjectedInto<RouteInfoScreenInstaller>();
					container.Bind<PopupState>().FromInstance(new PopupState()
					{
						PopupObserver = observer
					}).AsSingle().WhenInjectedInto<PopupInstaller>();
				}, LoadSceneRelationship.Child);

				return Disposable.Create(() => { });
			})
			.AsUnitObservable()
		)
		.AddTo(gameObject);
	}

}
