using UniRx;
using UnityEngine;
using Zenject;

public class MainMenu
{
	[System.Serializable]
	public class State
	{
		public ReactiveProperty<bool> CanExecRouteSelectionButtons = new ReactiveProperty<bool>(true);
	}
}


public class MainMenuInstaller : MonoInstaller<MainMenuInstaller>
{
	public override void InstallBindings()
	{
		LocaleInstaller.Install(Container);
		Container.Bind<MainMenu.State>().ToSelf().AsSingle();
	}

}
