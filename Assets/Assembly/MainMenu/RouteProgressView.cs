using UnityEngine;
using System.Collections;
using Zenject;
using UniRx;
using System.Collections.Generic;

public class RouteProgressView : MonoBehaviour {
	[Inject]
	void Construct(
		PlayerProgress pprogress,
		FRouteWaypoints waypoints,
		RouteProgressSegment.Factory factory
			)
	{
		int i = 1;
		foreach(FWaypointInfo w in waypoints)
		{
			factory.Create(i++);
		}
	}
}
