using UnityEngine;
using System.Collections;
using Zenject;
using UniRx;
using System.Collections.Generic;
using UnityEngine.UI;

public class RouteButtonCompletionStarView : MonoBehaviour {

	[SerializeField]
	private Sprite Complete;
	[SerializeField]
	private Sprite Incomplete;


	[Inject]
	void Construct(
		PlayerProgress playerprogress,
		FRouteWaypoints waypoints
		)
	{
		var imageComp = GetComponent<Image>();
		playerprogress.p_Points
			.Subscribe(p =>
			{
				imageComp.sprite = p == waypoints.Count ? Complete : Incomplete;
			})
			.AddTo(this.gameObject)
			;


	}
}
