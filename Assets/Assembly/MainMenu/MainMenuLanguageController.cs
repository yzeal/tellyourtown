using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UniRx;

public class MainMenuLanguageController
{
	public MainMenuLanguageController(
		[Inject(Id = "prevLanguage")] Button prevButton,
		[Inject(Id = "nextLanguage")] Button nextButton,
		LanguageHandler lh
		)
	{
		var sharedExecute = new ReactiveProperty<bool>(lh.AppLanguages.Count > 1);
		prevButton.BindToOnClick(sharedExecute, _ => lh.SetPrevAppLanguage().AsUnitObservable());
		nextButton.BindToOnClick(sharedExecute, _ => lh.SetNextAppLanguage().AsUnitObservable());
	}

}
