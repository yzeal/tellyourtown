using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
using Zenject;
using UniRx;

[RequireComponent(typeof(Text))]
public class CurrentLanguageNameView : MonoBehaviour {

	[Inject]
	void Construct(
		LanguageHandler lh
		)
	{
		var textComp = GetComponent<Text>();

		lh.p_CurrentLang
			.Select(lang => lh.CurrentCulture)
			.Subscribe(x =>
			{
				textComp.text = x.NativeName;
			})
			.AddTo(gameObject)
			;
	}
}
