using UnityEngine;
using Zenject;

public class RouteProgressInstaller : MonoInstaller<RouteProgressInstaller>
{

	[SerializeField]
	Settings _settings;

	public override void InstallBindings()
	{
		Container.BindFactory<int, RouteProgressSegment, RouteProgressSegment.Factory>().FromComponentInNewPrefab(_settings.RouteProgressSegment).UnderTransform(this.gameObject.transform);
	}

	[System.Serializable]
	public class Settings
	{
		public GameObject RouteProgressSegment;
	}
}
