using UnityEngine;
using System.Collections;
using Zenject;
using UniRx;
using UnityEngine.UI;

public class RouteProgressSegment : MonoBehaviour
{

	Color Reached = Color.white;
    Color NotReached = new Color(0.6f, 0.6f, 0.6f, 0.2f);

	[Inject]
	void Construct(
		int lowerBound,
		PlayerProgress pprogress
		)
	{
		pprogress.p_Points
			.Subscribe(x =>
			{
				GetComponent<Image>().color = x >= lowerBound ? Reached : NotReached;
			})
			.AddTo(this.gameObject)
			;
	}



	public class Factory : Factory<int, RouteProgressSegment> { }
}
