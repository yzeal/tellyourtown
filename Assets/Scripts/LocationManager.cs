using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UniRx;

public class LocationManager : MonoBehaviour {

	public ReactiveProperty<bool> p_LocationStatus = new ReactiveProperty<bool>(false);
	public bool LocationStatus { get { return p_LocationStatus.Value; } set { p_LocationStatus.Value = value; } }

	List<string> Providers = new List<string>();
	public Subject<List<string>> s_Provider = new Subject<List<string>>();

	int Satelites;

	[HideInInspector]
	public FLocation CurrentLocation = new FLocation { };

#if UNITY_ANDROID && !UNITY_EDITOR 
	AndroidJavaClass gpsActivityClass;
#endif

	void Start()
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJNI.AttachCurrentThread();
		this.gpsActivityClass = new AndroidJavaClass("com.thoughtbread.schulprojekt.FolkloreLocation");
		StartCoroutine(AskProviders());
#endif
	}

#if UNITY_ANDROID && !UNITY_EDITOR
	IEnumerator AskProviders()
	{
		while(true)
		{
			yield return new WaitForSeconds(3.0f);
			Providers.Clear();
			{
				var AndroidLocationManager = this.gpsActivityClass.GetStatic<AndroidJavaObject>("myLocationManager");

				var enabledProviders = AndroidLocationManager.Call<AndroidJavaObject>("getProviders", new object[] { true });

				for (int i = 0; i < enabledProviders.Call<int>("size"); ++i)
				{
					Providers.Add(enabledProviders.Call<string>("get", new object[] { i }));
				}

				s_Provider.OnNext(Providers);
			}
		}
	}
#endif

	void Update()
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaObject Location = this.gpsActivityClass.GetStatic<AndroidJavaObject>("currentLocation");
		CurrentLocation.latitude = Location.Call<double>("getLatitude");
		CurrentLocation.longitude = Location.Call<double>("getLongitude");
		CurrentLocation.accuracy = Location.Call<float>("getAccuracy");
		CurrentLocation.numSatellites = (Location.Call<AndroidJavaObject>("getExtras")).Call<int>("getInt", new object[] { "satellites", -1 });
#else
		CurrentLocation.latitude -= Input.GetAxis("Vertical") * Time.deltaTime;
		CurrentLocation.longitude += Input.GetAxis("Horizontal") * Time.deltaTime;
#endif
	}
}
