using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LongitudeToText : MonoBehaviour {
	public LocationManager LocationProvider;

	// Update is called once per frame
	void Update () {
		GetComponent<Text>().text = "" + LocationProvider.CurrentLocation.longitude;
	}
}
