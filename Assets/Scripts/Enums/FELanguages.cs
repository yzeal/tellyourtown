﻿using UnityEngine;
using System.Collections;

public enum FELanguages {
	German
	, English
	, Luxembourgish
	, French
	, Default	= English
}
