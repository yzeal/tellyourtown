using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NumSatellitesToText : MonoBehaviour {
	public LocationManager LocationProvider;

	// Update is called once per frame
	void Update()
	{
		GetComponent<Text>().text = "" + LocationProvider.CurrentLocation.numSatellites;
	}
}
