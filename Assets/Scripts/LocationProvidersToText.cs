using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;
using System.Linq;


public class LocationProvidersToText : MonoBehaviour {
	public LocationManager LocationProvider;

	// Use this for initialization
	void Start () {
		LocationProvider.s_Provider
			.Subscribe((providers) =>
			{
				GetComponent<Text>().text = string.Format("{0} Providers: {1}", providers.Count, providers.Aggregate((s0, s1) => s0 + ", " + s1));
			})
			.AddTo(this.gameObject)
			;
	}
	
}
