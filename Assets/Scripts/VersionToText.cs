﻿using UnityEngine;
using System.Collections;

public class VersionToText : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GetComponent<UnityEngine.UI.Text>().text = "v"+Application.version;
	}
	
}
