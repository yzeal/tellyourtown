
SEM_VER=`git describe --tags --abbrev=0`
ROOT_PATH=`git rev-parse --show-toplevel`
COMMIT=`git describe --abbrev=8 --always`
FULL_VER=`git describe --always --long --tags`
VERSION_FILE="$ROOT_PATH/Assets/Assembly/ProjectVersionHandler.cs"

echo "Writing GameVersion $SEM_VER ($COMMIT) to $VERSION_FILE"

sed -i "/^ProjectVersion=/s/=.*/=\"$SEM_VER ($COMMIT)\"/" $VERSION_FILE

# Test Ahead of remote

AHEAD_TEST=`git rev-list @{u}..HEAD` # Test for remote

if [ "$AHEAD_TEST" ]; then 
	echo "Trying to amend version commit and tag"
else 
	echo "There is no remote for this branch... Amending disabled."
fi
AHEAD=`git rev-list @{u}..HEAD | wc -l`
zero=0

if [ "$AHEAD" -gt "$zero" ]
	then 
	echo "Amend updated version to last commit"
	git commit --amend $VERSION_FILE -C HEAD
	git tag $SEM_VER -f
else 
	echo "No unpushed commits, amend would be a bad idea!" 
fi

